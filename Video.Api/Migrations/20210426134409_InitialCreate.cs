﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Video.Api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "admin",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    account = table.Column<string>(maxLength: 20, nullable: true),
                    password = table.Column<string>(maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 20, nullable: true),
                    avatar = table.Column<string>(maxLength: 20, nullable: true),
                    employee_id = table.Column<int>(nullable: false),
                    role_id = table.Column<int>(nullable: false),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: true),
                    last_login_ip = table.Column<string>(maxLength: 50, nullable: true),
                    last_login_time = table.Column<DateTime>(nullable: true),
                    is_root = table.Column<ulong>(type: "bit", nullable: false),
                    status = table.Column<int>(nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_admin", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "department",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 20, nullable: false),
                    parent_id = table.Column<int>(nullable: false),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false),
                    remark = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_department", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    job_no = table.Column<string>(maxLength: 20, nullable: true),
                    real_name = table.Column<string>(maxLength: 20, nullable: false),
                    mobile = table.Column<string>(maxLength: 20, nullable: false),
                    avatar = table.Column<string>(maxLength: 200, nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    entry_time = table.Column<DateTime>(nullable: true),
                    resign_time = table.Column<DateTime>(nullable: true),
                    birthday = table.Column<DateTime>(nullable: true),
                    department_id = table.Column<int>(nullable: false),
                    position_id = table.Column<int>(nullable: false),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "menu",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    parent_id = table.Column<int>(nullable: false),
                    sort = table.Column<int>(nullable: false),
                    icon = table.Column<string>(maxLength: 50, nullable: true),
                    link = table.Column<string>(maxLength: 200, nullable: true),
                    router_id = table.Column<int>(nullable: false),
                    target = table.Column<int>(maxLength: 100, nullable: false),
                    permission_code = table.Column<string>(maxLength: 50, nullable: true),
                    is_group = table.Column<bool>(nullable: false),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: true),
                    visible = table.Column<ulong>(type: "bit", nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_menu", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 20, nullable: true),
                    remark = table.Column<string>(maxLength: 200, nullable: true),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role_permission",
                columns: table => new
                {
                    编号 = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    role_id = table.Column<int>(nullable: false),
                    permission_code = table.Column<string>(maxLength: 50, nullable: false),
                    icons = table.Column<string>(maxLength: 50, nullable: true),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_permission", x => x.编号);
                });

            migrationBuilder.CreateTable(
                name: "router",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    parent_id = table.Column<int>(nullable: false),
                    sort = table.Column<int>(nullable: false),
                    path = table.Column<string>(maxLength: 100, nullable: false),
                    component = table.Column<string>(maxLength: 100, nullable: true),
                    redirect = table.Column<string>(maxLength: 100, nullable: true),
                    permission_code = table.Column<string>(maxLength: 50, nullable: true),
                    icons = table.Column<string>(maxLength: 500, nullable: true),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: true),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_router", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    pid = table.Column<string>(nullable: false),
                    account = table.Column<string>(maxLength: 100, nullable: false),
                    nick_name = table.Column<string>(maxLength: 50, nullable: true),
                    password = table.Column<string>(maxLength: 100, nullable: true),
                    real_name = table.Column<string>(maxLength: 50, nullable: true),
                    gender = table.Column<int>(nullable: false),
                    user_type = table.Column<int>(nullable: false),
                    signature = table.Column<string>(maxLength: 500, nullable: true),
                    mobile = table.Column<string>(maxLength: 20, nullable: true),
                    avatar = table.Column<string>(maxLength: 200, nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    birthday = table.Column<DateTime>(nullable: true),
                    permission_id = table.Column<int>(nullable: false),
                    register_type = table.Column<int>(nullable: false),
                    creator = table.Column<int>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    last_modifier = table.Column<int>(nullable: false),
                    last_modify_time = table.Column<DateTime>(nullable: true),
                    last_login_ip = table.Column<string>(maxLength: 50, nullable: true),
                    last_login_time = table.Column<DateTime>(nullable: true),
                    is_root = table.Column<ulong>(type: "bit", nullable: false),
                    wx_pid = table.Column<string>(maxLength: 100, nullable: true),
                    qq_pid = table.Column<string>(maxLength: 100, nullable: true),
                    dingtalk_pid = table.Column<string>(maxLength: 100, nullable: true),
                    status = table.Column<int>(nullable: false),
                    is_deleted = table.Column<ulong>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "admin");

            migrationBuilder.DropTable(
                name: "department");

            migrationBuilder.DropTable(
                name: "employee");

            migrationBuilder.DropTable(
                name: "menu");

            migrationBuilder.DropTable(
                name: "role");

            migrationBuilder.DropTable(
                name: "role_permission");

            migrationBuilder.DropTable(
                name: "router");

            migrationBuilder.DropTable(
                name: "user");
        }
    }
}
