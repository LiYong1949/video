using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Video.IService;
using Video.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.Distributed;
using Video.Common;
using StackExchange.Redis;
namespace Video.Api
{
    /// <summary>
    /// 权限策略处理器
    /// </summary>
    public class PermissionHandler : AuthorizationHandler<PermissionRequirement>
    {
        // private readonly string _User_Token_Key = "USER_JWT_TOKEN_";
        // private readonly string _Admin_Token_Key = "Admin_JWT_TOKEN_";
        private readonly IDatabase _redis;
        private IHttpContextAccessor _accessor;
        /// <summary>
        /// 验证方案提供对象
        /// </summary>
        public IAuthenticationSchemeProvider Schemes { get; set; }

        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="schemes"></param>
        /// <param name="client"></param>
        /// <param name="accessor"></param>
        public PermissionHandler(IAuthenticationSchemeProvider schemes,
        RedisHelper client,
        IHttpContextAccessor accessor)
        {
            Schemes = schemes;
            _redis = client.GetDatabase();
            _accessor = accessor;
        }

        /// <summary>
        /// 权限策略处理
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.Resource is RouteEndpoint route && route != null)
            {
                var questUrl = route.RoutePattern.RawText;
                var token = _accessor.HttpContext.Request.Headers["Authorization"].ToStringEx();

                var SecurityToken = JwtToken.SerializeToken(token);
                var role = SecurityToken.Claims.ToList().FirstOrDefault(c => c.Type == ClaimTypes.Role);
                var claim = SecurityToken.Claims.ToList().FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (claim != null && role != null)
                {
                    var id = DataTypeExtensions.ToInt32(claim.Value);
                    //用户
                    if (role.Value == "user")
                    {
                        // 校验过期时间
                        var nowTime = DateTimeOffset.Now.ToUnixTimeSeconds();
                        var issued = requirement.IssuedTime + Convert.ToInt64(requirement.Expiration.TotalSeconds);
                        if (issued < nowTime)
                            context.Fail();
                        ////判断在Redis中是否存在token
                        //var exist = await _redis.KeyExistsAsync(_User_Token_Key + id);
                        //if (exist)
                        //{
                        //    //判断过期时间
                        //    if (DateTime.Parse(context.User.Claims.SingleOrDefault(s => s.Type == ClaimTypes.Expiration).Value) >= DateTime.Now)
                        //    {
                        //        //若未过期则重新设置缓存
                        //        await _redis.StringSetAsync(_User_Token_Key + id, token, DateTime.Now.AddHours(1) - DateTime.Now);
                        //        context.Succeed(requirement);
                        //        return;
                        //    }
                        //}
                    }
                    //管理员
                    else if (role.Value == "admin")
                    {
                        // 校验过期时间
                        var nowTime = DateTimeOffset.Now.ToUnixTimeSeconds();
                        var issued = requirement.IssuedTime + Convert.ToInt64(requirement.Expiration.TotalSeconds);
                        if (issued < nowTime)
                            context.Fail();
                        ////判断在Redis中是否存在token
                        //var exist = await _redis.KeyExistsAsync(_Admin_Token_Key + id);
                        //if (exist)
                        //{
                        //    //判断过期时间
                        //    if (DateTime.Parse(context.User.Claims.SingleOrDefault(s => s.Type == ClaimTypes.Expiration).Value) >= DateTime.Now)
                        //    {
                        //        //若未过期则重新设置缓存
                        //        await _redis.StringSetAsync(_Admin_Token_Key + id, token, DateTime.Now.AddHours(1) - DateTime.Now);
                        //        context.Succeed(requirement);
                        //        return;
                        //    }
                        //}
                    }
                }
            }
            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
