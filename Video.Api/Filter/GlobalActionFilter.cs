using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Video.Model;

namespace Video.Api
{
    public class GlobalActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var msg = "";

                foreach (var item in context.ModelState.Values)
                {
                    foreach (var error in item.Errors)
                    {
                        msg += error.ErrorMessage + "|";
                    }
                }

                context.Result = new ObjectResult(
                    new ApiResult()
                    {
                        Code = 998,
                        Success = false,
                        Message = msg.Trim('|')
                    }
                );

                // context.Result = new ContentResult
                // {
                //     Content = Newtonsoft.Json.JsonConvert.SerializeObject(
                //         new ApiResult()
                //         {
                //             code = 999,
                //             message = msg.Trim('|')
                //         }),
                //     StatusCode = StatusCodes.Status200OK,
                //     ContentType = "application/json;charset=utf-8"
                // };
            }
        }
    }
}