using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Video.Common;
using Video.Model;

namespace Video.Api
{
    public class GlobalExceptionsFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            var errMsg = $"【异常类型】：{ex.GetType().Name} \r\n【异常信息】：{ex.Message} \r\n【堆栈调用】：{ex.StackTrace }";

            LogHelper.Error(errMsg);

            if (ex.GetType() == typeof(MyException))
            {
                context.Result = new ObjectResult(
                    new ApiResult()
                    {
                        Code = 999,
                        Success = false,
                        Message = ex.Message
                    }
                );
            }
            else
            {
                context.Result = new ContentResult
                {
                    // Content = Newtonsoft.Json.JsonConvert.SerializeObject(
                    //   new ApiResult()
                    //   {
                    //       code = 999,
                    //       success = false,
                    //       message = ex.Message
                    //   }
                    // ),
                    Content = ex.Message,
                    StatusCode = StatusCodes.Status500InternalServerError,
                    ContentType = "application/json;charset=utf-8"
                };
            }
        }
    }
}