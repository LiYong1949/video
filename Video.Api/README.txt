一、codefirst数据库更新
1.若未安装ef则执行 dotnet tool install --global dotnet-ef
2.生成构建数据库结构文件 dotnet ef migrations add InitialCreate
3.更新到数据库 dotnet ef database update

二、接口服务端启动
    定位到Video.Api，运行命令 dotnet run（最好cmd打开Video.Api,然后运行命令）

三、后台管理端启动
    终端定位到Video.Manage，运行命令 yarn start