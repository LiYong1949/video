using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.Common;
using System.Collections.Generic;

namespace Video.Api.Controllers
{
    [Produces("application/json")]
    [Authorize(Policy = "Permission")]
    [AllowAnonymous]
    public class BaseController : Controller
    {
        /// <summary>
        /// 当前用户编号
        /// </summary>
        public int UserId
        {
            get
            {
                var role = this.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
                var claim = this.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (claim != null && role.Value == "user")
                    return claim.Value.ToInt32();

                return 0;
            }
        }

        /// <summary>
        /// 当前管理员编号
        /// </summary>
        public int AdminId
        {
            get
            {
                var role = this.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
                var claim = this.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (claim != null && role.Value == "admin")
                    return claim.Value.ToInt32();

                return 0;
            }
        }



        /// <summary>
        /// Token
        /// </summary>
        /// <value></value>
        public string Token
        {
            get
            {
                try
                {
                    return HttpContext.Request.Headers["Authorization"].ToStringEx();
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// JwtSecurityToken
        /// </summary>
        /// <value></value>
        public JwtSecurityToken SecurityToken
        {
            get
            {
                return JwtToken.SerializeToken(this.Token);
            }
        }

        /// <summary>
        /// Claims
        /// </summary>
        /// <value></value>
        public List<Claim> Claims
        {
            get
            {
                try
                {
                    return this.SecurityToken.Claims.ToList();
                }
                catch
                {
                    return new List<Claim>();
                }
            }
        }
    }
}