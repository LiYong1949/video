﻿using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
  [Route("api/routers")]
  public class RouterController : BaseController
  {
    private readonly IRouterService _IRouterService;

    public RouterController(IRouterService routerService)
    {
      _IRouterService = routerService;
    }


    /// <summary>
    /// 添加路由
    /// </summary>
    /// <param name="dto">路由添加dto</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ApiResult> Add([FromBody] RouterAddDto dto)
    {
      try
      {
        var res = await _IRouterService.AddAsync(dto);
        return new ApiResult
        {
          Message = "添加成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 修改路由
    /// </summary>
    /// <param name="id">路由id</param>
    /// <param name="dto">路由修改dto</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<ApiResult> Update(int id, [FromBody] RouterUpdateDto dto)
    {
      try
      {
        if (id <= 0)
          throw new MyException("参数非法！");
        dto.Id = id;
        var res = await _IRouterService.UpdateAsync(dto);
        return new ApiResult
        {
          Message = "修改成功！",
          Success = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 获取路由信息
    /// </summary>
    /// <param name="id">路由id</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<ApiResult> GetInfo(int id = 0)
    {
      try
      {
        var res = await _IRouterService.GetAsync(id);
        return new ApiResult
        {
          Message = "获取成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 删除路由
    /// </summary>
    /// <param name="ids">路由id字符串(以英文逗号连接)</param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<ApiResult> Delete(string ids = "")
    {
      try
      {
        var res = await _IRouterService.DeleteAsync(Array.ConvertAll(ids.Split(','), int.Parse));
        return new ApiResult
        {
          Message = "删除成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 获取所有路由树
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    public async Task<ApiResult> GetAllTree()
    {
      try
      {
        var res = await _IRouterService.GetTreeAsync();
        return new ApiResult
        {
          Message = "获取成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 获取指定角色的路由树
    /// </summary>
    /// <param name="roleId">路由编号</param>
    /// <returns></returns>
    [HttpGet("role-tree")]
    public async Task<ApiResult> GetTree(int roleId)
    {
      try
      {
        var res = await _IRouterService.GetRouterTreeByRoleIdAsync(roleId);
        return new ApiResult
        {
          Message = "获取成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }
  }
}
