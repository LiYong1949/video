﻿using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
    [Route("api/admins")]
    public class AdminController : BaseController
    {
        // private readonly string _Token_Key = "Admin_JWT_TOKEN_";
        private readonly IAdminService _IAdminService;
        private readonly PermissionRequirement _PermissionRequirement;
        private readonly IMenuService _IMenuService;
        private readonly IRouterService _IRouterService;
        private readonly IDatabase _redis;
        public AdminController(IAdminService AdminService,
            PermissionRequirement permissionRequirement,
            IMenuService menuService,
            IRouterService routerService,
            RedisHelper client)
        {
            _IAdminService = AdminService;
            _PermissionRequirement = permissionRequirement;
            _IMenuService = menuService;
            _IRouterService = routerService;
            _redis = client.GetDatabase();
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ApiResult> Login([FromBody] LoginDto parms)
        {
            try
            {
                var admin = await _IAdminService.LoginAsync(parms);
                if (admin != null)
                {
                    var claims = new List<Claim> {
                    new Claim(ClaimTypes.Role,"admin"),
                    new Claim(ClaimTypes.Name, admin.Name),
                    new Claim(ClaimTypes.Sid, admin.Id.ToString()),
                    new Claim(ClaimTypes.Expiration, DateTime.Now.AddSeconds(_PermissionRequirement.Expiration.TotalMinutes).ToString()) };

                    //用户标识
                    var identity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
                    identity.AddClaims(claims);

                    _PermissionRequirement.Role = "Admin";

                    var token = JwtToken.BuildToken(claims, _PermissionRequirement);

                    //缓存token,过期时间为1小时后
                    //await _redis.StringSetAsync(_Token_Key + admin.Id, token, DateTime.Now.AddHours(1) - DateTime.Now);
                    return new ApiResult
                    {
                        Message = "登录成功",
                        Data = new { User = admin, Token = token }
                    };
                }
                else
                {
                    return new ApiResult
                    {
                        Code = 500,
                        Success = false,
                        Message = "用户不存在！"
                    };
                }
            }
            catch (Exception e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [HttpPost("logout")]
        public ApiResult Logout()
        {
            var EmployeeId = 0;
            //取得token
            var token = HttpContext.Request.Headers["Authorization"].ToStringEx();

            var SecurityToken = JwtToken.SerializeToken(token);
            var claim = SecurityToken.Claims.ToList().FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            if (claim != null)
                //取得用户Id
                EmployeeId = claim.Value.ToInt32();
            //移除缓存的token
            //await _redis.KeyDeleteAsync(_Token_Key + EmployeeId);
            return new ApiResult
            {
                Message = "登出成功"
            };
        }

        /// <summary>
        /// 获取管理员列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ApiResult> GetList()
        {
            try
            {
                var list = await _IAdminService.GetListAsync();
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = list
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取管理员分页
        /// </summary>
        /// <param name="dto">管理员分页dto</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetPageList(AdminPageDto dto)
        {
            try
            {
                var list = await _IAdminService.GetPageListAsync(dto);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = list
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 添加管理员
        /// </summary>
        /// <param name="dto">管理员添加dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Add([FromBody] AdminAddDto dto)
        {
            try
            {
                dto.Creator = base.AdminId;
                var res = await _IAdminService.AddAsync(dto);
                return new ApiResult
                {
                    Message = "添加成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 修改管理员
        /// </summary>
        /// <param name="id">管理员编号</param>
        /// <param name="dto">管理员修改dto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ApiResult> Update(int id, [FromBody] AdminUpdateDto dto)
        {
            try
            {
                if (id <= 0)
                    throw new MyException("参数非法！");
                dto.Id = id;
                dto.LastModifier = base.AdminId;
                dto.LastModifyTime = DateTime.Now;
                var res = await _IAdminService.UpdateAsync(dto);
                return new ApiResult
                {
                    Message = "修改成功！",
                    Success = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取管理员信息
        /// </summary>
        /// <param name="id">管理员编号</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult> GetInfo(int id = 0)
        {
            try
            {
                var res = await _IAdminService.GetAsync(id);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取当前管理员信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("current-info")]
        public async Task<ApiResult> GetCurrentInfo()
        {
            try
            {
                var res = await _IAdminService.GetAsync(base.AdminId);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 删除管理员
        /// </summary>
        /// <param name="ids">管理员编号集合</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> Delete(string ids)
        {
            try
            {
                var res = await _IAdminService.DeleteAsync(Array.ConvertAll(ids.Split(','), int.Parse));
                return new ApiResult
                {
                    Message = "删除成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取当前管理员的菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet("menu-tree")]
        public async Task<ApiResult> GetCurrentMenuTree()
        {
            try
            {
                var admin = await _IAdminService.GetAsync(base.AdminId);
                var res = await _IMenuService.GetMenuTreeByRoleIdAsync(admin.RoleId);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取指定角色的路由树
        /// </summary>
        /// <returns></returns>
        [HttpGet("role-tree")]
        public async Task<ApiResult> GetTree()
        {
            try
            {
                var admin = await _IAdminService.GetAsync(base.AdminId);
                var res = await _IRouterService.GetRouterTreeByRoleIdAsync(admin.RoleId);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }
    }
}
