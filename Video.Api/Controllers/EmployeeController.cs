/*
 * @Author: liyong
 * @Date: 2020-10-22 14:09:07
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 10:26:33
 * @Description: 员工Controller
 */
using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
  [Route("api/employees")]
  public class EmployeeController : BaseController
  {
    private readonly IEmployeeService _IEmployeeService;

    public EmployeeController(IEmployeeService employeeService)
    {
      _IEmployeeService = employeeService;
    }

    /// <summary>
    /// 获取员工列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("all")]
    public async Task<ApiResult> GetList()
    {
      try
      {
        var list = await _IEmployeeService.GetListAsync();
        return new ApiResult
        {
          Message = "获取成功！",
          Data = list
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 获取员工分页
    /// </summary>
    /// <param name="dto">员工分页dto</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ApiResult> GetPageList(EmployeePageDto dto)
    {
      try
      {
        var list = await _IEmployeeService.GetPageListAsync(dto);
        return new ApiResult
        {
          Message = "获取成功！",
          Data = list
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 添加员工
    /// </summary>
    /// <param name="dto">员工添加dto</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ApiResult> Add([FromBody] EmployeeAddDto dto)
    {
      try
      {
        dto.Creator = base.AdminId;
        var res = await _IEmployeeService.AddAsync(dto);
        return new ApiResult
        {
          Message = "添加成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 修改员工
    /// </summary>
    /// <param name="id">员工id</param>
    /// <param name="dto">员工修改dto</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<ApiResult> Update(int id, [FromBody] EmployeeUpdateDto dto)
    {
      try
      {
        if (id <= 0)
          throw new MyException("参数非法！");
        dto.Id = id;
        dto.LastModifier = base.AdminId;
        var res = await _IEmployeeService.UpdateAsync(dto);
        return new ApiResult
        {
          Message = "修改成功！",
          Success = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 获取员工信息
    /// </summary>
    /// <param name="id">员工id</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<ApiResult> GetInfo(int id = 0)
    {
      try
      {
        var res = await _IEmployeeService.GetAsync(id);
        return new ApiResult
        {
          Message = "获取成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// 删除员工
    /// </summary>
    /// <param name="ids">员工编号集合</param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<ApiResult> Delete(int[] ids)
    {
      try
      {
        var res = await _IEmployeeService.DeleteAsync(ids);
        return new ApiResult
        {
          Message = "删除成功！",
          Data = res
        };
      }
      catch (MyException e)
      {
        return new ApiResult
        {
          Code = 500,
          Success = false,
          Message = e.Message
        };
      }
    }
  }
}
