﻿using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
    [Route("api/roles")]
    public class RoleController : BaseController
    {
        private readonly IRoleService _IRoleService;
        private readonly IRolePermissionService _IRolePermissionService;
        public RoleController(IRoleService roleService,
        IRolePermissionService rolePermissionService)
        {
            _IRoleService = roleService;
            _IRolePermissionService = rolePermissionService;
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ApiResult> GetList()
        {
            try
            {
                var list = await _IRoleService.GetListAsync();
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = list
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取角色分页
        /// </summary>
        /// <param name="dto">角色分页dto</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetPageList(RolePageDto dto)
        {
            try
            {
                var list = await _IRoleService.GetPageListAsync(dto);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = list
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="dto">角色添加dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Add([FromBody] RoleAddDto dto)
        {
            try
            {
                var res = await _IRoleService.AddAsync(dto);
                return new ApiResult
                {
                    Message = "添加成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="id">角色id</param>
        /// <param name="dto">角色修改dto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ApiResult> Update(int id, [FromBody] RoleUpdateDto dto)
        {
            try
            {
                if (id <= 0)
                    throw new MyException("参数非法！");
                dto.Id = id;
                dto.LastModifier = base.AdminId;
                dto.LastModifyTime = DateTime.Now;
                var res = await _IRoleService.UpdateAsync(dto);
                return new ApiResult
                {
                    Message = "修改成功！",
                    Success = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult> GetInfo(int id = 0)
        {
            try
            {
                var res = await _IRoleService.GetAsync(id);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids">角色编号集合</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> Delete(string ids)
        {
            try
            {
                var res = await _IRoleService.DeleteAsync(Array.ConvertAll(ids.Split(','), int.Parse));
                return new ApiResult
                {
                    Message = "删除成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 权限按钮枚举
        /// </summary>
        /// <returns></returns>
        [HttpGet("enums/auth-icon")]
        public ApiResult AuthIconEnumList()
        {
            var list = EnumHelper.EnumToList(typeof(AuthIconEnum));
            return new ApiResult()
            {
                Data = list
            };
        }

        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        [HttpGet("permission/{id}")]
        public async Task<ApiResult> PermissionList(int id)
        {
            try
            {
                var res = await _IRolePermissionService.GetListByRoleAsync(id);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }
    }
}
