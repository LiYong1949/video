/*
 * @Author: liyong
 * @Date: 2020-12-07 22:17:56
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-21 19:53:42
 * @Description: 用户Controller
 */
using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
    [Route("api/users")]
    public class UserController : BaseController
    {
        // private readonly string _Token_Key = "USER_JWT_TOKEN_";
        private readonly IUserService _IUserService;
        private readonly PermissionRequirement _PermissionRequirement;
        private readonly IDatabase _redis;
        public UserController(IUserService userService,
            PermissionRequirement permissionRequirement,
            RedisHelper client)
        {
            _IUserService = userService;
            _PermissionRequirement = permissionRequirement;
            _redis = client.GetDatabase();
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ApiResult> Login([FromBody] LoginDto parms)
        {
            try
            {
                var user = await _IUserService.LoginAsync(parms);

                var claims = new List<Claim> {
                new Claim(ClaimTypes.Role,"user"),
                new Claim(ClaimTypes.Name, user.NickName),
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
                new Claim(ClaimTypes.Expiration, DateTime.Now.AddSeconds(_PermissionRequirement.Expiration.TotalMinutes).ToString()) };

                //用户标识
                var identity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
                identity.AddClaims(claims);

                _PermissionRequirement.Role = "User";

                var token = JwtToken.BuildToken(claims, _PermissionRequirement);

                //缓存token,过期时间为1小时后
                //await _redis.StringSetAsync(_Token_Key + user.Id, token, DateTime.Now.AddHours(1) - DateTime.Now);
                return new ApiResult
                {
                    Message = "登录成功",
                    Data = new { User = user, Token = token }
                };
            }
            catch (Exception e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [HttpPost("logout")]
        public ApiResult Logout()
        {
            var EmployeeId = 0;
            //取得token
            var token = HttpContext.Request.Headers["Authorization"].ToStringEx();

            var SecurityToken = JwtToken.SerializeToken(token);
            var claim = SecurityToken.Claims.ToList().FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            if (claim != null)
                //取得用户Id
                EmployeeId = claim.Value.ToInt32();
            //移除缓存的token
            //await _redis.KeyDeleteAsync(_Token_Key + EmployeeId);
            return new ApiResult
            {
                Message = "登出成功"
            };
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="dto">用户分页dto</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetList(UserPageDto dto)
        {
            try
            {
                var list = await _IUserService.GetListAsync(dto);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = list
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="dto">用户添加dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Add([FromBody] UserAddDto dto)
        {
            try
            {
                var res = await _IUserService.AddAsync(dto);
                return new ApiResult
                {
                    Message = "添加成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <param name="dto">用户修改dto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ApiResult> Update(int id, [FromBody] UserUpdateDto dto)
        {
            try
            {
                if (id <= 0)
                    throw new MyException("参数非法！");
                dto.Id = id;
                var res = await _IUserService.UpdateAsync(dto);
                return new ApiResult
                {
                    Message = "修改成功！",
                    Success = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult> GetInfo(int id = 0)
        {
            try
            {
                var res = await _IUserService.GetAsync(id);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids">用户编号集合</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> Delete(int[] ids)
        {
            try
            {
                var res = await _IUserService.DeleteAsync(ids);
                return new ApiResult
                {
                    Message = "删除成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }
    }
}
