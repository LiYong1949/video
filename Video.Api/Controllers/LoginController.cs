/*
 * @Author: liyong
 * @Date: 2020-06-02 22:11:20
 * @LastEditors: liyong
 * @LastEditTime: 2020-12-07 22:09:45
 * @Description: 登录——Controller
 */
using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;
namespace Video.Api.Controllers
{
    [Produces("application/json")]
    public class LoginController : Controller
    {
        private readonly string _Token_Key = "USER_JWT_TOKEN";
        private readonly PermissionRequirement _PermissionRequirement;
        private readonly IEmployeeService _IEmployeeService;
        private readonly IUserService _IUserService;
        private readonly IDatabase _redis;
        public LoginController(
            PermissionRequirement permissionRequirement,
            IEmployeeService employeeService,
            IUserService userService,
            RedisHelper client
            )
        {
            _PermissionRequirement = permissionRequirement;
            _IEmployeeService = employeeService;
            _IUserService = userService;
            _redis = client.GetDatabase();
        }

        #region 前台
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ApiResult> Login([FromBody] LoginDto parms)
        {
            try
            {
                var user = await _IUserService.LoginAsync(parms);

                var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.NickName),
                new Claim(ClaimValueTypes.String,"User"),
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
                new Claim(ClaimTypes.Expiration, DateTime.Now.AddSeconds(_PermissionRequirement.Expiration.TotalMinutes).ToString()) };

                //用户标识
                var identity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
                identity.AddClaims(claims);
                
                _PermissionRequirement.Role = "User";

                var token = JwtToken.BuildToken(claims, _PermissionRequirement);

                //缓存token,过期时间为1小时后
                await _redis.StringSetAsync(_Token_Key + user.Id, token, DateTime.Now.AddHours(1) - DateTime.Now);
                return new ApiResult
                {
                    Message = "登录成功",
                    Data = new { User = user, Token = token}
                };
            }
            catch (Exception e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [HttpPost("logout")]
        public async Task<ApiResult> Logout()
        {
            var EmployeeId = 0;
            //取得token
            var token = HttpContext.Request.Headers["Authorization"].ToStringEx();

            var SecurityToken = JwtToken.SerializeToken(token);
            var claim = SecurityToken.Claims.ToList().FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            if (claim != null)
                //取得用户Id
                EmployeeId = claim.Value.ToInt32();
            //移除缓存的token
            await _redis.KeyDeleteAsync(_Token_Key + EmployeeId);
            return new ApiResult
            {
                Message = "登出成功"
            };
        }
        #endregion

        #region 后台

        #endregion
    }
}