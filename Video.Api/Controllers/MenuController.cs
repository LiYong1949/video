/*
 * @Author: liyong
 * @Date: 2020-12-06 19:49:54
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 20:31:13
 * @Description: 菜单Controller
 */
using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;

namespace Video.Api.Controllers
{
    [Route("api/menus")]
    public class MenuController : BaseController
    {
        private readonly IMenuService _IMenuService;

        public MenuController(IMenuService menuService)
        {
            _IMenuService = menuService;
        }


        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="dto">菜单添加dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Add([FromBody] MenuAddDto dto)
        {
            try
            {
                dto.Creator = base.AdminId;
                dto.CreateTime = DateTime.Now;
                var res = await _IMenuService.AddAsync(dto);
                return new ApiResult
                {
                    Message = "添加成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="id">菜单id</param>
        /// <param name="dto">菜单修改dto</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ApiResult> Update(int id, [FromBody] MenuUpdateDto dto)
        {
            try
            {
                if (id <= 0)
                    throw new MyException("参数非法！");
                dto.Id = id;
                var res = await _IMenuService.UpdateAsync(dto);
                return new ApiResult
                {
                    Message = "修改成功！",
                    Success = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="id">菜单id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult> GetInfo(int id = 0)
        {
            try
            {
                var res = await _IMenuService.GetAsync(id);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="ids">菜单id字符串(以英文逗号连接)</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> Delete(string ids = "")
        {
            try
            {
                var res = await _IMenuService.DeleteAsync(Array.ConvertAll(ids.Split(','), int.Parse));
                return new ApiResult
                {
                    Message = "删除成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取所有菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet("tree")]
        public async Task<ApiResult> GetAllTree()
        {
            try
            {
                var res = await _IMenuService.GetTreeAsync();
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// 获取指定角色的菜单树
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns></returns>
        [HttpGet("role-tree")]
        public async Task<ApiResult> GetTree(int roleId)
        {
            try
            {
                var res = await _IMenuService.GetMenuTreeByRoleIdAsync(roleId);
                return new ApiResult
                {
                    Message = "获取成功！",
                    Data = res
                };
            }
            catch (MyException e)
            {
                return new ApiResult
                {
                    Code = 500,
                    Success = false,
                    Message = e.Message
                };
            }
        }
    }
}
