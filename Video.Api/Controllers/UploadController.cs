using System.Reflection.Metadata.Ecma335;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Video.IService;
using Video.Model;
using StackExchange.Redis;
using Video.Common;
using System.Linq;
using System.IO;

namespace Video.Api.Controllers
{
  [Route("api/upload")]
  public class UploadController : BaseController
  {
    private readonly IWebHostEnvironment _IWebHostEnvironment;

    public UploadController(IWebHostEnvironment webHostEnvironment)
    {
      _IWebHostEnvironment = webHostEnvironment;
    }


    /// <summary>
    /// 头像上传
    /// </summary>
    /// <returns></returns>
    [HttpPost("avatar")]
    public ApiResult Upload()
    {
      var result = new ApiResult();

      var files = Request.Form.Files;
      if (files == null || files.Count() <= 0)
        throw new Exception("请选择上传的文件");

      //格式限制
      var allowType = new string[] { "image/jpg", "image/png", "image/jpeg" };
      if (!files.Any(c => allowType.Contains(c.ContentType)))
        throw new Exception("不允许上传的格式");

      if (files.Sum(c => c.Length) >= 1024 * 1024 * 2)
        throw new Exception("文件过大");

      List<dynamic> fileList = new List<dynamic>();

      foreach (var file in files)
      {
        fileList.Add(Upload(file));
      }

      return new ApiResult()
      {
        Data = fileList
      };
    }

    private dynamic Upload(IFormFile file)
    {
      var fileName = file.FileName;
      var ext = Path.GetExtension(fileName).Trim('.');
      fileName = $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}.{ext}";

      var webRootPath = _IWebHostEnvironment.WebRootPath;
      var dir = DateTime.Now.ToString("yyyyMM");

      var fullPath = Path.Combine(webRootPath, "upload");
      fullPath = Path.Combine(fullPath, dir);
      fullPath = Path.Combine(fullPath, ext.ToUpper());

      if (!Directory.Exists(fullPath))
        Directory.CreateDirectory(fullPath);

      fullPath = Path.Combine(fullPath, fileName);

      using (var stream = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
      {
        file.CopyTo(stream);
      }

      return new
      {
        Name = file.FileName,
        Url = $"/upload/{dir}/{ext}/{fileName}"
      };
    }
  }
}
