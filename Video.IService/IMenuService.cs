using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;
namespace Video.IService
{
    public interface IMenuService
    {
        ///<summary>
        ///获取菜单树
        ///</summary>
        /// <returns></returns>
        Task<List<MenuTreeDto>> GetTreeAsync(int parentId = 0);

        /// <summary>
        /// 获取指定角色菜单树
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Task<List<MenuTreeDto>> GetMenuTreeByRoleIdAsync(int roleId);

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="dto">菜单添加Dto</param>
        /// <returns></returns>
        Task<int> AddAsync(MenuAddDto dto);

        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="dto">菜单修改Dto</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(MenuUpdateDto dto);

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="idArray">菜单数组</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int[] idArray);

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="dto">菜单id</param>
        /// <returns></returns>
        Task<Menu> GetAsync(int id);
    }
}
