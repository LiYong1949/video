﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IService
{
    public  interface IRouterService
    {
        /// <summary>
        /// 获取路由信息
        /// </summary>
        /// <param name="id">路由编号</param>
        /// <returns></returns>
        Task<RouterDto> GetAsync(int id);

        /// <summary>
        /// 创建路由
        /// </summary>
        /// <param name="dto">路由添加dto</param>
        /// <returns></returns>
        Task<int> AddAsync(RouterAddDto dto);

        /// <summary>
        /// 修改路由
        /// </summary>
        /// <param name="dto">路由修改dto</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(RouterUpdateDto dto);

        /// <summary>
        /// 删除路由
        /// </summary>
        /// <param name="idArray">路由编号集合</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int[] idArray);

        /// <summary>
        /// 获取路由树
        /// </summary>
        /// <param name="parentId">父级编号</param>
        /// <returns></returns>
        Task<List<RouterTreeDto>> GetTreeAsync(int parentId=0);

        /// <summary>
        /// 获取指定角色路由树
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns></returns>
        Task<List<RouterTreeDto>> GetRouterTreeByRoleIdAsync(int roleId);
    }
}
