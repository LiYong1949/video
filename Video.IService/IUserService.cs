/*
 * @Author: liyong
 * @Date: 2020-12-07 21:35:40
 * @LastEditors: liyong
 * @LastEditTime: 2020-12-07 22:03:25
 * @Description: 用户IService
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;


namespace Video.IService
{
    public interface IUserService
    {
        #region 登录

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<User> LoginAsync(LoginDto dto);

        #endregion

        #region 用户管理
        
        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="dto">用户分页查询Dto</param>
        Task<PageResultDto> GetListAsync(UserPageDto dto);

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="dto">用户新增Dto</param>
        Task<int> AddAsync(UserAddDto dto);

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="dto">用户修改Dto</param>
        Task<bool> UpdateAsync(UserUpdateDto dto);

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="dto">用户修改密码Dto</param>
        Task<bool> UpdatePasswordAsync(int id, UserUpdatePasswordDto dto);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="idArray">编号集合</param>
        Task<bool> DeleteAsync(int[] idArray);  

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        Task<User> GetAsync(int id);

        #endregion
    }
}
