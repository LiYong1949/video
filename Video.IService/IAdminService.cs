﻿using System.Threading.Tasks;
using Video.Model;

namespace Video.IService
{
  public interface IAdminService
  {
    /// <summary>
    /// 管理员登录
    /// </summary>
    /// <param name="dto">登录Dto</param>
    Task<Admin> LoginAsync(LoginDto dto);

    /// <summary>
    /// 获取所有管理员
    /// </summary>
    Task<PageResultDto> GetListAsync();

    /// <summary>
    /// 获取管理员分页
    /// </summary>
    /// <param name="dto">管理员分页查询Dto</param>
    Task<PageResultDto> GetPageListAsync(AdminPageDto dto);

    /// <summary>
    /// 新增管理员
    /// </summary>
    /// <param name="dto">管理员新增Dto</param>
    Task<int> AddAsync(AdminAddDto dto);

    /// <summary>
    /// 修改管理员
    /// </summary>
    /// <param name="dto">管理员修改Dto</param>
    Task<bool> UpdateAsync(AdminUpdateDto dto);

    /// <summary>
    /// 删除管理员
    /// </summary>
    /// <param name="idArray">编号集合</param>
    Task<bool> DeleteAsync(int[] idArray);

    /// <summary>
    /// 获取管理员信息
    /// </summary>
    /// <param name="id">管理员id</param>
    /// <returns></returns>
    Task<AdminDto> GetAsync(int id);
  }
}
