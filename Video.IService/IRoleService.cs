﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IService
{
  public interface IRoleService
  {
    /// 获取角色列表
    /// </summary>
    Task<PageResultDto> GetListAsync();

    /// 获取角色分页
    /// </summary>
    /// <param name="dto">角色分页查询Dto</param>
    Task<PageResultDto> GetPageListAsync(RolePageDto dto);

    /// <summary>
    /// 新增角色
    /// </summary>
    /// <param name="dto">角色新增Dto</param>
    Task<int> AddAsync(RoleAddDto dto);

    /// <summary>
    /// 修改角色
    /// </summary>
    /// <param name="dto">角色修改Dto</param>
    Task<bool> UpdateAsync(RoleUpdateDto dto);

    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="idArray">编号集合</param>
    Task<bool> DeleteAsync(int[] idArray);

    /// <summary>
    /// 获取角色信息
    /// </summary>
    /// <param name="id">角色id</param>
    /// <returns></returns>
    Task<Role> GetAsync(int id);
  }
}
