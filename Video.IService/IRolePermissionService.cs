/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-16 15:58:00
 * @Description: 权限IService
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IService
{
    public interface IRolePermissionService
    {
        /// <summary>
        /// 获取指定角色的权限列表
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns></returns>
        Task<List<RolePermission>> GetListByRoleAsync(int roleId);

        /// <summary>
        /// 添加角色权限
        /// </summary>
        /// <param name="dto">角色权限添加dto</param>
        /// <returns></returns>
        Task<int> AddAsync(RolePermissionAddDto dto);

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="idArray">编号集合</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int[] idArray);

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="dto">角色授权Dto</param>
        /// <returns></returns>
        Task<bool> AuthAsync(RoleAuthDto dto);
    }
}
