/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 10:23:23
 * @Description: 员工IService
 */
using System.Collections.Generic;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IService
{
  public interface IEmployeeService
  {

    #region 员工管理

    /// <summary>
    /// 获取员工列表
    /// </summary>
    /// <returns></returns>
    Task<PageResultDto> GetListAsync();

    /// <summary>
    /// 获取员工分页
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<PageResultDto> GetPageListAsync(EmployeePageDto dto);

    /// <summary>
    /// 添加员工信息
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<int> AddAsync(EmployeeAddDto dto);

    /// <summary>
    /// 修改员工信息
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    Task<bool> UpdateAsync(EmployeeUpdateDto dto);

    /// <summary>
    /// 删除员工
    /// </summary>
    /// <param name="idArray"></param>
    Task<bool> DeleteAsync(int[] idArray);

    /// <summary>
    /// 获取员工
    /// </summary>
    /// <param name="userID"></param>
    /// <returns></returns>
    Task<Employee> GetAsync(int userID);

    #endregion
  }
}