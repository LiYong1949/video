/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2020-10-22 17:21:11
 * @Description: 员工IRepository
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IRepository
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        Task<PageResultDto> GetListAsync(Expression<Func<EmployeeDto, bool>> where, int pageIndex, int pageSize, string order);
    }
}