/*
 * @Author: liyong
 * @Date: 2020-10-22 17:03:31
 * @LastEditors: liyong
 * @LastEditTime: 2020-10-22 17:09:51
 * @Description: 菜单IRepository
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IRepository
{
    public interface IMenuRepository : IBaseRepository<Menu>
    {
    }
}
