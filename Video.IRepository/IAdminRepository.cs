﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Video.Model;
namespace Video.IRepository
{
    public interface IAdminRepository : IBaseRepository<Admin>
    {
    }
}
