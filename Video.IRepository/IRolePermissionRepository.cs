/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2020-10-22 17:10:09
 * @Description: 用户权限IRepository
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IRepository
{
    public interface IRolePermissionRepository : IBaseRepository<RolePermission>
    {
    }
}
