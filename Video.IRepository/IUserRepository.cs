/*
 * @Author: liyong
 * @Date: 2020-12-07 20:44:44
 * @LastEditors: liyong
 * @LastEditTime: 2020-12-07 20:45:44
 * @Description: 用户IRepository
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Video.Model;

namespace Video.IRepository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<PageResultDto> GetListAsync(Expression<Func<UserDto, bool>> where, int pageIndex, int pageSize, string order);
    }
}