using System;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;

namespace Video.IRepository
{
    public interface IBaseRepository<T> where T : class
    {
         Task<int> SaveChangesAsync();

        /// <summary>
        /// 新增一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<int> AddAsync(T entity, bool isSave = true);

        /// <summary>
        /// 新增多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> AddAsync(List<T> entitys, bool isSave = true);

        /// <summary>
        /// 删除一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T entity, bool isSave = true);

        /// <summary>
        /// 删除多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(List<T> entitys, bool isSave = true);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity, bool isSave = true);

        /// <summary>
        /// 修改一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dto"></param>
        /// <param name="isSave"></param>
        /// <typeparam name="TDto"></typeparam>
        /// <returns></returns>
        Task<bool> UpdateAsync<TDto>(T entity, TDto dto, bool isSave = true);

        /// <summary>
        /// 修改指定字段
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="updatePropertyList"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity, List<string> updatePropertyList, bool isSave = true);

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(List<T> entitys, bool isSave = true);

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="dtoType"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(List<T> entitys, Type dtoType, bool isSave = true);

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(List<T> entitys, List<string> updatePropertyList, bool isSave = true);

        /// <summary>
        /// 获取entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetAsync(object id);

        /// <summary>
        /// 获取entity
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<T> GetAsync(Expression<Func<T, bool>> where = null, bool isNoTracking = true);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync(bool isNoTracking = true);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, bool isNoTracking = true);

        /// <summary>
        /// 可指定排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="where"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, string order, bool isNoTracking = true);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, int pageIndex, int pageSize, bool isNoTracking = true);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, int pageIndex, int pageSize, string order, bool isNoTracking = true);

        /// <summary>
        /// 可指定返回结果、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, bool isNoTracking = true);

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, int pageIndex, int pageSize, bool isNoTracking = true);

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, string order, bool isNoTracking = false);

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, int pageIndex, int pageSize, string order, bool isNoTracking = false);

        /// <summary>
        /// 获取记录条数
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> GetCountAsync(Expression<Func<T, bool>> where);

        /// <summary>
        /// 验证是否存在
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<bool> IsExistAsync(Expression<Func<T, bool>> where);
    }
}
