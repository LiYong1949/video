﻿/*
 * @Description: 角色Service
 * @Author: liyong
 * @Date: 2021-05-25 15:14:20
 * @LastEditTime: 2021-06-15 10:46:42
 * @LastEditors: liyong
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;

namespace Video.Service
{
  public class RoleService : IRoleService
  {
    private readonly IMapper _IMapper;
    private readonly IRoleRepository _IRoleRepository;
    public RoleService(
        IMapper mapper,
        IRoleRepository userRepository)
    {
      _IMapper = mapper;
      _IRoleRepository = userRepository;
    }

    /// 获取角色列表
    /// </summary>
    public async Task<PageResultDto> GetListAsync()
    {
      Expression<Func<Role, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);

      var total = await _IRoleRepository.GetCountAsync(where);

      var dbres = await _IRoleRepository.GetListAsync(where);
      return new PageResultDto() { Total = total, Data = _IMapper.Map<List<RoleDto>>(dbres) };
    }

    /// 获取角色分页
    /// </summary>
    /// <param name="dto">角色分页查询Dto</param>
    public async Task<PageResultDto> GetPageListAsync(RolePageDto dto)
    {
      Expression<Func<Role, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);
      if (!string.IsNullOrEmpty(dto.Name))
        where = where.And(p => p.Name.Contains(dto.Name));

      if (Enum.IsDefined(typeof(RoleStatusEnum), dto.Status))
        where = where.And(p => p.Status == (RoleStatusEnum)dto.Status);
      var total = await _IRoleRepository.GetCountAsync(where);

      var dbres = await _IRoleRepository.GetListAsync(where, dto.PageIndex, dto.PageSize, dto.Sort);
      return new PageResultDto() { Total = total, Data = _IMapper.Map<List<RoleDto>>(dbres) };
    }

    /// <summary>
    /// 新增角色
    /// </summary>
    /// <param name="dto">角色新增Dto</param>
    public async Task<int> AddAsync(RoleAddDto dto)
    {
      var role = _IMapper.Map<Role>(dto) ?? new Role();

      var isExistName = await _IRoleRepository.IsExistAsync(p => p.Name == role.Name && p.IsDeleted == false);
      if (isExistName)
        throw new MyException("已存在相同名称的角色，请重新输入！");

      await _IRoleRepository.AddAsync(role);
      return role.Id;
    }

    /// <summary>
    /// 修改角色
    /// </summary>
    /// <param name="dto">角色修改Dto</param>
    public async Task<bool> UpdateAsync(RoleUpdateDto dto)
    {
      var entity = await GetAsync(dto.Id);
      entity = _IMapper.Map(dto, entity);

      var isExistName = await _IRoleRepository.IsExistAsync(p => p.Name == entity.Name && p.Id != entity.Id && p.IsDeleted == false);
      if (isExistName)
        throw new MyException("角色已存在");

      var dbres = await _IRoleRepository.UpdateAsync(entity, dto);
      return dbres;
    }

    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="idArray">编号集合</param>
    public async Task<bool> DeleteAsync(int[] idArray)
    {
      if (idArray.Length == 0)
        throw new MyException("参数异常，删除角色失败");

      var entityList = await _IRoleRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<Role>();

      entityList.ForEach(entity =>
      {
        entity.IsDeleted = true;
      });

      var dbres = await _IRoleRepository.UpdateAsync(entityList,
        new List<string>() {
                          "IsDeleted"
                });
      return true;
    }

    /// <summary>
    /// 获取角色信息
    /// </summary>
    /// <param name="id">角色id</param>
    /// <returns></returns>
    public async Task<Role> GetAsync(int id)
    {
      if (id <= 0)
        throw new MyException($"未获取到角色信息");

      var entity = await _IRoleRepository.GetAsync(p => p.Id == id && p.IsDeleted == false, true);
      return entity ?? new Role();
    }
  }
}
