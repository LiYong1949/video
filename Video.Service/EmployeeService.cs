/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 10:46:57
 * @Description: 员工Service
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;
using Microsoft.AspNetCore.Http;

namespace Video.Service
{
  public class EmployeeService : IEmployeeService
  {
    private readonly IMapper _IMapper;
    private readonly IEmployeeRepository _IEmployeeRepository;
    private IHttpContextAccessor _accessor;
    public EmployeeService(
        IMapper mapper,
        IEmployeeRepository employeeRepository,
        IHttpContextAccessor accessor
        )
    {
      _IMapper = mapper;
      _IEmployeeRepository = employeeRepository;
      _accessor = accessor;
    }


    #region 员工管理

    /// <summary>
    /// 获取员工列表
    /// </summary>
    /// <returns></returns>
    public async Task<PageResultDto> GetListAsync()
    {
      Expression<Func<Employee, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);

      var total = await _IEmployeeRepository.GetCountAsync(where);
      var dbres = await _IEmployeeRepository.GetListAsync(where);
      var map = _IMapper.Map<List<EmployeeDto>>(dbres);
      return new PageResultDto() { Total = total, Data = map };
    }

    /// <summary>
    /// 获取员工分页
    /// </summary>
    /// <param name="dto">员工分页查询Dto</param>
    /// <returns></returns>
    public async Task<PageResultDto> GetPageListAsync(EmployeePageDto dto)
    {
      Expression<Func<Employee, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);

      if (!string.IsNullOrEmpty(dto.RealName))
        where = where.And(p => p.RealName.Contains(dto.RealName));

      if (!string.IsNullOrEmpty(dto.Mobile))
        where = where.And(p => p.Mobile.Contains(dto.Mobile));

      if (dto.DepartmentId > 0)
        where = where.And(p => p.DepartmentId == dto.DepartmentId);
      var total = await _IEmployeeRepository.GetCountAsync(where);
      var dbres = await _IEmployeeRepository.GetListAsync(where, dto.PageIndex, dto.PageSize, dto.Sort);
      var map = _IMapper.Map<List<EmployeeDto>>(dbres);
      return new PageResultDto() { Total = total, Data = map };
    }

    /// <summary>
    /// 添加员工信息
    /// </summary>
    /// <param name="dto">员工添加Dto</param>
    /// <returns></returns>
    public async Task<int> AddAsync(EmployeeAddDto dto)
    {
      var employee = _IMapper.Map<Employee>(dto) ?? new Employee();

      var isExistMobile = await _IEmployeeRepository.IsExistAsync(p => p.Mobile == employee.Mobile && p.IsDeleted == false);
      if (isExistMobile)
        throw new MyException("手机号码已存在");

      var isExistEmail = await _IEmployeeRepository.IsExistAsync(p => p.Email == employee.Email && p.IsDeleted == false);
      if (isExistEmail)
        throw new MyException("邮箱地址已经存在");
      employee.JobNo = DateTime.Now.ToString("yyyyMMddHHmmss");
      await _IEmployeeRepository.AddAsync(employee);
      return employee.Id;
    }

    /// <summary>
    /// 修改员工信息
    /// </summary>
    /// <param name="dto">员工修改Dto</param>
    /// <returns></returns>
    public async Task<bool> UpdateAsync(EmployeeUpdateDto dto)
    {
      var entity = await GetAsync(dto.Id);
      entity = _IMapper.Map(dto, entity);

      var isExistMobile = await _IEmployeeRepository.IsExistAsync(p => p.Mobile == entity.Mobile && p.Id != entity.Id && p.IsDeleted == false);
      if (isExistMobile)
        throw new MyException("手机号码已存在");

      var isExistEmail = await _IEmployeeRepository.IsExistAsync(p => p.Email == entity.Email && p.Id != entity.Id && p.IsDeleted == false);
      if (isExistEmail)
        throw new MyException("邮箱地址已经存在");

      var dbres = await _IEmployeeRepository.UpdateAsync(entity, dto);
      return dbres;
    }

    /// <summary>
    /// 删除员工
    /// </summary>
    /// <param name="idArray">员工编号集合</param>
    /// <returns></returns>
    public async Task<bool> DeleteAsync(int[] idArray)
    {
      if (idArray.Length == 0)
        throw new MyException("参数异常，删除员工失败");

      var entityList = await _IEmployeeRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<Employee>();

      entityList.ForEach(entity =>
      {
        entity.IsDeleted = true;
      });

      var dbres = await _IEmployeeRepository.UpdateAsync(entityList,
        new List<string>() {
                          "IsDeleted"
                });
      return true;
    }

    /// <summary>
    /// 获取员工信息
    /// </summary>
    /// <param name="id">员工编号</param>
    /// <returns></returns>
    public async Task<Employee> GetAsync(int id)
    {
      if (id <= 0)
        throw new MyException($"未获取到员工信息");

      var entity = await _IEmployeeRepository.GetAsync(p => p.Id == id && p.IsDeleted == false, true);
      return entity ?? new Employee();
    }
    #endregion
  }
}
