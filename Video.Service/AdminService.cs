﻿/*
 * @Description: 管理员Service
 * @Author: liyong
 * @Date: 2021-05-25 15:14:20
 * @LastEditTime: 2021-06-09 16:41:25
 * @LastEditors: liyong
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;

namespace Video.Service
{
  public class AdminService : IAdminService
  {
    private readonly IMapper _IMapper;
    private readonly IAdminRepository _IAdminRepository;
    private readonly IRoleService _IRoleService;
    private IHttpContextAccessor _accessor;
    public AdminService(
        IMapper mapper,
        IAdminRepository userRepository,
        IRoleService roleService,
        IHttpContextAccessor accessor)
    {
      _IMapper = mapper;
      _IAdminRepository = userRepository;
      _IRoleService = roleService;
      _accessor = accessor;
    }

    /// <summary>
    /// 管理员登录
    /// </summary>
    /// <param name="dto">登录Dto</param>
    public async Task<Admin> LoginAsync(LoginDto dto)
    {
      var admin = await _IAdminRepository.GetAsync(p => p.Account == dto.Account && p.IsDeleted == false);
      if (admin == null)
        throw new MyException("帐号或密码错误，请核实后重新输入！");

      if (!admin.Password.Equals(dto.Password))
        throw new MyException("密码不正确");

      if (admin.Status != AdminStatusEnum.Normal)
        throw new MyException("账号状态异常，请联系管理员");
      admin = _IMapper.Map(dto, admin);
      admin.LastLoginIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
      admin.LastLoginTime = DateTime.Now;
      //更新用户登录信息
      await _IAdminRepository.UpdateAsync(admin, dto);
      return admin;
    }

    /// <summary>
    /// 获取管理员列表
    /// </summary>
    public async Task<PageResultDto> GetListAsync()
    {
      Expression<Func<Admin, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);
      var total = await _IAdminRepository.GetCountAsync(where);
      var dbres = await _IAdminRepository.GetListAsync(where);
      var map = _IMapper.Map<List<AdminDto>>(dbres);
      var role = new Role();
      foreach (var item in map)
      {
        role = await _IRoleService.GetAsync(item.RoleId);
        item.RoleName = role.Name ?? "";
      }
      return new PageResultDto() { Total = total, Data = map };
    }

    /// <summary>
    /// 获取管理员分页
    /// </summary>
    /// <param name="dto">管理员分页查询Dto</param>
    public async Task<PageResultDto> GetPageListAsync(AdminPageDto dto)
    {
      Expression<Func<Admin, bool>> where = p => true;
      where = where.And(p => p.IsDeleted == false);
      if (!string.IsNullOrEmpty(dto.Name))
        where = where.And(p => p.Name.Contains(dto.Name));
      if (dto.RoleId > 0)
        where = where.And(p => p.RoleId == dto.RoleId);
      if (Enum.IsDefined(typeof(AdminStatusEnum), dto.Status))
        where = where.And(p => p.Status == (AdminStatusEnum)dto.Status);
      var total = await _IAdminRepository.GetCountAsync(where);
      var dbres = await _IAdminRepository.GetListAsync(where, dto.PageIndex, dto.PageSize, dto.Sort);
      var map = _IMapper.Map<List<AdminDto>>(dbres);
      var role = new Role();
      foreach (var item in map)
      {
        role = await _IRoleService.GetAsync(item.RoleId);
        item.RoleName = role.Name ?? "";
      }

      return new PageResultDto() { Total = total, Data = map };
    }

    /// <summary>
    /// 新增管理员
    /// </summary>
    /// <param name="dto">管理员新增Dto</param>
    public async Task<int> AddAsync(AdminAddDto dto)
    {
      var user = _IMapper.Map<Admin>(dto) ?? new Admin();

      user.Password = CryptHelper.GetMD5(user.Password);//密码md5加密
      await _IAdminRepository.AddAsync(user);
      return user.Id;
    }

    /// <summary>
    /// 修改管理员
    /// </summary>
    /// <param name="dto">管理员修改Dto</param>
    public async Task<bool> UpdateAsync(AdminUpdateDto dto)
    {
      var entity = await _IAdminRepository.GetAsync(dto.Id);
      entity = _IMapper.Map(dto, entity);

      var dbres = await _IAdminRepository.UpdateAsync(entity, dto);
      return dbres;
    }

    /// <summary>
    /// 修改管理员密码
    /// </summary>
    /// <param name="id">管理员id</param>
    /// <param name="dto">管理员修改密码Dto</param>
    public async Task<bool> UpdatePasswordAsync(int id, AdminUpdatePasswordDto dto)
    {
      var entity = await _IAdminRepository.GetAsync(id);

      if (CryptHelper.GetMD5(dto.OldPassword) != entity.Password)
        throw new MyException("旧密码不正确");

      entity.Password = CryptHelper.GetMD5(dto.NewPassword);

      var dbres = await _IAdminRepository.UpdateAsync(entity,
          new List<string>() {
                    "Password"
          });
      return dbres;
    }

    /// <summary>
    /// 删除管理员
    /// </summary>
    /// <param name="idArray">编号集合</param>
    public async Task<bool> DeleteAsync(int[] idArray)
    {
      if (idArray.Length == 0)
        throw new MyException("参数异常，删除管理员失败");

      var entityList = await _IAdminRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<Admin>();

      entityList.ForEach(entity =>
      {
        entity.IsDeleted = true;
      });

      var dbres = await _IAdminRepository.UpdateAsync(entityList,
        new List<string>() {
                          "IsDeleted"
                });
      return true;
    }

    /// <summary>
    /// 获取管理员信息
    /// </summary>
    /// <param name="id">管理员id</param>
    /// <returns></returns>
    public async Task<AdminDto> GetAsync(int id)
    {
      if (id <= 0)
        throw new MyException($"未获取到管理员信息");

      var entity = _IMapper.Map<AdminDto>(await _IAdminRepository.GetAsync(p => p.Id == id && p.IsDeleted == false, true));
      return entity ?? new AdminDto();
    }
  }
}
