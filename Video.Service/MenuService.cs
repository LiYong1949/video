/*
 * @Author: liyong
 * @Date: 2020-10-23 09:05:48
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-17 21:01:11
 * @Description: 菜单Service
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;
using Newtonsoft.Json;

namespace Video.Service
{
    public class MenuService : IMenuService
    {
        private readonly IMapper _IMapper;
        private readonly IMenuRepository _IMenuRepository;
        private readonly IRolePermissionService _IRolePermissionService;

        public MenuService(IMapper Mapper,
        IMenuRepository MenuRepository,
        IRolePermissionService RolePermissionService)
        {
            _IMapper = Mapper;
            _IMenuRepository = MenuRepository;
            _IRolePermissionService = RolePermissionService;
        }

        ///<summary>
        ///构造菜单树
        ///</summary>
        /// <param name="menuList">菜单集合</param>
        /// <param name="parentId">父级Id</param>
        /// <returns></returns>
        private List<MenuTreeDto> GenerateMenuTree(List<Menu> menuList, int parentId)
        {
            var menuTree = new List<MenuTreeDto>();
            menuList.Where(p => p.ParentId == parentId).ToList().ForEach(item =>
                {
                    var children = GenerateMenuTree(menuList, item.Id);
                    menuTree.Add(new MenuTreeDto()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Icon = item.Icon,
                        PermissionCode = item.PermissionCode,
                        RouterPath = item.RouterPath,
                        ParentId = item.ParentId,
                        Sort = item.Sort,
                        Visible = item.Visible,
                        Target = item.Target,
                        AuthButtons = item.AuthButtons,
                        Meta = new
                        {
                            Title = item.Name,
                            Icon = item.Icon
                        },
                        Children = children
                    });
                });

            return menuTree;
        }

        /// <summary>
        /// 构造指定权限列表菜单树
        /// </summary>
        /// <param name="menuList">菜单列表</param>
        /// <param name="permissionList">权限列表</param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<MenuTreeDto> GenerateMenuTreeByPermission(List<MenuTreeDto> menuList, List<RolePermission> permissionList, long parentId)
        {
            var menuTree = new List<MenuTreeDto>();
            var children = new List<MenuTreeDto>();
            menuList.Where(p => p.ParentId == parentId).ToList().ForEach(menu =>
            {

                if (permissionList.Count(s => s.PermissionCode == menu.PermissionCode) == 1)
                {
                    children = GenerateMenuTreeByPermission(menuList, permissionList, menu.Id);

                    menuTree.Add(new MenuTreeDto()
                    {
                        Id = menu.Id,
                        RouterPath = menu.RouterPath,
                        Visible = menu.Visible,
                        Meta = new
                        {
                            Title = menu.Name,
                            Icon = menu.Icon
                        },
                        Icon = menu.Icon,
                        IsGroup = menu.IsGroup,
                        Name = menu.Name,
                        Link = menu.Link,
                        ParentId = menu.ParentId,
                        Target = menu.Target,
                        PermissionCode = menu.PermissionCode,
                        AuthButtons = menu.AuthButtons,
                        Children = children
                    });
                }

            });

            return menuTree;
        }

        ///<summary>
        ///获取菜单树
        ///</summary>
        /// <returns></returns>
        public async Task<List<MenuTreeDto>> GetTreeAsync(int parentId = 0)
        {
            var list = await _IMenuRepository.GetListAsync(p => p.IsDeleted == false);
            return GenerateMenuTree(list, parentId);
        }

        /// <summary>
        /// 获取指定角色菜单树
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<List<MenuTreeDto>> GetMenuTreeByRoleIdAsync(int roleId)
        {
            var menuList = await _IMenuRepository.GetListAsync();
            var list = _IMapper.Map<List<MenuTreeDto>>(menuList);
            var permissionList = await _IRolePermissionService.GetListByRoleAsync(roleId);

            var menuTree = GenerateMenuTreeByPermission(list, permissionList, 0);
            return menuTree;
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="dto">菜单添加Dto</param>
        /// <returns></returns>
        public async Task<int> AddAsync(MenuAddDto dto)
        {
            var menu = _IMapper.Map<Menu>(dto) ?? new Menu();
            menu.PermissionCode = System.Guid.NewGuid().ToString();
            await _IMenuRepository.AddAsync(menu);
            return menu.Id;
        }

        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <param name="dto">菜单修改Dto</param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(MenuUpdateDto dto)
        {
            var entity = await _IMenuRepository.GetAsync(dto.Id);
            entity = _IMapper.Map(dto, entity);
            var dbres = await _IMenuRepository.UpdateAsync(entity, dto);
            return dbres;
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="idArray">菜单数组</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int[] idArray)
        {
            if (idArray.Length == 0)
                throw new MyException("参数异常，删除菜单失败");

            var entityList = await _IMenuRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<Menu>();

            entityList.ForEach(entity =>
            {
                entity.IsDeleted = true;
            });

            var dbres = await _IMenuRepository.UpdateAsync(entityList,
              new List<string>() {
                          "IsDeleted"
                      });
            return true;
        }

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="dto">菜单id</param>
        /// <returns></returns>
        public async Task<Menu> GetAsync(int id)
        {
            if (id <= 0)
                throw new MyException($"未获取到菜单信息");

            var entity = await _IMenuRepository.GetAsync(p => p.Id == id && p.IsDeleted == false, true);
            return entity ?? new Menu();
        }
    }

}


