/*
 * @Author: liyong
 * @Date: 2020-12-07 21:35:17
 * @LastEditors: liyong
 * @LastEditTime: 2020-12-10 20:17:23
 * @Description: 用户Service
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;
using Microsoft.AspNetCore.Http;

namespace Video.Service
{
    public class UserService : IUserService
    {
        private readonly IMapper _IMapper;
        private readonly IUserRepository _IUserRepository;
        private IHttpContextAccessor _accessor;
        public UserService(
            IMapper mapper,
            IUserRepository userRepository,
            IHttpContextAccessor accessor
            )
        {
            _IMapper = mapper;
            _IUserRepository = userRepository;
            _accessor = accessor;
        }

        #region 登录

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="dto">登录Dto</param>
        public async Task<User> LoginAsync(LoginDto dto)
        {
            var user = await _IUserRepository.GetAsync(p => p.Mobile == dto.Account && p.IsDeleted == false);
            if (user == null)
                throw new MyException("帐号或密码错误，请核实后重新输入！");

            if (!user.Password.Equals(dto.Password))
                throw new MyException("密码不正确");

            if (user.Status != UserStatusEnum.Normal)
                throw new MyException("账号状态异常，请联系管理员");
            user = _IMapper.Map(dto, user);
            user.LastLoginIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            user.LastLoginTime = DateTime.Now;
            //更新用户登录信息
            await _IUserRepository.UpdateAsync(user, dto);
            return user;
        }

        #endregion

        #region  用户管理

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="dto">用户分页查询Dto</param>
        public async Task<PageResultDto> GetListAsync(UserPageDto dto)
        {
            Expression<Func<UserDto, bool>> where = p => true;

            if (!string.IsNullOrEmpty(dto.NickName))
                where = where.And(p => p.NickName.Contains(dto.NickName));

            if (!string.IsNullOrEmpty(dto.Mobile))
                where = where.And(p => p.Mobile.Contains(dto.Mobile));

            var dbres = await _IUserRepository.GetListAsync(where, dto.PageIndex, dto.PageSize, dto.Sort);
            return dbres ?? new PageResultDto();
        }

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="dto">用户新增Dto</param>
        public async Task<int> AddAsync(UserAddDto dto)
        {
            var user = _IMapper.Map<User>(dto) ?? new User();

            var isExistMobile = await _IUserRepository.IsExistAsync(p => p.Mobile == user.Mobile && p.IsDeleted == false);
            if (isExistMobile)
                throw new MyException("手机号码已绑定，请重新输入！");

            user.Pid = Guid.NewGuid().ToString();
            user.Password = CryptHelper.GetMD5(user.Password);//密码md5加密
            await _IUserRepository.AddAsync(user);
            return user.Id;
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="dto">用户修改Dto</param>
        public async Task<bool> UpdateAsync(UserUpdateDto dto)
        {
            var entity = await GetAsync(dto.Id);
            entity = _IMapper.Map(dto, entity);

            var isExistMobile = await _IUserRepository.IsExistAsync(p => p.Mobile == entity.Mobile && p.Id != entity.Id && p.IsDeleted == false);
            if (isExistMobile)
                throw new MyException("手机号码已存在");

            var isExistEmail = await _IUserRepository.IsExistAsync(p => p.Email == entity.Email && p.Id != entity.Id && p.IsDeleted == false);
            if (isExistEmail)
                throw new MyException("邮箱地址已经存在");

            var dbres = await _IUserRepository.UpdateAsync(entity, dto);
            return dbres;
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="id">用户id</param>
        /// <param name="dto">用户修改密码Dto</param>
        public async Task<bool> UpdatePasswordAsync(int id, UserUpdatePasswordDto dto)
        {
            var entity = await GetAsync(id);

            if (CryptHelper.GetMD5(dto.OldPassword) != entity.Password)
                throw new MyException("旧密码不正确");

            entity.Password = CryptHelper.GetMD5(dto.NewPassword);

            var dbres = await _IUserRepository.UpdateAsync(entity,
                new List<string>() {
                    "Password"
                });
            return dbres;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="idArray">编号集合</param>
        public async Task<bool> DeleteAsync(int[] idArray)
        {
            if (idArray.Length == 0)
                throw new MyException("参数异常，删除用户失败");

            var entityList = await _IUserRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<User>();

            entityList.ForEach(entity =>
            {
                entity.IsDeleted = true;
            });

            var dbres = await _IUserRepository.UpdateAsync(entityList,
              new List<string>() {
                          "IsDeleted"
                      });
            return true;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        public async Task<User> GetAsync(int id)
        {
            if (id <= 0)
                throw new MyException($"未获取到用户信息");

            var entity = await _IUserRepository.GetAsync(p => p.Id == id && p.IsDeleted == false, true);
            return entity ?? new User();
        }

        #endregion
    }
}
