/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-21 20:44:19
 * @Description: 权限Service
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;

namespace Video.Service
{
    public class RolePermissionService : IRolePermissionService
    {

        private readonly IMapper _IMapper;
        private readonly IRolePermissionRepository _IRolePermissionRepository;
        private readonly IRoleService _IRoleService;

        public RolePermissionService(
            IMapper mapper,
            IRolePermissionRepository RolePermissionRepository,
            IRoleService roleService
            )
        {
            _IMapper = mapper;
            _IRolePermissionRepository = RolePermissionRepository;
            _IRoleService = roleService;
        }

        /// <summary>
        /// 获取指定角色的权限列表
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns></returns>
        public async Task<List<RolePermission>> GetListByRoleAsync(int roleId)
        {
            Expression<Func<RolePermission, bool>> where = p => true;

            where = where.And(p => p.RoleId == roleId);
            return await _IRolePermissionRepository.GetListAsync(where, "id asc");
        }

        /// <summary>
        /// 添加角色权限
        /// </summary>
        /// <param name="dto">角色权限添加dto</param>
        /// <returns></returns>
        public async Task<int> AddAsync(RolePermissionAddDto dto)
        {
            return await _IRolePermissionRepository.AddAsync(_IMapper.Map<RolePermission>(dto));
        }

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="idArray">编号集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int[] idArray)
        {
            if (idArray.Length == 0)
                throw new MyException("参数异常，删除权限失败");

            var entityList = await _IRolePermissionRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<RolePermission>();

            var dbres = await _IRolePermissionRepository.DeleteAsync(entityList);
            return true;
        }

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="dto">角色授权Dto</param>
        /// <returns></returns>
        public async Task<bool> AuthAsync(RoleAuthDto dto)
        {
            var role = new Role();
            var entityList = new List<RolePermission>();
            foreach (var roleId in dto.RoleIds)
            {
                role = await _IRoleService.GetAsync(roleId);
                if (role != null)
                {
                    entityList = await GetListByRoleAsync(roleId);
                    // 若存在授权则先删除原授权，再添加授权
                    if (entityList.Count > 0)
                    {
                        await _IRolePermissionRepository.DeleteAsync(entityList);
                    }
                    foreach (var item in dto.Auths)
                    {
                        await AddAsync(new RolePermissionAddDto() { PermissionCode = item.PermissionCode, AuthButtons = item.AuthButtons, Creator = dto.Creator, CreateTime = dto.CreateTime });
                    }
                }
            }
            return true;
        }
    }
}
