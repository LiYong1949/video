﻿/*
 * @Description: 路由Service
 * @Author: liyong
 * @Date: 2021-05-25 15:14:20
 * @LastEditTime: 2021-06-16 16:18:36
 * @LastEditors: liyong
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Video.Common;
using Video.IRepository;
using Video.IService;
using Video.Model;
using Newtonsoft.Json;

namespace Video.Service
{
    public class RouterService : IRouterService
    {
        private readonly IMapper _IMapper;
        private readonly IRouterRepository _IRouterRepository;
        private readonly IRolePermissionService _IRolePermissionService;

        public RouterService(IMapper Mapper,
            IRouterRepository RouterRepository,
            IRolePermissionService RolePermissionService)
        {
            _IMapper = Mapper;
            _IRouterRepository = RouterRepository;
            _IRolePermissionService = RolePermissionService;
        }

        /// <summary>
        /// 获取路由信息
        /// </summary>
        /// <param name="id">路由编号</param>
        /// <returns></returns>
        public async Task<RouterDto> GetAsync(int id)
        {
            var res = await _IRouterRepository.GetAsync(id);
            return _IMapper.Map<RouterDto>(res);
        }

        /// <summary>
        /// 创建路由
        /// </summary>
        /// <param name="dto">路由添加dto</param>
        /// <returns></returns>
        public async Task<int> AddAsync(RouterAddDto dto)
        {
            var entity = _IMapper.Map<Router>(dto);
            entity.PermissionCode = Guid.NewGuid().ToString();
            return await _IRouterRepository.AddAsync(entity);
        }

        /// <summary>
        /// 修改路由
        /// </summary>
        /// <param name="dto">路由修改dto</param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(RouterUpdateDto dto)
        {
            var entity = await _IRouterRepository.GetAsync(dto.Id);
            entity = _IMapper.Map(dto, entity);
            return await _IRouterRepository.UpdateAsync(entity);
        }

        /// <summary>
        /// 删除路由
        /// </summary>
        /// <param name="idArray">路由编号集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int[] idArray)
        {
            if (idArray.Length == 0)
                throw new MyException("参数异常，删除用户失败");

            var entityList = await _IRouterRepository.GetListAsync(p => idArray.Contains(p.Id)) ?? new List<Router>();

            entityList.ForEach(entity =>
            {
                entity.IsDeleted = true;
            });

            var dbres = await _IRouterRepository.UpdateAsync(entityList,
              new List<string>() {
                          "IsDeleted"
                      });
            return true;
        }

        /// <summary>
        /// 获取路由树
        /// </summary>
        /// <param name="parentId">父级编号</param>
        /// <returns></returns>
        public async Task<List<RouterTreeDto>> GetTreeAsync(int parentId = 0)
        {
            var routerList = await _IRouterRepository.GetListAsync();
            return GenerateRouterTree(routerList, parentId);
        }

        /// <summary>
        /// 构造路由树
        /// </summary>
        /// <param name="routerList">路由数据</param>
        /// <param name="parentId">父级编号</param>
        /// <returns></returns>
        public List<RouterTreeDto> GenerateRouterTree(List<Router> routerList, int parentId)
        {
            var routerTree = new List<RouterTreeDto>();

            routerList.Where(p => p.ParentId == parentId).ToList().ForEach(router =>
            {
                var children = GenerateRouterTree(routerList, router.Id);
                routerTree.Add(new RouterTreeDto()
                {
                    Id = router.Id,
                    Path = router.Path,
                    Name = router.Name,
                    ParentId = router.ParentId,
                    PermissionCode = router.PermissionCode,
                    Sort = router.Sort,
                    Component = router.Component,
                    Redirect = router.Redirect,
                    Children = children
                });
            });

            return routerTree;
        }

        /// <summary>
        /// 获取指定角色路由树
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns></returns>
        public async Task<List<RouterTreeDto>> GetRouterTreeByRoleIdAsync(int roleId)
        {
            var routerList = await _IRouterRepository.GetListAsync();
            var permissionList = await _IRolePermissionService.GetListByRoleAsync(roleId);

            var routerTree = GenerateRouterTree(routerList, permissionList, 0);
            return routerTree;
        }

        /// <summary>
        /// 构造指定权限列表路由树
        /// </summary>
        /// <param name="routerList">路由列表</param>
        /// <param name="permissionList">权限列表</param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        private List<RouterTreeDto> GenerateRouterTree(List<Router> routerList, List<RolePermission> permissionList, int parentId)
        {
            var routerTree = new List<RouterTreeDto>();

            routerList.Where(p => p.ParentId == parentId).ToList().ForEach(router =>
            {
                if (permissionList.Count(s => s.PermissionCode == router.PermissionCode) == 1)
                {
                    var children = GenerateRouterTree(routerList, permissionList, router.Id);

                    routerTree.Add(new RouterTreeDto()
                    {
                        Id = router.Id,
                        Name = router.Name,
                        Path = router.Path,
                        Component = router.Component,
                        Redirect = router.Redirect,
                        Meta = new
                        {
                            Title = router.Name,
                        },
                        Children = children
                    });
                }
                else if (permissionList.Count(s => s.PermissionCode == router.PermissionCode) == 0)
                {
                    routerTree.AddRange(GenerateRouterTree(routerList.FindAll(s => s.ParentId == router.Id), permissionList, router.Id));
                }
            });

            return routerTree;
        }
    }
}
