/*
 * @Description:管理员
 * @Author: liyong
 * @Date: 2021-05-25 15:14:20
 * @LastEditTime: 2021-06-11 15:19:11
 * @LastEditors: liyong
 */
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react'
import LayoutUnit from '@/components/Layout'
import { Table, Button, message, Modal, Tag } from 'antd';
import AdminCreate from './Create';
import AdminModify from './Modify';
import { getAdminPageList, deleteAdmin } from '@/api/admin'
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      total: 0,
      dataSource: [],
      columns: [],
      create: {
        title: '添加管理员',
        visible: false
      },
      modify: {
        title: '更新管理员',
        visible: false,
        id: 0
      },
      sorter: {// 排序
        columnKey: 'createTime',
        order: 'descend'
      },
      selectedRowKeys: [],// 勾选
      pagination: { // 分页
        current: 1, // 当前页
        pageSize: 20, // 每页显示条数
        total: 0 // 总数
      }
    };
  }

  componentDidMount () {
    this.handleGetAdmins();
  }
  //获取管理员列表数据
  handleGetAdmins = async (type) => {
    let { create, modify, pagination, sorter } = this.state;
    create.visible = false;
    modify.visible = false;
    this.setState({ create, modify });
    if (type !== 1) { // 取消操作的回调
      this.setState({ loading: true })
      let res = await getAdminPageList(Object.assign({ pageIndex: pagination.index, pageSize: pagination.size, sort: 'CreateTime DESC' }));
      if (res.success) {
        let columns = [
          {
            title: '序号',
            dataIndex: 'index',
            render: (text, record, index) => {
              return index + 1;
            }
          },
          {
            title: '名字',
            key: 'name',
            dataIndex: 'name'
          },
          {
            title: '账号',
            key: 'account',
            dataIndex: 'account'
          },
          {
            title: '性别',
            key: 'gender',
            dataIndex: 'gender',
            render: (text, record) => {
              return text !== 0 ? (text === 1 ? '男' : '女') : '未知'
            }
          },
          {
            title: '角色',
            key: 'roleName',
            dataIndex: 'roleName'
          },
          {
            title: '状态',
            dataIndex: 'status',
            render: (text, record) => {
              let color = text === 0 ? 'green' : 'red'
              return (
              <Tag color={color} key={text}>
                {text === 0 ? '正常' : '锁定'}
              </Tag>
              )
            }
          },
          {
            title: '创建时间',
            key: 'createTime',
            dataIndex: 'createTime',
            defaultSortOrder: 'descend',
            sorter: (a, b) => {
              let aTime = new Date(a.createTime).getTime();
              let bTime = new Date(b.createTime).getTime();
              return aTime - bTime;
            }
          },
          {
            title: '操作',
            dataIndex: 'options',
            render: (text, record) => {
              return (
                <>
                  <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'modifyAdmin', record)}>修改</Button>
                  <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'deleteAdmin', record)}>删除</Button>
                </>
              )
            }
          }
        ];
        this.setState({
          columns,
          total: res.data.total,
          dataSource: res.data.data,
          loading: false
        });
      } else {
        message.error(res.message)
      }
    }
  }
  //操作
  handleVisibleModal = (type, record) => {
    let _this = this
    let { create, modify, selectedRowKeys } = this.state;
    switch (type) {
      case 'createAdmin':
        create.visible = true
        break;
      case 'modifyAdmin':
        modify.visible = true
        modify.id = record.id
        break;
      case 'deleteAdmin':
        if (record.id) {
          Modal.confirm({
            title: '删除管理员',
            content: '确认要删除该管理员？',
            onOk () {
              deleteAdmin(record.id).then(res => {
                if (res.success) {
                  message.success('删除成功！')
                  _this.handleGetAdmins()
                } else {
                  message.error(res.message)
                }
              })
            }
          })
        }
        else {
          if (selectedRowKeys.length > 0) {
            Modal.confirm({
              title: '删除管理员',
              content: '确认要删除勾选的管理员？',
              onOk () {
                deleteAdmin(selectedRowKeys.join(',')).then(res => {
                  if (res.success) {
                    message.success('删除成功！')
                    _this.handleGetAdmins()
                  } else {
                    message.error(res.message)
                  }
                })
              }
            })
          } else {
            message.warning('未勾选！')
          }
        }
        break;
      case 'cancel':
        break;
      default:
        break;
    }
    this.setState({
      create,
      modify
    });
  }

  //表格变化
  handleTableChange = (pagination, filters, sorter, extra) => {
    if (extra.action === 'paginate')// 分页
    {
      this.setState({ pagination: pagination })
    } else if (extra.action === 'sort') //排序
    {
      this.setState({ sort: sorter.field + ' ' + (sorter.order ? sorter.order.replace('end', '') : '') })
    } else { //筛选，filter

    }
    this.handleGetAdmins()
  }

  //勾选checkbox
  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys: selectedRowKeys });
  };
  render () {
    const { loading, selectedRowKeys, pagination } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    return (<div>
      <div style={{ marginBottom: 15 }}>
        <Button type="primary" style={{ marginRight: 8 }} onClick={this.handleVisibleModal.bind(this, 'createAdmin')}>新建管理员</Button>
        <Button type="primary" onClick={this.handleVisibleModal.bind(this, 'deleteAdmin')}>删除管理员</Button>
      </div>
      <Table
        rowSelection={rowSelection}
        className="d-table-center-container"
        dataSource={this.state.dataSource}
        columns={this.state.columns}
        loading={this.state.loading}
        rowKey={record => record.id}
        bordered
        pagination={pagination}
        onChange={this.handleTableChange}
      />
      <AdminCreate isVisible={this.state.create.visible} width={500} title={this.state.create.title} callbackParent={this.handleGetAdmins}></AdminCreate>
      <AdminModify id={this.state.modify.id} isVisible={this.state.modify.visible} width={500} title={this.state.modify.title} callbackParent={this.handleGetAdmins}></AdminModify>
    </div>)
  }
}

export default LayoutUnit(HomePage)