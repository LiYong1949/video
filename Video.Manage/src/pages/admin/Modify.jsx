import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, Radio, message, Modal, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { upload } from '@/api/public'
import { getRoleList } from '@/api/role'
import { getAdminInfo, modifyAdmin } from '@/api/admin'
import { getEmployeeList } from '@/api/employee'
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
  const { id, isVisible, title, callbackParent } = props
  const [form] = Form.useForm()
  const [visible, setVisible] = useState(isVisible)
  // 状态控制
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const [fileList, setFileList] = useState([])
  const [roleList, setRoleList] = useState([])
  const [employeeList, setEmployeeList] = useState([])

  useEffect(async () => {
    // 获取角色列表
    let res = await getRoleList()
    if (res.success) {
      setRoleList(res.data.data)
    } else {
      message.error(res.message)
    }

    let res2 = await getEmployeeList()
    if (res2.success) {
      setEmployeeList(res2.data.data)
    } else {
      message.error(res2.message)
    }

    if (id > 0) {
      let res3 = await getAdminInfo(id)
      if (res3.success) {
        form.setFieldsValue(res3.data)
        return res3.data
      } else {
        message.error(res3.message)
      }
    }

  }, [id])

  // 提交表单
  const handleOk = async () => {
    setLoading(true);
    form.validateFields().then(values => {
      setLoading(false);
      modifyAdmin(id, values).then(res => {
        setLoading(false);
        if (res.success) {
          message.success('管理员修改成功！');
          // form.resetFields()
          callbackParent(); // 关闭父容器
        } else {
          message.error(res.message);
        }
      });
    })
  };

  //取消
  const handleCancel = () => {
    callbackParent(1); // 关闭父容器
  }

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }
  //上传前钩子
  const beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('只支持JPEG和PNG格式!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('图片大小不能超过2M!');
    }
    return isJpgOrPng && isLt2M;
  }

  //上传事件
  const handleChange = info => {
    if (info.file.status === 'uploading') {
      return;
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, imageUrl =>
        setImageUrl(imageUrl)
      );
    }
  };


  // 表单布局
  const layout = {
    labelCol: {
      span: 5
    },
    wrapperCol: {
      span: 18
    }
  };
  //上传按钮
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>上传头像</div>
    </div>
  );

  return (
    <Modal
      forceRender
      width={700}
      title={title}
      onOk={handleOk}
      visible={isVisible}
      onCancel={handleCancel}
    >
      <Spin spinning={loading}>
        <Form
          {...layout}
          name="AdminModify"
          form={form}
        >
          <Form.Item
            label="名字"
            name="name"
            rules={[
              {
                required: true,
                message: '请输入管理员名字！'
              }
            ]}
          >
            <Input allowClear maxLength={10} placeholder="输入管理员名字" />
          </Form.Item>
          <Form.Item
            label="账号"
            name="account"
            rules={[
              {
                required: true,
                message: '请输入管理员账号！'
              }
            ]}
          >
            <Input allowClear maxLength={20} placeholder="输入管理员账号" />
          </Form.Item>
          <Form.Item
            label="性别"
            name="gender"
            rules={[
              {
                required: true,
                message: '请选择性别！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>未知</Radio>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            label="角色"
            name="roleId"
          >
            <Select placeholder="选择角色">
              {
                roleList.map((item, index) => {
                  return <Option key={index} value={item.id}>{item.name}</Option>
                })
              }
            </Select>
          </Form.Item>
          <Form.Item
            label="员工"
            name="employeeId"
          >
            <Select placeholder="选择员工">
              {
                employeeList.map((item, index) => {
                  return <Option key={index} value={item.id}>{item.realName}</Option>
                })
              }
            </Select>
          </Form.Item>
          <Form.Item
            label="状态"
            name="status"
            rules={[
              {
                required: true,
                message: '请选择状态！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>正常</Radio>
              <Radio value={1}>锁定</Radio>
            </Radio.Group>
          </Form.Item>
          {/* <Form.Item label="头像"
            name="roleId">
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              fileList={fileList}
              maxCount={1}
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={handleChange}
            >
              {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
          </Form.Item> */}
        </Form>
      </Spin>
    </Modal>
  );
};

export default Home;