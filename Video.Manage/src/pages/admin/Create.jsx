import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, message, Modal, Radio } from 'antd';
import { getRoleList } from '@/api/role'
import { createAdmin } from '@/api/admin'
import { getEmployeeList } from '@/api/employee'
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
  const { title, isVisible, callbackParent } = props
  const [form] = Form.useForm()
  const [visible, setVisible] = useState(isVisible)
  // 状态控制
  const [loading, setLoading] = useState(false);
  const [roleList, setRoleList] = useState([])
  const [employeeList, setEmployeeList] = useState([])

  useEffect(async () => {
    let res = await getRoleList()
    if (res.success) {
      setRoleList(res.data.data)
    } else {
      message.error(res.message)
    }

    let res2 = await getEmployeeList()
    if (res2.success) {
      setEmployeeList(res2.data.data)
    } else {
      message.error(res2.message)
    }

    form.setFieldsValue({ gender: 0 })
  }, [])

  // 提交表单
  const handleOk = async (values) => {
    form.validateFields().then(values => {
      setLoading(true);
      createAdmin(values).then(res => {
        setLoading(false);
        if (res.success) {
          message.success('管理员创建成功！');
          form.resetFields()
          callbackParent(); // 关闭父容器
        } else {
          message.error(res.message)
        }
      });
    })
  };

  //取消
  const handleCancel = () => {
    callbackParent(1); // 关闭父容器
  }

  // 表单布局
  const layout = {
    labelCol: {
      span: 5
    },
    wrapperCol: {
      span: 18
    }
  };

  return (
    <Modal
      forceRender
      width={700}
      title={title}
      onOk={handleOk}
      visible={isVisible}
      onCancel={handleCancel}
    >
      <Spin spinning={loading}>
        <Form
          {...layout}
          name="AdminCreate"
          form={form}
        >
          <Form.Item
            label="名字"
            name="name"
            rules={[
              {
                required: true,
                message: '请输入管理员名字！'
              }
            ]}
          >
            <Input allowClear maxLength={10} placeholder="输入管理员名字" />
          </Form.Item>
          <Form.Item
            label="账号"
            name="account"
            rules={[
              {
                required: true,
                message: '请输入管理员账号！'
              }
            ]}
          >
            <Input allowClear maxLength={20} placeholder="输入管理员账号" />
          </Form.Item>
          <Form.Item
            label="性别"
            name="gender"
            rules={[
              {
                required: true,
                message: '请选择管理员性别！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>未知</Radio>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            label="角色"
            name="roleId"
          >
            <Select placeholder="选择角色">
              {
                roleList.map((item, index) => {
                  return <Option key={index} value={item.id}>{item.name}</Option>
                })
              }
            </Select>
          </Form.Item>
          <Form.Item
            label="员工"
            name="employeeId"
          >
            <Select placeholder="选择员工">
              {
                employeeList.map((item, index) => {
                  return <Option key={index} value={item.id}>{item.realName}</Option>
                })
              }
            </Select>
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default Home;