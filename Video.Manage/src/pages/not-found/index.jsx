/*
 * @Author: liyong
 * @Date: 2021-03-11 21:52:56
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-04 15:54:15
 * @Description: 404
 */
import { Component } from 'react'
import './Common.less'
import cloud404 from '../../assets/images/404_images/404_cloud.png'
import cloud from '../../assets/images/404_images/404.png'

class NotFound extends Component {

    render() {
        return (
            <div className="wscn-http404-container">
                <div className="wscn-http404">
                    <div className="pic-404">
                        <img className="pic-404__parent" src={cloud} alt="404" />
                        <img className="pic-404__child left" src={cloud404} alt="404" />
                        <img className="pic-404__child mid" src={cloud404} alt="404" />
                        <img className="pic-404__child right" src={cloud404} alt="404" />
                    </div>
                    <div className="bullshit">
                        <div className="bullshit__oops">OOPS!</div>
                        <div className="bullshit__info">All rights reserved
                        </div>
                        <div className="bullshit__headline">页面不存在！</div>
                        <div className="bullshit__info">Please check that the URL you entered is correct, or click the button below to return to the homepage.</div>
                        <a href="/" className="bullshit__return-home">返回首页</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default NotFound