import React, { Component } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { observer, inject } from 'mobx-react'
import md5 from 'md5'
import './Common.less'
@inject('user', 'permission') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      rediectToReferrer: false // 是否重定向之前的界面
    }
  }
  login = async (values: any) => {
    this.setState({ loading: true })
    let code = await this.props.user.login(values)
    this.setState({ loading: false })
    // 若登录成功
    if (code === true) {
      // 获取当前用户信息
      await this.props.user.getUserInfo()
      // 获取当前用户菜单数据
      await this.props.permission.getMenus()
      // const lastHref = localStorage.getItem('last-href')
      // window.location.href = lastHref || '/'

      this.setState({
        rediectToReferrer: true
      });
      let RedirectUrl = this.props.location.state
        ? this.props.location.state.from.pathname
        : '/';
      // 登陆成功之后的跳转
      this.props.history.push(RedirectUrl);
    }
  };
  render () {
    const layout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 16 }
    };
    const tailLayout = {
      wrapperCol: { offset: 11, span: 16 }
    };
    const { loading } = this.state
    return (
      <div className="login-bg">
        <div className="login-title">   后台管理系统</div>
        <Form style={{ margin: '10rem auto', width: '40rem' }}
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={this.login}
        >
          <Form.Item
            label="账户"
            name="account"
            rules={[{ required: true, message: '请输入账户' }]}
          >
            <Input placeholder="请输入账号" />
          </Form.Item>

          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码' }]}
          >
            <Input.Password placeholder="请输入密码" />
          </Form.Item>

          {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>记住密码</Checkbox>
                    </Form.Item> */}

          <Form.Item {...tailLayout}>
            <Button loading={loading} type="primary" htmlType="submit">
              登录
                        </Button>
          </Form.Item>
        </Form>
      </div>
    )
  }
}

export default LoginForm