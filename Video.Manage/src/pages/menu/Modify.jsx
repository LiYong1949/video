import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, Radio, message, Modal, TreeSelect } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { getMenuInfo, modifyMenu, getMenuTree } from '@/api/menu'
import { getAuthButtonEnumList } from '@/api/role';
const { TreeNode } = TreeSelect;
const { Option } = Select;
const Home = (props) => {
    const { id, isVisible, title, callbackParent } = props
    const [form] = Form.useForm()
    const [visible, setVisible] = useState(isVisible)
    // 状态控制
    const [loading, setLoading] = useState(false)
    const [imageUrl, setImageUrl] = useState('')
    const [data, setData] = useState({})
    const [treeData, setTreeData] = useState([])
    const [parentName, setParentName] = useState('')
    const [authButtonList, setAuthButtonList] = useState([]);
    const [authButtons, setAuthButtons] = useState([]);
    useEffect(async () => {
        let dataSource = {}
        let hasParent = false
        if (id > 0) {
            let res2 = await getMenuInfo(id)
            if (res2.success) {
                dataSource = res2.data
                setData(res2.data)
                form.setFieldsValue(res2.data)

            } else {
                message.error(res2.message)
            }
        }
        //构造菜单数据
        const generateMenuTree = (array = []) => {
            let tree = []
            if (array.length > 0) {
                array.map((item, index) => {
                    let children = []
                    if (item.children.length > 0) {
                        children = generateMenuTree(item.children)
                    }
                    if (item.id === dataSource.parentId) {
                        hasParent = true
                        setParentName(item.name)
                    }
                    if (item.id !== dataSource.id) {
                        tree.push({ value: item.id, title: item.name, children: children })
                    }
                })
            }
            return tree
        }
        let res = await getMenuTree()
        if (res.success) {
            setTreeData(generateMenuTree(res.data))
        }

        //获取权限菜单选择器数据
        let res3 = await getAuthButtonEnumList()
        if (res3.success) {
            setAuthButtonList(res3.data)
        }
        let btns = dataSource.authButtons ? dataSource.authButtons.split(',') : []
        setAuthButtons(btns)
        if (!hasParent) {
            setParentName('')
        }
    }, [id])



    // 提交表单
    const handleOk = async () => {
        setLoading(true);
        form.validateFields().then(values => {
            setLoading(false);
            values.parentId = data.parentId
            values.authButtons = authButtons.join(',')
            modifyMenu(id, values).then(res => {
                setLoading(false);
                if (res.success) {
                    message.success('菜单修改成功！');
                    // form.resetFields()
                    callbackParent(); // 关闭父容器
                } else {
                    message.error(res.message);
                }
            });
        })
    };

    //取消
    const handleCancel = () => {
        callbackParent(1); // 关闭父容器
    }
    // 父级菜单变更
    const handleParentMenuChange = (value, label, extra) => {
        let data2 = data
        data2.parentId = value || 0
        setData(data2)
        setParentName(label[0]);
    };

    //权限按钮变更
    const handleAuthButtonChange = (value) => {
        console.log(value);
        setAuthButtons(value)
    }

    // 表单布局
    const layout = {
        labelCol: {
            span: 5
        },
        wrapperCol: {
            span: 18
        }
    };

    return (
        < Modal
            forceRender
            width={700}
            title={title}
            onOk={handleOk}
            visible={isVisible}
            onCancel={handleCancel}
        >
            <Spin spinning={loading}>
                <Form
                    {...layout}
                    name="MenuModify"
                    form={form}
                >
                    <Form.Item
                        label="父级菜单"
                        name="parentId"
                    >
                        <TreeSelect
                            style={{ width: '100%' }}
                            value={data.parentId === 0 ? '' : data.parentId}
                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                            placeholder="父级菜单"
                            allowClear
                            treeData={treeData}
                            treeDefaultExpandAll
                            onChange={handleParentMenuChange}
                        >
                        </TreeSelect>
                    </Form.Item>
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: '请输入菜单名称！'
                            }
                        ]}
                    >
                        <Input allowClear maxLength={10} placeholder="输入菜单名称" />
                    </Form.Item>
                    <Form.Item
                        label="图标"
                        name="icon"
                    >
                        <Input allowClear maxLength={20} placeholder="输入图标" />
                    </Form.Item>
                    <Form.Item
                        label="序号"
                        name="sort"
                    >
                        <Input allowClear placeholder="输入序号" />
                    </Form.Item>
                    <Form.Item
                        label="权限按钮"
                    >
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}
                            placeholder="选择权限按钮"
                            value={authButtons}
                            onChange={handleAuthButtonChange}
                        >
                            {
                                authButtonList.map((item, index) => {
                                    return <Option key={index} value={item.value}>{item.description}</Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="根菜单"
                        name="isGroup"
                        rules={[
                            {
                                required: true,
                                message: '请选择是否根菜单！'
                            }
                        ]}
                    >
                        <Radio.Group>
                            <Radio value={true}>是</Radio>
                            <Radio value={false}>否</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        label="是否显示"
                        name="visible"
                        rules={[
                            {
                                required: true,
                                message: '请选择是否显示菜单！'
                            }
                        ]}
                    >
                        <Radio.Group>
                            <Radio value={true}>显示</Radio>
                            <Radio value={false}>隐藏</Radio>
                        </Radio.Group>
                    </Form.Item>
                </Form>
            </Spin>
        </Modal >
    );
};

export default Home;