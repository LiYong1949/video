import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, message, Modal, Radio, TreeSelect } from 'antd';
import { createMenu, getMenuTree } from '@/api/menu'
import { getAuthButtonEnumList } from '@/api/role';
const { Option } = Select;
const Home = (props) => {
    const { title, isVisible, callbackParent } = props
    const [form] = Form.useForm()
    const [visible, setVisible] = useState(isVisible)
    // 状态控制
    const [loading, setLoading] = useState(false);
    const [treeData, setTreeData] = useState([])
    const [parentId, setParentId] = useState(0);
    const [parnentName, setParentName] = useState('');
    const [authButtonList, setAuthButtonList] = useState([]);
    const [authButtons, setAuthButtons] = useState([]);

    useEffect(async () => {
        const generateMenuTree = (array = []) => {
            let tree = []
            if (array.length > 0) {
                array.map((item, index) => {
                    let children = []
                    if (item.children.length > 0) {
                        children = generateMenuTree(item.children)
                    }
                    tree.push({ value: item.id, title: item.name, children: children })
                })
            }
            return tree
        }
        let res = await getMenuTree()
        if (res.success) {
            setTreeData(generateMenuTree(res.data))
        }
        let res2 = await getAuthButtonEnumList()
        if (res2.success) {
            setAuthButtonList(res2.data)
        }
        form.setFieldsValue({ isGroup: false, visible: true })
    }, [])

    // 提交表单
    const handleOk = async (values) => {
        form.validateFields().then(values => {
            setLoading(true);
            values.parentId = parentId
            values.authButtons = authButtons.join(',')
            createMenu(values).then(res => {
                setLoading(false);
                if (res.success) {
                    message.success('菜单创建成功！');
                    form.resetFields()
                    callbackParent(); // 关闭父容器
                } else {
                    message.error(res.message)
                }
            });
        })
    };

    //取消
    const handleCancel = () => {
        setAuthButtons([])
        callbackParent(1); // 关闭父容器
    }
    // 父级菜单变更
    const handleParentMenuChange = (value, label, extra) => {
        setParentId(value || 0)
        setParentName(label[0]);
    };

    //权限按钮变更
    const handleAuthButtonChange = (value) => {
        setAuthButtons(value)
    }

    // 表单布局
    const layout = {
        labelCol: {
            span: 5
        },
        wrapperCol: {
            span: 18
        }
    };

    return (
        <Modal
            forceRender
            width={700}
            title={title}
            onOk={handleOk}
            visible={isVisible}
            onCancel={handleCancel}
        >
            <Spin spinning={loading}>
                <Form
                    {...layout}
                    name="MenuCreate"
                    form={form}
                >
                    <Form.Item
                        label="父级菜单"
                    >
                        <TreeSelect
                            style={{ width: '100%' }}
                            value={parentId === 0 ? '' : parentId}
                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                            placeholder="父级菜单"
                            allowClear
                            treeData={treeData}
                            treeDefaultExpandAll
                            onChange={handleParentMenuChange}
                        >
                        </TreeSelect>
                    </Form.Item>
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: '请输入菜单名称！'
                            }
                        ]}
                    >
                        <Input allowClear maxLength={10} placeholder="输入菜单名称" />
                    </Form.Item>
                    <Form.Item
                        label="图标"
                        name="icon"
                    >
                        <Input allowClear maxLength={20} placeholder="输入图标" />
                    </Form.Item>
                    <Form.Item
                        label="序号"
                        name="sort"
                    >
                        <Input allowClear placeholder="输入序号" />
                    </Form.Item>
                    <Form.Item
                        label="按钮"
                        name="authButtons"
                    >
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}
                            placeholder="选择权限按钮"
                            onChange={handleAuthButtonChange}
                        >
                            {
                                authButtonList.map((item, index) => {
                                    return <Option key={index} value={item.value}>{item.description}</Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="根菜单"
                        name="isGroup"
                        rules={[
                            {
                                required: true,
                                message: '请选择是否根菜单！'
                            }
                        ]}
                    >
                        <Radio.Group>
                            <Radio value={true}>是</Radio>
                            <Radio value={false}>否</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        label="是否显示"
                        name="visible"
                        rules={[
                            {
                                required: true,
                                message: '请选择是否显示菜单！'
                            }
                        ]}
                    >
                        <Radio.Group>
                            <Radio value={true}>显示</Radio>
                            <Radio value={false}>隐藏</Radio>
                        </Radio.Group>
                    </Form.Item>
                </Form>
            </Spin>
        </Modal>
    );
};

export default Home;