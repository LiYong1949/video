/*
 * @Description:菜单
 * @Author: liyong
 * @Date: 2021-06-15 13:57:33
 * @LastEditTime: 2021-06-15 20:06:39
 * @LastEditors: liyong
 */
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react'
import LayoutUnit from '@/components/Layout'
import { Table, Button, message, Modal, Tag, Tree } from 'antd';
import MenuCreate from './Create';
import MenuModify from './Modify';
import { getMenuTree, deleteMenu } from '@/api/menu'
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      dataSource: [],
      create: {
        title: '添加菜单',
        visible: false
      },
      modify: {
        title: '更新菜单',
        visible: false,
        id: 0
      },
      treeOptions: {
        expandedKeys: [],
        checkedKeys: [],
        selectedKeys: [],
        autoExpandParent: false
      }
    };
  }

  componentDidMount() {
    this.handleGetMenus();
  }
  //获取菜单列表数据
  handleGetMenus = async (type) => {
    let { create, modify, pagination, sorter } = this.state;
    create.visible = false;
    modify.visible = false;
    this.setState({ create, modify });
    if (type !== 1) { // 取消操作的回调
      this.setState({ loading: true })
      let res = await getMenuTree();
      if (res.success) {
        this.setState({
          dataSource: this.generateMenuTree(res.data),
          loading: false
        });
      } else {
        message.error(res.message)
      }
    }
  }

  // 构造菜单树数据
  generateMenuTree = (data = []) => {
    let tree = []
    if (data.length > 0) {
      data.map((item, index) => {
        let children = []
        if (item.children.length > 0) {
          children = this.generateMenuTree(item.children)
        }
        tree.push({ key: item.id, title: item.name, children: children })
      })
    }
    return tree
  }
  //操作
  handleVisibleModal = (type, record) => {
    let _this = this
    let { create, modify, selectedRowKeys,treeOptions } = this.state;
    switch (type) {
      case 'createMenu':
        create.visible = true
        break;
      case 'modifyMenu':
        modify.visible = true
        modify.id = record.id
        break;
      case 'deleteMenu':
        if (treeOptions.checkedKeys.length > 0) {
          Modal.confirm({
            title: '删除菜单',
            content: '确认要删除勾选的菜单？',
            onOk() {
              deleteMenu(treeOptions.checkedKeys.join(',')).then(res => {
                if (res.success) {
                  message.success('删除成功！')
                  _this.handleGetMenus()
                } else {
                  message.error(res.message)
                }
              })
            }
          })
        } else {
          message.warning('未勾选！')
        }
        break;
      case 'cancel':
        break;
      default:
        break;
    }
    this.setState({
      create,
      modify
    });
  }

  onExpand = (expandedKeysValue: React.Key[]) => {
    let { treeOptions } = this.state
    treeOptions.expandedKeys = expandedKeysValue
    // if not set autoExpandParent to false, if children expanded, parent can not collapse.
    // or, you can remove all expanded children keys.
    this.setState({ treeOptions })
  };

  onCheck = (checkedKeysValue: React.Key[]) => {
    let { treeOptions } = this.state
    treeOptions.checkedKeys = checkedKeysValue
    this.setState({ treeOptions })
  };

  onSelect = (selectedKeysValue: React.Key[], info: any) => {
    let { treeOptions, modify } = this.state
    treeOptions.selectedKeys = selectedKeysValue
    modify.id = info.node.key
    modify.visible = true
    this.setState({ treeOptions, modify })
  };

  onDrop = info => {
    console.log(info);
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data, key, callback) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === key) {
          return callback(data[i], i, data);
        }
        if (data[i].children) {
          loop(data[i].children, key, callback);
        }
      }
    };
    const data = [...this.state.dataSource];

    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, item => {
        item.children = item.children || [];
        // where to insert 示例添加到头部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, item => {
        item.children = item.children || [];
        // where to insert 示例添加到头部，可以是随意位置
        item.children.unshift(dragObj);
        // in previous version, we use item.children.push(dragObj) to insert the
        // item to the tail of the children
      });
    } else {
      let ar;
      let i;
      loop(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }
    this.setState({ dataSource: data })
  }
  render() {
    return (<div>
      <div style={{ marginBottom: 15 }}>
        <Button type="primary" style={{ marginRight: 8 }} onClick={this.handleVisibleModal.bind(this, 'createMenu')}>新建菜单</Button>
        <Button type="primary" onClick={this.handleVisibleModal.bind(this, 'deleteMenu')}>删除菜单</Button>
      </div>
      <Tree
        checkable
        draggable
        onExpand={this.onExpand}
        expandedKeys={this.state.treeOptions.expandedKeys}
        autoExpandParent={false}
        onCheck={this.onCheck}
        checkedKeys={this.state.treeOptions.checkedKeys}
        onSelect={this.onSelect}
        selectedKeys={this.state.treeOptions.selectedKeys}
        treeData={this.state.dataSource}
        onDrop={this.onDrop}
      />
      <MenuCreate isVisible={this.state.create.visible} width={500} title={this.state.create.title} callbackParent={this.handleGetMenus}></MenuCreate>
      <MenuModify id={this.state.modify.id} isVisible={this.state.modify.visible} width={500} title={this.state.modify.title} callbackParent={this.handleGetMenus}></MenuModify>
    </div>)
  }
}

export default LayoutUnit(HomePage)