/*
 * @Description:角色授权
 * @Author: liyong
 * @Date: 2021-06-16 10:45:40
 * @LastEditTime: 2021-06-21 21:28:18
 * @LastEditors: liyong
 */
import React, { Component, useState, useEffect } from 'react';
import { Table, Divider, Tag, Button, Checkbox } from 'antd';
import { createRole, getPermissionList, getAuthButtonEnumList } from '@/api/role'
import { getMenuTree } from '@/api/menu'

const Home = (props) => {
    const { ids, title, isVisible, callbackParent } = props
    const [columns, setColumns] = useState([])
    const [checkStrictly, setCheckStrictly] = React.useState(false);
    const [visible, setVisible] = useState(isVisible)
    // 状态控制
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([])
    const [checkedKeys, setCheckedKeys] = useState([])
    useEffect(async () => {
        let authButtons = []
        let res3 = await getAuthButtonEnumList()
        if (res3.success) {
            authButtons = res3.data
        }

        //构造菜单数据
        const generateMenuTree = (array = []) => {
            let tree = []
            if (array.length > 0) {
                array.map((item, index) => {
                    let children = []
                    if (item.children.length > 0) {
                        children = generateMenuTree(item.children)
                    }
                    let data = { code: item.permissionCode }
                    authButtons.map(auth => {
                        data.auth.name = true
                    })
                    tree.push({ key: item.permissionCode, title: item.name, children: children })
                })
            }
            return tree
        }
        let res = await getMenuTree()
        if (res.success) {
            setData(generateMenuTree(res.data))
        }
        if (ids.length === 1) {
            let res2 = await getPermissionList(ids[0])
            if (res2.success) {
                let authArray = []
                if (res2.data && res2.data.length > 0) {
                    res2.data.map(item => {
                        authArray.push(item.permissionCode)
                    })
                }
                setCheckedKeys(authArray)
            }
        }


    }, [ids])

    const handleOk = () => {

    }

    //取消
    const handleCancel = () => {
        callbackParent(1); // 关闭父容器
    }

    const onCheck = (checkedKeysValue: React.Key[]) => {
        setCheckedKeys(checkedKeysValue)
    };

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        }
    };

    return (
        <Modal
            forceRender
            width={500}
            title={title}
            onOk={handleOk}
            visible={isVisible}
            onCancel={handleCancel}
        >
            <Spin spinning={loading}>

                <Table
                    columns={columns}
                    rowSelection={{ ...rowSelection, checkStrictly }}
                    dataSource={data}
                />

            </Spin>
        </Modal>
    );
};

export default Home;