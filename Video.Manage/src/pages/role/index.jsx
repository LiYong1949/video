import React, { Component } from 'react';
import { observer, inject } from 'mobx-react'
import LayoutUnit from '@/components/Layout'
import { Table, Button, message, Modal, Tag } from 'antd';
import RoleCreate from './Create';
import RoleModify from './Modify';
import RoleAuth from './Auth';
import { getRolePageList, deleteRole } from '@/api/role'
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            total: 0,
            dataSource: [],
            columns: [],
            create: {
                title: '添加角色',
                visible: false
            },
            modify: {
                title: '更新角色',
                visible: false,
                id: 0
            },
            auth: {
                title: '角色授权',
                visible: false,
                ids: [],
                user:this.props.user.userInfo
            },
            selectedRowKeys: []
        };
    }

    componentDidMount() {
        this.handleGetRoles();
    }
    //获取角色列表数据
    handleGetRoles = async (type) => {
        let { create } = this.state;
        let { modify } = this.state;
        let { auth } = this.state;
        create.visible = false;
        modify.visible = false;
        auth.visible = false;
        this.setState({ create, modify, auth });
        if (type !== 1) { // 取消操作的回调
            this.setState({ loading: true })
            let res = await getRolePageList();
            if (res.success) {
                let columns = [
                    {
                        title: '序号',
                        dataIndex: 'index',
                        render: (text, record, index) => {
                            return index + 1;
                        }
                    },
                    {
                        title: '名字',
                        dataIndex: 'name'
                    },
                    {
                        title: '状态',
                        dataIndex: 'status',
                        render: (text, record) => {
                            let color = text === 0 ? 'green' : 'red'
                            return (
                                <Tag color={color} key={text}>
                                    {text === 0 ? '正常' : '锁定'}
                                </Tag>
                            )
                        }
                    },
                    {
                        title: '创建时间',
                        dataIndex: 'createTime'
                    },
                    {
                        title: '操作',
                        dataIndex: 'options',
                        render: (text, record) => {
                            return (
                                <>
                                    <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'modifyRole', record)}>修改</Button>
                                    <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'deleteRole', record)}>删除</Button>
                                    <Button size="small" type="link" onClick={this.handleVisibleModal.bind(this, 'authRole', record)}>授权</Button>
                                </>
                            )
                        }
                    }
                ];
                this.setState({
                    columns,
                    total: res.data.total,
                    dataSource: res.data.data,
                    loading: false
                });
            } else {
                message.error(res.message)
            }
        }
    }
    //操作
    handleVisibleModal = (type, record) => {
        let _this = this
        let { create } = this.state;
        let { modify } = this.state;
        let { auth } = this.state;
        let { selectedRowKeys } = this.state;
        switch (type) {
            case 'createRole':
                create.visible = true
                break;
            case 'modifyRole':
                modify.visible = true
                modify.id = record.id
                break;
            case 'deleteRole':
                if (record.id) {
                    Modal.confirm({
                        title: '删除角色',
                        content: '确认要删除该角色？',
                        onOk() {
                            deleteRole(record.id).then(res => {
                                if (res.success) {
                                    message.success('删除成功！')
                                    _this.handleGetRoles()
                                } else {
                                    message.error(res.message)
                                }
                            })
                        }
                    })
                }
                else {
                    if (selectedRowKeys.length > 0) {
                        Modal.confirm({
                            title: '删除角色',
                            content: '确认要删除勾选的角色？',
                            onOk() {
                                deleteRole(selectedRowKeys.join(',')).then(res => {
                                    if (res.success) {
                                        message.success('删除成功！')
                                        _this.handleGetRoles()
                                    } else {
                                        message.error(res.message)
                                    }
                                })
                            }
                        })
                    } else {
                        message.warning('未勾选！')
                    }
                }
                break;
            case 'authRole':
                if (record.id) {
                    auth.ids = [record.id]
                } else {
                    if (selectedRowKeys.length > 0) {
                        auth.ids = selectedRowKeys
                    } else {
                        message.warning('未勾选！')
                        return false
                    }
                }
                auth.visible = true
            case 'cancel':
                break;
            default:
                break;
        }
        this.setState({
            create,
            modify,
            auth
        });
    }

    //勾选checkbox
    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys: selectedRowKeys });
    };
    render() {
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };
        return (<div>
            <div style={{ marginBottom: 15 }}>
                <Button type="primary" style={{ marginRight: 8 }} onClick={this.handleVisibleModal.bind(this, 'createRole')}>新建角色</Button>
                {/* <small className="d-text-gray">（当前共有角色{this.state.total}个）</small> */}
                <Button type="primary" style={{ marginRight: 8 }} onClick={this.handleVisibleModal.bind(this, 'deleteRole')}>删除角色</Button>
                <Button type="primary" onClick={this.handleVisibleModal.bind(this, 'authRole')}>角色授权</Button>
            </div>
            <Table
                rowSelection={rowSelection}
                className="d-table-center-container"
                dataSource={this.state.dataSource}
                columns={this.state.columns}
                loading={this.state.loading}
                rowKey={record => record.id}
                bordered
                pagination={{ pageSize: 20 }}
            />

            <RoleCreate isVisible={this.state.create.visible} width={500} title={this.state.create.title} callbackParent={this.handleGetRoles}></RoleCreate>
            <RoleModify id={this.state.modify.id} isVisible={this.state.modify.visible} width={500} title={this.state.modify.title} callbackParent={this.handleGetRoles}></RoleModify>
            <RoleAuth ids={this.state.auth.ids} isVisible={this.state.auth.visible} width={500} title={this.state.auth.title} callbackParent={this.handleGetRoles} user={this.state.auth.user}></RoleAuth>

        </div>)
    }
}

export default LayoutUnit(HomePage)