/*
 * @Author: liyong
 * @Date: 2021-06-08 20:57:04
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-16 16:53:35
 * @Description: 角色更新
 */
import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, Radio, message, Modal, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { getRoleInfo, modifyRole } from '@/api/role'
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
    const { id, isVisible, title, callbackParent } = props
    const [form] = Form.useForm()
    const [visible, setVisible] = useState(isVisible)
    // 状态控制
    const [loading, setLoading] = useState(false)
    const [imageUrl, setImageUrl] = useState('')
    const [fileList, setFileList] = useState([])
    const [data, setData] = useState({})
    const [roleList, setRoleList] = useState([])

    useEffect(async () => {

        if (id > 0) {
            let res = await getRoleInfo(id)
            if (res.success) {
                form.setFieldsValue(res.data)
                return res.data
            } else {
                message.error(res.message)
            }
        }

    }, [id])

    // 提交表单
    const handleOk = async () => {
        setLoading(true);
        form.validateFields().then(values => {
            setLoading(false);
            modifyRole(id, values).then(res => {
                setLoading(false);
                if (res.success) {
                    message.success('角色修改成功！');
                    // form.resetFields()
                    callbackParent(); // 关闭父容器
                } else {
                    message.error(res.message);
                }
            });
        })
    };

    //取消
    const handleCancel = () => {
        callbackParent(1); // 关闭父容器
    }


    // 表单布局
    const layout = {
        labelCol: {
            span: 5
        },
        wrapperCol: {
            span: 18
        }
    };

    return (
        <Modal
            forceRender
            width={500}
            title={title}
            onOk={handleOk}
            visible={isVisible}
            onCancel={handleCancel}
        >
            <Spin spinning={loading}>
                <Form
                    {...layout}
                    name="RoleModify"
                    form={form}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: '请输入角色名称！'
                            }
                        ]}
                    >
                        <Input allowClear maxLength={10} placeholder="输入角色名称" />
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                    >
                        <TextArea allowClear maxLength={200} placeholder="输入角色备注" />
                    </Form.Item>
                    <Form.Item
                        label="状态"
                        name="status"
                        rules={[
                            {
                                required: true,
                                message: '请选择状态！'
                            }
                        ]}
                    >
                        <Radio.Group>
                            <Radio value={0}>正常</Radio>
                            <Radio value={1}>锁定</Radio>
                        </Radio.Group>
                    </Form.Item>
                </Form>
            </Spin>
        </Modal>
    );
};

export default Home;