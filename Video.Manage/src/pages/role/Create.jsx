/*
 * @Author: liyong
 * @Date: 2021-06-08 20:57:04
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-09 17:13:30
 * @Description: 角色添加
 */
import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, message, Modal, Radio } from 'antd';
import { createRole } from '@/api/role'
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
    const { title, isVisible, callbackParent } = props
    const [form] = Form.useForm()
    const [visible, setVisible] = useState(isVisible)
    // 状态控制
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState({
        roleList: []
    })

    useEffect(async () => {

        form.setFieldsValue({ gender: 0 })
    }, [])

    // 提交表单
    const handleOk = async (values) => {
        setLoading(true)
        form.validateFields().then(values => {
            setLoading(false);
            createRole(values).then(res => {
                setLoading(false);
                if (res.success) {
                    message.success('角色创建成功！');
                    form.resetFields()
                    callbackParent(); // 关闭父容器
                } else {
                    message.error(res.message)
                }
            });
        })
    };

    //取消
    const handleCancel = () => {
        callbackParent(1); // 关闭父容器
    }

    // 表单布局
    const layout = {
        labelCol: {
            span: 5
        },
        wrapperCol: {
            span: 18
        }
    };

    return (
        <Modal
            forceRender
            width={500}
            title={title}
            onOk={handleOk}
            visible={isVisible}
            onCancel={handleCancel}
        >
            <Spin spinning={loading}>
                <Form
                    {...layout}
                    name="RoleCreate"
                    form={form}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: '请输入角色名称！'
                            }
                        ]}
                    >
                        <Input allowClear maxLength={10} placeholder="输入角色名称" />
                    </Form.Item>
                    <Form.Item
                        label="备注"
                        name="remark"
                    >
                        <TextArea allowClear maxLength={200} placeholder="输入角色备注" />
                    </Form.Item>

                </Form>
            </Spin>
        </Modal>
    );
};

export default Home;