import React, { Component } from 'react';
import { Layout, Menu, Tooltip, Popover } from 'antd';
import { Link } from 'react-router-dom';
import './home.less'
import { HomeOutlined, TeamOutlined, AppstoreOutlined, LaptopOutlined, ToolOutlined, MenuUnfoldOutlined, MenuFoldOutlined, RetweetOutlined } from '@ant-design/icons'
import { observer, inject } from 'mobx-react'
import LayoutUnit from '../../components/Layout'


const { Header, Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false
        };
    }
    render() {
        return (<div>首页</div>)
    }
}

export default LayoutUnit(Home)