import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, message, Modal, Radio } from 'antd';
import { createEmployee } from '@/api/employee'
import moment from 'moment'
import 'moment/locale/zh-cn'
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
  const { title, isVisible, callbackParent } = props
  const [form] = Form.useForm()
  const [visible, setVisible] = useState(isVisible)
  // 状态控制
  const [loading, setLoading] = useState(false);

  useEffect(async () => {

    form.setFieldsValue({ gender: 0, entryTime: moment(new Date()) })
  }, [])

  // 提交表单
  const handleOk = async (values) => {
    form.validateFields().then(values => {
      setLoading(true);
      createEmployee(values).then(res => {
        setLoading(false);
        if (res.success) {
          message.success('员工创建成功！');
          form.resetFields()
          callbackParent(); // 关闭父容器
        } else {
          message.error(res.message)
        }
      });
    })
  };

  //取消
  const handleCancel = () => {
    callbackParent(1); // 关闭父容器
  }

  // 表单布局
  const layout = {
    labelCol: {
      span: 5
    },
    wrapperCol: {
      span: 18
    }
  };

  return (
    <Modal
      forceRender
      width={700}
      title={title}
      onOk={handleOk}
      visible={isVisible}
      onCancel={handleCancel}
    >
      <Spin spinning={loading}>
        <Form
          {...layout}
          name="EmployeeCreate"
          form={form}
        >
          <Form.Item
            label="姓名"
            name="realName"
            rules={[
              {
                required: true,
                message: '请输入员工姓名！'
              }
            ]}
          >
            <Input allowClear maxLength={10} placeholder="输入员工名字" />
          </Form.Item>
          <Form.Item
            label="手机号"
            name="mobile"
            rules={[{ required: true, pattern: /^1[3|4|5|7|8][0-9]\d{8}$/, message: '请输入正确的手机号' }]}
          >
            <Input allowClear maxLength={20} placeholder="输入手机号" />
          </Form.Item>
          <Form.Item
            label="邮箱"
            name="email" rules={[{ pattern: /^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*\.[a-z]{2,}$/, message: '请输入正确的邮箱' }]}
          >
            <Input allowClear maxLength={20} placeholder="输入邮箱" />
          </Form.Item>
          <Form.Item
            label="性别"
            name="gender"
            rules={[
              {
                required: true,
                message: '请选择员工性别！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>未知</Radio>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="入职时间"
            name="entryTime">
            <DatePicker picker="date" />
          </Form.Item>
          <Form.Item label="生日"
            name="birthday">
            <DatePicker picker="date" />
          </Form.Item>
          <Form.Item
            label="备注"
            name="remark"
          >
            <TextArea allowClear maxLength={200} placeholder="输入员工备注" />
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default Home;