/*
 * @Description:员工
 * @Author: liyong
 * @Date: 2021-06-11 15:18:21
 * @LastEditTime: 2021-06-15 11:47:25
 * @LastEditors: liyong
 */
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react'
import LayoutUnit from '@/components/Layout'
import { Table, Button, message, Modal, Tag } from 'antd';
import EmployeeCreate from './Create';
import EmployeeModify from './Modify';
import { getEmployeePageList, deleteEmployee } from '@/api/employee'
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      total: 0,
      dataSource: [],
      columns: [],
      create: {
        title: '添加员工',
        visible: false
      },
      modify: {
        title: '更新员工',
        visible: false,
        id: 0
      },
      sorter: {// 排序
        columnKey: 'createTime',
        order: 'descend'
      },
      selectedRowKeys: [],// 勾选
      pagination: { // 分页
        current: 1, // 当前页
        pageSize: 20, // 每页显示条数
        total: 0 // 总数
      }
    };
  }

  componentDidMount () {
    this.handleGetEmployees();
  }
  //获取员工列表数据
  handleGetEmployees = async (type) => {
    let { create, modify, pagination, sorter } = this.state;
    create.visible = false;
    modify.visible = false;
    this.setState({ create, modify });
    if (type !== 1) { // 取消操作的回调
      this.setState({ loading: true })
      let res = await getEmployeePageList(Object.assign({ pageIndex: pagination.index, pageSize: pagination.size, sort: 'CreateTime DESC' }));
      if (res.success) {
        let columns = [
          {
            title: '序号',
            dataIndex: 'index',
            render: (text, record, index) => {
              return index + 1;
            }
          },
          {
            title: '姓名',
            key: 'realName',
            dataIndex: 'realName'
          },
          {
            title: '手机号',
            key: 'mobile',
            dataIndex: 'mobile'
          },
          {
            title: '性别',
            key: 'gender',
            dataIndex: 'gender',
            render: (text, record) => {
              return text !== 0 ? (text === 1 ? '男' : '女') : '未知'
            }
          },
          {
            title: '状态',
            dataIndex: 'status',
            render: (text, record) => {
              let color = text === 0 ? 'green' : 'red'
              return (
                <Tag color={color} key={text}>
                  {text === 0 ? '正常' : '离职'}
                </Tag>
              )
            }
          },
          {
            title: '创建时间',
            key: 'createTime',
            dataIndex: 'createTime',
            defaultSortOrder: 'descend',
            sorter: (a, b) => {
              let aTime = new Date(a.createTime).getTime();
              let bTime = new Date(b.createTime).getTime();
              return aTime - bTime;
            }
          },
          {
            title: '操作',
            dataIndex: 'options',
            render: (text, record) => {
              return (
                <>
                  <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'modifyEmployee', record)}>修改</Button>
                  <Button size="small" type="link" danger onClick={this.handleVisibleModal.bind(this, 'deleteEmployee', record)}>删除</Button>
                </>
              )
            }
          }
        ];
        this.setState({
          columns,
          total: res.data.total,
          dataSource: res.data.data,
          loading: false
        });
      } else {
        message.error(res.message)
      }
    }
  }
  //操作
  handleVisibleModal = (type, record) => {
    let _this = this
    let { create, modify, selectedRowKeys } = this.state;
    switch (type) {
      case 'createEmployee':
        create.visible = true
        break;
      case 'modifyEmployee':
        modify.visible = true
        modify.id = record.id
        break;
      case 'deleteEmployee':
        if (record.id) {
          Modal.confirm({
            title: '删除员工',
            content: '确认要删除该员工？',
            onOk () {
              deleteEmployee(record.id).then(res => {
                if (res.success) {
                  message.success('删除成功！')
                  _this.handleGetEmployees()
                } else {
                  message.error(res.message)
                }
              })
            }
          })
        }
        else {
          if (selectedRowKeys.length > 0) {
            Modal.confirm({
              title: '删除员工',
              content: '确认要删除勾选的员工？',
              onOk () {
                deleteEmployee(selectedRowKeys.join(',')).then(res => {
                  if (res.success) {
                    message.success('删除成功！')
                    _this.handleGetEmployees()
                  } else {
                    message.error(res.message)
                  }
                })
              }
            })
          } else {
            message.warning('未勾选！')
          }
        }
        break;
      case 'cancel':
        break;
      default:
        break;
    }
    this.setState({
      create,
      modify
    });
  }

  //表格变化
  handleTableChange = (pagination, filters, sorter, extra) => {
    if (extra.action === 'paginate')// 分页
    {
      this.setState({ pagination: pagination })
    } else if (extra.action === 'sort') //排序
    {
      this.setState({ sort: sorter.field + ' ' + (sorter.order ? sorter.order.replace('end', '') : '') })
    } else { //筛选，filter

    }
    this.handleGetEmployees()
  }

  //勾选checkbox
  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys: selectedRowKeys });
  };
  render () {
    const { loading, selectedRowKeys, pagination } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    return (<div>
      <div style={{ marginBottom: 15 }}>
        <Button type="primary" style={{ marginRight: 8 }} onClick={this.handleVisibleModal.bind(this, 'createEmployee')}>新建员工</Button>
        <Button type="primary" onClick={this.handleVisibleModal.bind(this, 'deleteEmployee')}>删除员工</Button>
      </div>
      <Table
        rowSelection={rowSelection}
        className="d-table-center-container"
        dataSource={this.state.dataSource}
        columns={this.state.columns}
        loading={this.state.loading}
        rowKey={record => record.id}
        bordered
        pagination={pagination}
        onChange={this.handleTableChange}
      />
      <EmployeeCreate isVisible={this.state.create.visible} width={500} title={this.state.create.title} callbackParent={this.handleGetEmployees}></EmployeeCreate>
      <EmployeeModify id={this.state.modify.id} isVisible={this.state.modify.visible} width={500} title={this.state.modify.title} callbackParent={this.handleGetEmployees}></EmployeeModify>
    </div>)
  }
}

export default LayoutUnit(HomePage)