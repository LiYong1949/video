/*
 * @Author: liyong
 * @Date: 2021-06-08 20:57:04
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 11:38:32
 * @Description: 员工更新
 */
import React, { Component, useState, useEffect } from 'react';
import { Spin, Form, Input, Button, DatePicker, Select, Radio, message, Modal, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { getEmployeeInfo, modifyEmployee } from '@/api/employee'
import moment from 'moment'
import 'moment/locale/zh-cn'
const { TextArea } = Input;
const { Option } = Select;

const Home = (props) => {
  const { id, isVisible, title, callbackParent } = props
  const [form] = Form.useForm()
  const [visible, setVisible] = useState(isVisible)
  // 状态控制
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const [fileList, setFileList] = useState([])

  useEffect(async () => {

    if (id > 0) {
      let res2 = await getEmployeeInfo(id)
      if (res2.success) {
        form.setFieldsValue({ realName: res2.data.realName, mobile: res2.data.mobile, email: res2.data.email, gender: res2.data.gender, entryTime: moment(res2.data.entryTime), resignTime: moment(res2.data.resignTime), birthday: moment(res2.data.birthday), remark: res2.data.remark, status: res2.data.status })
      } else {
        message.error(res2.message)
      }
    }

  }, [id])

  // 提交表单
  const handleOk = async () => {
    setLoading(true);
    form.validateFields().then(values => {
      setLoading(false);
      modifyEmployee(id, values).then(res => {
        setLoading(false);
        if (res.success) {
          message.success('员工修改成功！');
          // form.resetFields()
          callbackParent(); // 关闭父容器
        } else {
          message.error(res.message);
        }
      });
    })
  };

  //取消
  const handleCancel = () => {
    callbackParent(1); // 关闭父容器
  }


  // 表单布局
  const layout = {
    labelCol: {
      span: 5
    },
    wrapperCol: {
      span: 18
    }
  };

  return (
    <Modal
      forceRender
      width={700}
      title={title}
      onOk={handleOk}
      visible={isVisible}
      onCancel={handleCancel}
    >
      <Spin spinning={loading}>
        <Form
          {...layout}
          name="EmployeeModify"
          form={form}
        >
          <Form.Item
            label="姓名"
            name="realName"
            rules={[
              {
                required: true,
                message: '请输入员工姓名！'
              }
            ]}
          >
            <Input allowClear maxLength={10} placeholder="输入员工姓名" />
          </Form.Item>
          <Form.Item
            label="手机号"
            name="mobile"
            rules={[
              {
                required: true,
                message: '请输入手机号！'
              }
            ]}
          >
            <Input allowClear maxLength={11} placeholder="输入手机号" />
          </Form.Item>
          <Form.Item
            label="邮箱"
            name="email" rules={[{ pattern: /^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*\.[a-z]{2,}$/, message: '请输入正确的邮箱' }]}
          >
            <Input allowClear maxLength={20} placeholder="输入邮箱" />
          </Form.Item>
          <Form.Item
            label="性别"
            name="gender"
            rules={[
              {
                required: true,
                message: '请选择员工性别！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>未知</Radio>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="入职时间"
            name="entryTime">
            <DatePicker picker="date" />
          </Form.Item>
          <Form.Item label="离职时间"
            name="resignTime">
            <DatePicker picker="date" />
          </Form.Item>
          <Form.Item label="生日"
            name="birthday">
            <DatePicker picker="date" />
          </Form.Item>
          <Form.Item
            label="备注"
            name="remark"
          >
            <TextArea allowClear maxLength={200} placeholder="输入员工备注" />
          </Form.Item>
          <Form.Item
            label="状态"
            name="status"
            rules={[
              {
                required: true,
                message: '请选择状态！'
              }
            ]}
          >
            <Radio.Group>
              <Radio value={0}>正常</Radio>
              <Radio value={1}>离职</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
};

export default Home;