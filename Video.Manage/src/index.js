/*
 * @Description:
 * @Author: liyong
 * @Date: 2021-05-25 15:14:20
 * @LastEditTime: 2021-06-11 17:08:41
 * @LastEditors: liyong
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '@/styles/index.less'
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { ConfigProvider } from 'antd'
import zhCN from 'antd/lib/locale/zh_CN'

import store from '@/store'
ReactDOM.render(
  <BrowserRouter>
    <ConfigProvider locale={zhCN}>
      <Provider {...store}>
        <App />
      </Provider>
    </ConfigProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
