/*
 * @Author: liyong
 * @Date: 2021-05-12 19:32:44
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-21 20:49:58
 * @Description: 角色请求
 */
import request from '@/utils/request'

// 获取角色数据
export async function getRoleList() {
    return request({
        url: '/roles/all',
        method: 'get'
    })
}

// 获取角色分页
export async function getRolePageList() {
    return request({
        url: '/roles',
        method: 'get'
    })
}

// 获取指定角色信息
export async function getRoleInfo(id) {
    return request({
        url: `/roles/${id}`,
        method: 'get'
    })
}

//创建角色
export async function createRole(data) {
    return request({
        url: '/roles',
        method: 'post',
        data
    })
}

//修改角色
export async function modifyRole(id, data) {
    return request({
        url: `/roles/${id}`,
        method: 'put',
        data
    })
}

//删除角色
export async function deleteRole(ids) {
    return request({
        url: '/roles',
        method: 'delete',
        params: { ids }
    })
}

// 获取权限按钮枚举
export async function getAuthButtonEnumList() {
    return request({
        url: '/roles/enums/auth-icon',
        method: 'get'
    })
}

//获取角色权限列表
export async function getPermissionList(id){
    return request({
        url: `/roles/permission/${id}`,
        method: 'get'
    })
}