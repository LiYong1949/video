/*
 * @Description:
 * @Author: liyong
 * @Date: 2021-06-15 13:58:41
 * @LastEditTime: 2021-06-15 14:23:12
 * @LastEditors: liyong
 */
import request from '@/utils/request'
// 获取菜单树
export async function getMenuTree() {
  return request({
      url: '/menus/tree',
      method: 'get'
  })
}

// 获取指定菜单信息
export async function getMenuInfo(id) {
    return request({
        url: `/menus/${id}`,
        method: 'get'
    })
}

//创建菜单
export async function createMenu(data) {
    return request({
        url: '/menus',
        method: 'post',
        data
    })
}

//修改菜单
export async function modifyMenu(id, data) {
    return request({
        url: `/menus/${id}`,
        method: 'put',
        data
    })
}

//删除菜单
export async function deleteMenu(ids) {
    return request({
        url: '/menus',
        method: 'delete',
        params: { ids }
    })
}