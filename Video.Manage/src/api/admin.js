/*
 * @Author: liyong
 * @Date: 2021-03-09 20:39:07
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 10:01:07
 * @Description: 管理员请求
 */
import request from '@/utils/request'
// 获取管理员数据
export async function getAdminList() {
  return request({
      url: '/admins/all',
      method: 'get'
  })
}

// 获取管理员分页
export async function getAdminPageList(data) {
    return request({
        url: '/admins',
        method: 'get',
        params:data
    })
}

// 获取指定管理员信息
export async function getAdminInfo(id) {
    return request({
        url: `/admins/${id}`,
        method: 'get'
    })
}

//创建管理员
export async function createAdmin(data) {
    return request({
        url: '/admins',
        method: 'post',
        data
    })
}

//修改管理员
export async function modifyAdmin(id, data) {
    return request({
        url: `/admins/${id}`,
        method: 'put',
        data
    })
}

//删除管理员
export async function deleteAdmin(ids) {
    return request({
        url: '/admins',
        method: 'delete',
        params: { ids }
    })
}