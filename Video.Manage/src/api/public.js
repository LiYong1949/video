/*
 * @Author: liyong
 * @Date: 2021-03-09 20:38:36
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-03 14:01:10
 * @Description: 公共请求
 */
import request from '@/utils/request'

// 获取当前登录用户路由
export async function getCurrentRouterTree() {
    return request({
        url: '/admins/router-tree',
        method: 'get'
    })
}

// 获取当前登录用户菜单
export async function getCurrentMenuTree() {
    return request({
        url: '/admins/menu-tree',
        method: 'get'
    })
}

// 获取当前登录用户信息
export async function getCurrentUserInfo() {
    return request({
        url: '/admins/current-info',
        method: 'get'
    })
}

//登录
export async function login(data) {
    return request({
        url: '/admins/login',
        method: 'post',
        data
    })
}

//登出
export async function logout() {
    return request({
        url: '/admins/logout',
        method: 'post'
    })
}

//文件上传
export function upload (url, file) {
  return request({
    url: url,
    method: 'post',
    data: file
  })
}

export default{

}