/*
 * @Author: liyong
 * @Date: 2021-05-12 23:20:24
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-03 13:54:40
 * @Description:
 */
import admin from './admin'
import publics from './public'
import role from './role'
import user from './user'

export default {
  ...admin,
  ...publics,
  ...role,
  ...user
}