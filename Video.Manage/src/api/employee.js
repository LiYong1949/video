/*
 * @Description:员工api
 * @Author: liyong
 * @Date: 2021-06-11 15:19:29
 * @LastEditTime: 2021-06-11 15:21:46
 * @LastEditors: liyong
 */
import request from '@/utils/request'
// 获取员工数据
export async function getEmployeeList() {
  return request({
      url: '/employees/all',
      method: 'get'
  })
}

// 获取员工分页
export async function getEmployeePageList(data) {
    return request({
        url: '/employees',
        method: 'get',
        params:data
    })
}

// 获取指定员工信息
export async function getEmployeeInfo(id) {
    return request({
        url: `/employees/${id}`,
        method: 'get'
    })
}

//创建员工
export async function createEmployee(data) {
    return request({
        url: '/employees',
        method: 'post',
        data
    })
}

//修改员工
export async function modifyEmployee(id, data) {
    return request({
        url: `/employees/${id}`,
        method: 'put',
        data
    })
}

//删除员工
export async function deleteEmployee(ids) {
    return request({
        url: '/employees',
        method: 'delete',
        params: { ids }
    })
}