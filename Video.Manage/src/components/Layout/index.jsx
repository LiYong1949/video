/*
 * @Author: liyong
 * @Date: 2021-03-08 21:44:49
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-04 13:44:32
 * @Description: Layout
 */
import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import SiderBar from './SiderBar'
import HeaderBar from './HeaderBar'
import './Common.less'

const { Header, Sider, Content, Footer } = Layout;

const LayoutUnit = (WrappedComponent, options) => {
  return class HOC extends Component {
    constructor(props) {
      super(props);
      this.state = {
        collapsed: false
      };
    }

    toggle = () => {
      this.setState({
        collapsed: !this.state.collapsed
      });
    };
    // 退出登录
    handleLogout = async () => {
      await this.props.user.logout()
    }

    render() {
      const state = this.state;
      return (
        <Layout>
          <Sider className="site-layout-background" id="siderNav" trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo" />
            <SiderBar />
          </Sider >
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }}>
              <HeaderBar history={this.props.history} setCollapsed={this.toggle} />
            </Header>
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280
              }}
            >
              <div className="d-content-container">
                {/* 内容体 */
                  WrappedComponent && <WrappedComponent {...this.props} />
                }
              </div>
            </Content>
            <Footer>Footer</Footer>
          </Layout>
        </Layout>
      );
    }
  }
}
export default LayoutUnit;