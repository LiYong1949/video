/*
 * @Author: liyong
 * @Date: 2021-03-08 21:52:25
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-17 21:19:19
 * @Description: 侧边导航栏
 */
import React, { Component } from 'react';
import 'antd/dist/antd.min.css';
import { Layout, Menu, message } from 'antd';
import { getCurrentMenuTree } from '@/api/public'
import { Link, withRouter } from 'react-router-dom';
import * as Icon from '@ant-design/icons';
import { observer, inject } from 'mobx-react'
import './Common.less'
import {
    AppstoreOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    PieChartOutlined,
    DesktopOutlined,
    ContainerOutlined,
    MailOutlined
} from '@ant-design/icons';

const { SubMenu } = Menu;
const Sider = Layout;//使用定义
@inject('permission') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者
class SiderBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuList: this.props.permission.menus,
            openKeys: this.props.permission.openKeys,// 展开的submenu
            current: '/'
            // theme: 'dark',
            // isOpen: false,
            //  collapsed: this.props.collapsed
        };
    }
    componentDidMount () {
        if (this.props.location.pathname) {
            this.setState({
                current: this.props.location.pathname !== '/' ? this.props.location.pathname.substr(1) : this.props.location.pathname
            })
        }
    }
    // 构造菜单
    generateMenu = (dataSource = []) => {
        return (
            dataSource && dataSource.map((menu, index) => {
                if (menu.visible) { // 菜单且属于显示状态
                    if ((menu.children && menu.children.length > 0) || menu.isGroup) { // 若有子级菜单或属于根菜单
                        return (
                            <SubMenu key={menu.id} title={menu.name} icon={React.createElement(Icon[menu.icon])}>
                                {this.generateMenu(menu.children)}
                            </SubMenu>
                        )
                    } else {
                        return (<Menu.Item key={menu.id} icon={React.createElement(Icon[menu.icon])}><Link to={menu.routerPath}>{menu.name}</Link> </Menu.Item>)
                    }
                }
            })
        )
    }
    // 获取菜单
    getMenuTree = () => {
        getCurrentMenuTree().then(res => {
            if (res.success) {
                return res.data
            } else {
                message.error(res.message)
            }
        })
    }
    // MenuItem点击
    handleClick = e => {
        this.setState({
            current: e.key.indexOf('/') > -1 ? e.key : '/' + e.key
        })
    }
    //SubMenu 展开/关闭的回调
    openChange = (k) => {
        this.props.permission.setOpenKeys(k)
        this.setState({
            openKeys: k
        })
    }
    render () {
        return (
            <Menu theme="dark" mode="inline" openKeys={this.state.openKeys} selectedKeys={[this.state.current]} onClick={this.handleClick} onOpenChange={this.openChange}>
                {
                    this.generateMenu(this.state.menuList)
                }
            </Menu >
        );
    }
}
export default withRouter(SiderBar)