/*
 * @Author: liyong
 * @Date: 2021-03-08 21:56:17
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 11:52:01
 * @Description: 页头
 */
import React, { Component } from 'react';
import 'antd/dist/antd.min.css';//显示样式，不加显示不出来
import { Layout, Tooltip, Popover } from 'antd';
import { Link } from 'react-router-dom';
import { observer, inject } from 'mobx-react'
import { HomeOutlined, TeamOutlined, AppstoreOutlined, LaptopOutlined, ToolOutlined, MenuUnfoldOutlined, MenuFoldOutlined, RetweetOutlined } from '@ant-design/icons'
import './Common.less'

const { Header } = Layout;      //使用前定义，不加大括号会没有样式
@inject('user') //将组件链接到 store,以props的形式传递给目标组件
@observer //观察者
class HeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      visible: false
    };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
    this.props.setCollapsed()
  };

  handleVisibleChange = () => {
    this.setState({ visible: !this.state.visible });
  }

  //退出登录
  handleLogout = async () => {
    await this.props.user.logout()
    this.props.history.push('/login')
  }
  render () {
    const state = this.state;
    const userNavigationCustom = (
      <div className="d-navigation-custom-avatar">
        <ul className="d-navigation-menu">
          <li className="d-navigation-menu-item" onClick={this.handleLogout}>退出登录</li>
        </ul>
      </div >
    );

    return (
      <div>
        <div className="d-collapsed">
          {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: this.toggle
          })}
        </div>
        <div className="d-header" >
          {/* <div className="d-project">
          交控MAAS项目
          <Tooltip placement="bottom" title="切换项目">
            <RetweetOutlined className="d-header-icon" />
          </Tooltip>
        </div> */}
          <div className="d-avatar">
            <Popover
              content={userNavigationCustom}
              title=""
              trigger="hover"
              visible={state.visible}
              onVisibleChange={this.handleVisibleChange}
              placement="bottomRight"
            >
              <img src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png" alt="avatar" />
            </Popover>
          </div>
        </div>
      </div>
    )
  }
}
export default HeaderBar
