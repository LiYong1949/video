import React, { Component, useState, useEffect } from 'react';
import { Modal, Button } from 'antd';
import { useRef } from 'react'
const Home = (props) => {

    const { title, isVisible, width, children, callbackParent } = props;
    const [isModalVisible, setIsModalVisible] = useState(isVisible);

    // 取消
    const handleCancel = () => {
        setIsModalVisible(false);
        if (callbackParent) callbackParent();
    };

    //提交
    const handleOk = () => {
        // setIsModalVisible(false);
        //  callbackParent();
        console.log(props.children);
    }

    useEffect(() => {
        setIsModalVisible(isVisible);
    })

    return (
        <>
            <Modal
                forceRender
                width={width || '60%'}
                title={title}
                onCancel={handleCancel}
                onOk={handleOk}
                visible={isModalVisible}
            >
                {children}
            </Modal>
        </>
    );
};

export default Home;