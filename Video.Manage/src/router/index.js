/* eslint-disable no-unused-vars */
/*
 * @Author: liyong
 * @Date: 2021-03-08 20:52:59
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 14:21:40
 * @Description: 路由
 */
import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import Login from '@/pages/login'
import Dashboard from '@/pages/home'
import NotFound from '@/pages/not-found'
import Admin from '@/pages/admin'
import Role from '@/pages/role'
import Employee from '@/pages/employee'
import Menu from '@/pages/menu'
import RouterAuth from './router-auth'
const configRoutes = [
  {
    path: '/login',
    exact: true,
    main: Login,
    auth:false
  },
  {
    path: '/',
    exact: true,
    main: Dashboard,
    auth:true
  },
  {
    path: '/admin',
    exact: true,
    main: Admin,
    auth:true
  },
  {
    path: '/role',
    exact: true,
    main: Role,
    auth:true
  },
  {
    path: '/employee',
    exact: true,
    main: Employee,
    auth:true
  },
  {
    path: '/menu',
    exact: true,
    main: Menu,
    auth:true
  },
  {
    path: '/404',
    exact: true,
    main: NotFound,
    auth:false
  }
]
// 4.如果需要导航则使用link或navlink，一般情况可以不使用link/NavLink,在浏览器直接输入路由跳转同样生效
// 5. Suspense这种方式只支持16.6以上的react，低版本react封装时候，可以参考上述的路由懒加载中的其他实现代码
const Routes = () => (
  <Router>
    <Switch>
      {
        // configRoutes.map((route, index) => {
        //   return <Route key={index} exact={route.exact} path={route.path} component={(props) => <route.main {...props} />} />
        // })
        <RouterAuth routerConfig={configRoutes} />
      }
    </Switch>
  </Router>
)
export default Routes