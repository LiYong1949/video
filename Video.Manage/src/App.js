/*
 * @Author: liyong
 * @Date: 2021-03-08 20:45:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-05-12 22:03:19
 * @Description: 入口
 */
import React, { Component } from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'
import Routes from '@/router/index.js'
export default class App extends Component {
  render() {
    return (
      <Routes />
    )
  }
}

