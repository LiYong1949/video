/* eslint-disable no-undef */
/*
 * @Author: liyong
 * @Date: 2021-03-11 20:57:39
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-20 19:38:39
 * @Description: axios封装
 */
import axios from 'axios'
// import { message } from 'antd'
import { getToken } from '@/utils/auth'
import {message} from 'antd'
let baseUrl = ''
if (process.env.NODE_ENV === 'development') {
    baseUrl = 'http://localhost:5010/api'
} else if (process.env.NODE_ENV === 'production') {
    baseUrl = 'http://heartcity:5010/api'
}
// create an axios instance
const service = axios.create({
    baseURL: baseUrl, // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 60000 // request timeout
})

// request interceptor
service.interceptors.request.use(
    config => {
        // do something before request is sent
        config.headers['Authorization'] = getToken()
        return config
    },
    error => {
        // do something with request error
        return Promise.reject(error)
    }
)

service.interceptors.response.use(
    response => {
        const res = response.data
        return res
    },
    error => {
        console.log(error)
        var res = error.response
        var status = res.status

        if (status === 401) {
            message.error('登录失效，请重新登录！')
            window.location.href = '/login'
        } else if (status === 500) {
            message.error('服务器异常，请稍后再试或联系管理员！')
        } else {
            message.error('出错啦！')
        }
        return Promise.reject(error)
    }
)

export default service
