/*
 * @Description:表单校验规则
 * @Author: liyong
 * @Date: 2021-06-11 15:53:03
 * @LastEditTime: 2021-06-11 15:53:19
 * @LastEditors: liyong
 */
// 下拉选择（id大于0）
export function checkSelectID (rule, value, callback) {
  if (/^\+?[1-9]\d*$/.test(value)) {
    callback()
  } else {
    callback(new Error('未选择'))
  }
}

// QQ
export function checkQQ (rule, value, callback) {
  if (value) {
    if (/^[1-9][0-9]{4,10}$/.test(value)) {
      callback()
    } else {
      callback(new Error('输入正确的QQ号'))
    }
  } else {
    callback()
  }
}

// 手机号

export function checkMobile (rule, value, callback) {
  if (value) {
    if (/^1[34578]\d{9}$/.test(value)) {
      callback()
    } else {
      callback(new Error('手机号码格式错误'))
    }
  } else {
    callback()
  }
}

// 座机号

export function checkPhone (rule, value, callback) {
  if (value) {
    if (/0\d{2,3}-\d{7,8}/.test(value)) {
      callback()
    } else {
      callback(new Error('号码格式错误'))
    }
  } else {
    callback()
  }
}

// 邮箱
export function checkEmail (rule, value, callback) {
  if (value) {
    var reg = /^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*\.[a-z]{2,}$/
    if (!reg.test(value)) {
      return callback(new Error('邮箱格式错误'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

// 类似金钱,首位不为0,最多2位小数
export function checkMoney (rule, value, callback) {
  if (value) {
    const reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
    if (!reg.test(value)) {
      return callback(new Error('请填写数字,最多2位小数'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

// 身份证
export function checkIdNum (rule, value, callback) {
  if (value) {
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    if (!reg.test(value)) {
      return callback(new Error('证件号码不正确'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

// 整数
export function checkInterNum (rule, value, callback) {
  if (value) {
    const reg = /^[0-9]*[1-9][0-9]*$/
    if (!reg.test(value)) {
      return callback(new Error('请输入整数'))
    } else {
      callback()
    }
  } else {
    callback()
  }
}

// 手机号(非空)
export function validateMobile (rule, value, callback) {
  if (value === '') {
    callback(new Error('请填写手机号'))
  } else {
    if (value) {
      if (/^1[34578]\d{9}$/.test(value)) {
        callback()
      } else {
        callback(new Error('手机号码格式错误'))
      }
    } else {
      callback()
    }
  }
}

// 邮箱(非空)
export function validateEmail (rule, value, callback) {
  if (value === '') {
    callback(new Error('请填写邮箱'))
  } else {
    if (value) {
      var reg = /^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*\.[a-z]{2,}$/
      if (!reg.test(value)) {
        return callback(new Error('邮箱格式错误'))
      } else {
        callback()
      }
    } else {
      callback()
    }
  }
}

export default {
  Required: [{
    required: true,
    message: '不能为空',
    trigger: 'blur'
  }],
  QQ: [{
    validator: checkQQ,
    trigger: 'blur'
  }],
  Email: [{
    validator: checkEmail,
    trigger: 'blur'
  }],
  Mobile: [{
    validator: checkMobile,
    trigger: 'blur'
  }],
  Phone: [{
    validator: checkPhone,
    trigger: 'blur'
  }],
  Money: [{
    validator: checkMoney,
    trigger: 'blur'
  }],
  InterNum: [{
    validator: checkInterNum,
    trigger: 'blur'
  }],
  CallbackID: [{
    required: true,
    validator: checkSelectID,
    message: '未选择',
    trigger: 'blur'
  }], // 回调
  SelectID: [{
    required: true,
    validator: checkSelectID,
    message: '未选择',
    trigger: 'change'
  }]
}
