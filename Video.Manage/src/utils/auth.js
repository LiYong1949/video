/*
 * @Author: liyong
 * @Date: 2021-03-09 20:52:34
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-17 21:26:53
 * @Description: 权限管理
 */
const TokenKey = 'user_token'
const UserKey = 'user_info'
const MenuKey = 'user_menu'
const OpenKey = 'menu_sub'
// 获取token
export function getToken() {
    return localStorage.getItem(TokenKey)
}

//设置token
export function setToken(token) {
    return localStorage.setItem(TokenKey, token)
}
//移除token
export function removeToken() {
    return localStorage.removeItem(TokenKey)
}

// 获取用户
export function getUserInfo() {
    return JSON.parse(localStorage.getItem(UserKey))
}

//设置用户
export function setUserInfo(info) {
    return localStorage.setItem(UserKey, JSON.stringify(info))
}
//移除用户
export function removeUserInfo() {
    return localStorage.removeItem(UserKey)
}

//设置菜单数据
export function setMenus(menus) {
    return localStorage.setItem(MenuKey, JSON.stringify(menus))
}

// 获取菜单数据
export function getMenus() {
    return JSON.parse(localStorage.getItem(MenuKey))
}
//移除菜单数据
export function removeMenus() {
    return localStorage.removeItem(MenuKey)
}

// 设置SubMenu
export function setOpenKeys(keys) {
    return localStorage.setItem(OpenKey, JSON.stringify(keys))
}

// 获取SubMenu
export function getOpenKeys() {
    return JSON.parse(localStorage.getItem(OpenKey))
}