/*
 * @Author: liyong
 * @Date: 2021-03-09 20:50:40
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-03 14:23:37
 * @Description: 用户状态管理
 */
import { observable, action } from 'mobx'
import { login, logout, getCurrentUserInfo } from '@/api/public'
import { getToken, setToken, removeToken, getUserInfo, setUserInfo, removeUserInfo, removeMenus } from '@/utils/auth'
import { message } from 'antd'
import { Component } from 'react'
import md5 from 'md5'

class User {
    // 被观察者属性
    @observable isLogin = !!getToken()  //利用token来判断用户是否登录，避免刷新页面后登录状态丢失
    @observable userInfo = getUserInfo()  //用户
    @observable token = getToken()


    // 触发的动作
    // 登录
    @action.bound login = async (data = {}) => {
        const { account, password } = data  //设置登录用户信息
        return new Promise(async (resolve, reject) => {
            await login({ account: account.trim(), password: md5(password) }).then(res => {
                if (res.success) {
                    this.isLogin = true
                    this.userInfo = res.data.user
                    this.token = res.data.token
                    setToken(res.data.token)
                    setUserInfo(res.data.user)
                    resolve(true)
                } else {
                    message.error(res.message)
                    resolve(false)
                }
            }).catch(error => {
                reject(error)
            })
        })
    }

    // 登出
    @action.bound logout = async () => {
        return new Promise(async (resolve, reject) => {
            await logout().then(res => {
                if (res.success) {
                    this.isLogin = false
                    this.userInfo = null
                    this.token = null
                    removeToken()
                    removeUserInfo()
                    removeMenus()
                    resolve(true)
                }
                else {
                    message.error(res.message)
                }
            }).catch(error => {
                reject(error)
            })
        })
    }
    // 获取用户信息
    @action.bound getUserInfo = async () => {
        return new Promise(async (resolve, reject) => {
            await getCurrentUserInfo().then(res => {
                if (res.success) {
                    this.userInfo = res.data
                    setUserInfo(res.data)
                    resolve(this.userInfo)
                } else {
                    message.error(res.message)
                }
            }).catch(error => {
                reject(error)
            })
        })
    }

    // 移除token
    @action.bound removeToken() {
        return new Promise(resolve => {
            removeToken()
            resolve()
        })
    }

    // 移除用户信息
    @action.bound removeUserInfo() {
        return new Promise(resolve => {
            removeUserInfo()
            resolve(true)
        })
    }
}

export default new User()