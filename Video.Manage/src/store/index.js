/*
 * @Author: liyong
 * @Date: 2021-03-09 20:47:58
 * @LastEditors: liyong
 * @LastEditTime: 2021-04-20 22:04:30
 * @Description: 状态管理
 */
import user from './user'
import permission from './permission'
const store = {
    user,
    permission
}
export default store