/*
 * @Author: liyong
 * @Date: 2021-03-13 18:40:22
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-11 14:59:16
 * @Description: 权限
 */
import { observable, action } from 'mobx'
import { message } from 'antd'
import React, { Component } from 'react'
import { getMenus, setMenus, removeMenus,getOpenKeys,setOpenKeys } from '@/utils/auth'
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
    Link
} from 'react-router-dom'
import { getCurrentRouterTree, getCurrentMenuTree } from '@/api/public'
class Permission {
    @observable routers = []  //路由
    @observable menus = getMenus()  //菜单
    @observable openKeys=getOpenKeys() // 打开的submenu

    // // 构造路由
    // generateRoute(router) {
    //     if (router.children) {
    //         return (
    //             <React.Fragment key={router.path} >
    //                 <Route exact strict path={router.path} render={
    //                     router.redirect ?
    //                         <Redirect push to={router.redirect} from={router.path}></Redirect> :
    //                         <router.component routes={router.children}>
    //                             {
    //                                 router.children.map((item) => {
    //                                     return this.generateRoute(item)
    //                                 })
    //                             }
    //                         </router.component>
    //                 }>

    //                 </Route>
    //             </React.Fragment>
    //         )
    //     }
    //     return <Route key={router.path} path={router.path} component={router.component} />
    // }
    // 获取路由
    // @action getRouters() {

    //     getCurrentRouterTree().then(res => {
    //         this.routers = res.data
    //         return (
    //             <Router forceRefresh={true}>
    //                 <div>
    //                     <nav>
    //                         <ul>
    //                             {
    //                                 res.data.map((item) => {
    //                                     return <li key={item.path}>
    //                                         <Link to={item.path} >{item.path || '/'}</Link>
    //                                     </li>
    //                                 })
    //                             }
    //                         </ul>
    //                     </nav>

    //                     <Switch>
    //                         {
    //                             res.data.map(item => {
    //                                 return this.generateRoute(item)
    //                             })
    //                         }
    //                     </Switch>
    //                 </div>
    //             </Router>
    //         )
    //     })
    // }

    // 获取菜单
    @action getMenus = async () => {
       await getCurrentMenuTree().then(res => {
            if (res.success) {
                this.menus = res.data
                setMenus(res.data)
            }
            else {
                message.error(res.message)
            }
        })
    }

    // 设置submenu
    @action setOpenKeys=(data)=>{
      setOpenKeys(data)
    }

}

export default new Permission()