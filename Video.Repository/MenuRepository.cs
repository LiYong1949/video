/*
 * @Author: liyong
 * @Date: 2020-10-22 17:04:49
 * @LastEditors: liyong
 * @LastEditTime: 2020-10-22 17:06:16
 * @Description: 菜单Repository
 */
using System.Data.Common;
using System.Threading.Tasks;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore.Internal;
using Video.IRepository;
using Video.Model;
using System.Linq;
using System.Linq.Expressions;
using System;
using Video.Common;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Video.Repository
{
    public class MenuRepository : BaseRepository<Menu>, IMenuRepository
    {
        public MenuRepository(AppDbContext dbContext)
           : base(dbContext)
        {
        }
    }
}
