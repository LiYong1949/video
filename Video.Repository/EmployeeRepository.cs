/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-15 09:13:18
 * @Description: 员工Repository
 */
using System.Data.Common;
using System.Threading.Tasks;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore.Internal;
using Video.IRepository;
using Video.Model;
using System.Linq;
using System.Linq.Expressions;
using System;
using Video.Common;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Video.Repository
{
  public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
  {
    public EmployeeRepository(AppDbContext dbContext)
        : base(dbContext)
    {
    }

    public async Task<PageResultDto> GetListAsync(Expression<Func<EmployeeDto, bool>> where, int pageIndex, int pageSize, string order)
    {
      #region linq方式

      var employeeQuery = _DbContext.Set<Employee>().AsQueryable();
      var departmentQuery = _DbContext.Set<Department>().AsQueryable();

      var query = from employee in employeeQuery
                  join department in departmentQuery on employee.DepartmentId equals department.Id into departmentDefault
                  from subdep in departmentDefault.DefaultIfEmpty()
                  where employee.IsDeleted == false

                  select new EmployeeDto
                  {
                    Id = employee.Id,
                    Avatar = employee.Avatar,
                    RealName = employee.RealName,
                    Mobile = employee.Mobile,
                    Email = employee.Email,
                    Gender = employee.Gender,
                    Status = employee.Status,
                    CreateTime = employee.CreateTime,
                    DepartmentName = subdep.Name,
                    Creator = employee.Creator,
                    LastModifier = employee.LastModifier,
                    LastModifyTime = employee.LastModifyTime,
                    EntryTime = employee.EntryTime,
                    ResignTime = employee.ResignTime,
                    Birthday = employee.Birthday,
                    Remark = employee.Remark
                  };

      if (where != null)
        query = query.Where(where);

      if (!string.IsNullOrEmpty(order))
      {
        query = query.OrderByBatch(order);
      }
      else
      {
        query = query.OrderByBatch("CreateTime DESC");
      }
      var total = await query.CountAsync();
      var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

      #endregion

      #region 外键方式

      // if (where == null)
      //     where = c => true;

      // var query = _DbContext.Set<SysEmployee>()
      //     .Include(p => p.Department)
      //     .Include(p => p.Permission)
      //     .Where(where)
      //     .AsNoTracking()
      //     .Select(it => new EmployeeListDto
      //     {
      //         Id = it.Id,
      //         Account = it.Account,
      //         Avatar = it.Avatar,
      //         RealName = it.RealName,
      //         DepartmentName = it.Department.Name,
      //         PermissionName = it.Permission.Name,
      //         Mobile = it.Mobile,
      //         Email = it.Email,
      //         Status = it.Status,
      //         AddTime = it.AddTime,
      //         LastLoginIP = it.LastLoginIP,
      //         LastLoginTime = it.LastLoginTime
      //     });

      // if (!string.IsNullOrEmpty(order))
      //     query = query.OrderByBatch(order);

      // var total = query.Count();
      // var data = await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
      #endregion

      data.ForEach(it =>
      {
        var creator = (from employee in employeeQuery where employee.Id == it.Creator select employee.RealName).FirstOrDefault();
        var modifier = (from employee in employeeQuery where employee.Id == it.LastModifier select employee.RealName).FirstOrDefault();
        it.StatusText = it.Status.GetDescription();
        it.CreatorName = creator;
        it.LastModifierName = modifier;
      });

      return new PageResultDto
      {
        Total = total,
        Data = data
      }; ;
    }
  }
}