﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Video.Model;
using Video.IRepository;
namespace Video.Repository
{
    public class AdminRepository : BaseRepository<Admin>, IAdminRepository
    {
        public AdminRepository(AppDbContext dbContext)
           : base(dbContext)
        {
        }
    }
}
