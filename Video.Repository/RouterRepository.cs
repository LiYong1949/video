﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Video.Model;
using Video.IRepository;
namespace Video.Repository
{
    public class RouterRepository : BaseRepository<Router>, IRouterRepository
    {
        public RouterRepository(AppDbContext dbContext)
           : base(dbContext)
        {
        }
    }
}
