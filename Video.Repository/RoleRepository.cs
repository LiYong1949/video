﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Video.Model;
using Video.IRepository;
namespace Video.Repository
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(AppDbContext dbContext)
           : base(dbContext)
        {
        }
    }
}
