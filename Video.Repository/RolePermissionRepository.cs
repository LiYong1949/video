/*
 * @Author: liyong
 * @Date: 2020-10-13 21:57:02
 * @LastEditors: liyong
 * @LastEditTime: 2020-10-13 22:12:24
 * @Description: 权限Repository
 */
using System.Data.Common;
using System.Threading.Tasks;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore.Internal;
using Video.IRepository;
using Video.Model;
using System.Linq;
using System.Linq.Expressions;
using System;
using Video.Common;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Video.Repository
{
    public class PermissionRepository : BaseRepository<RolePermission>, IRolePermissionRepository
    {
        public PermissionRepository(AppDbContext dbContext)
           : base(dbContext)
        {
        }
    }
}
