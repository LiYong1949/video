using Microsoft.EntityFrameworkCore;
using Video.IRepository;
using Video.Common;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Video.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected AppDbContext _DbContext;
        private readonly DbSet<T> _DbSet;

        public BaseRepository(AppDbContext dbContext)
        {
            this._DbContext = dbContext;
            this._DbSet = _DbContext.Set<T>();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _DbContext.SaveChangesAsync();
        }

        /// <summary>
        /// 新增一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<int> AddAsync(T entity, bool isSave = true)
        {
            _DbContext.Add(entity);
            if (isSave)
            {
                var dbres = await SaveChangesAsync();
                _DbContext.Entry(entity);
                return dbres;
            }
            return 0;
        }

        /// <summary>
        /// 新增多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> AddAsync(List<T> entitys, bool isSave = true)
        {
            _DbContext.AddRange(entitys);
            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 删除一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(T entity, bool isSave = true)
        {
            _DbContext.Attach(entity);
            _DbContext.Remove(entity);

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 删除多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(List<T> entitys, bool isSave = true)
        {
            entitys.ForEach(entity =>
            {
                _DbContext.Attach(entity);
                _DbContext.Remove(entity);
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(T entity, bool isSave = true)
        {
            if (entity == null)
                return false;

            _DbContext.Attach(entity);
            var entry = _DbContext.Entry<T>(entity);
            entry.State = EntityState.Modified;

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改一条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dto"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync<TDto>(T entity, TDto dto, bool isSave = true)
        {
            _DbContext.Attach(entity);
            var entry = _DbContext.Entry<T>(entity);

            dto.GetType().GetProperties().ToList().ForEach(it =>
            {
                //检测当前字段是否是主键
                var metadata = entry.Property(it.Name).Metadata;
                var keys = metadata.GetType().GetProperty("Keys");
                object value = keys.GetValue(metadata);

                if (value == null)
                    entry.Property(it.Name).IsModified = true;
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改指定字段
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="updatePropertyList"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(T entity, List<string> updatePropertyList, bool isSave = true)
        {
            if (entity == null || updatePropertyList == null)
                return false;

            _DbContext.Attach(entity);
            var entry = _DbContext.Entry<T>(entity);

            updatePropertyList.ForEach(c =>
            {
                entry.Property(c).IsModified = true;
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(List<T> entitys, bool isSave = true)
        {
            if (entitys == null || entitys.Count == 0)
                return false;

            entitys.ForEach(c =>
            {
                _DbContext.Attach(c);
                _DbContext.Entry<T>(c).State = EntityState.Modified;
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dtoType"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(List<T> entitys, Type dtoType, bool isSave = true)
        {
            if (entitys == null || entitys.Count == 0)
                return false;

            entitys.ForEach(entity =>
            {
                _DbContext.Attach(entity);
                var entry = _DbContext.Entry<T>(entity);

                dtoType.GetProperties().ToList().ForEach(it =>
                {
                    //检测当前字段是否是主键
                    var metadata = entry.Property(it.Name).Metadata;
                    var keys = metadata.GetType().GetProperty("Keys");
                    object value = keys.GetValue(metadata);

                    if (value == null)
                        entry.Property(it.Name).IsModified = true;
                });
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 修改多条记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(List<T> entitys, List<string> updatePropertyList, bool isSave = true)
        {
            if (entitys == null || entitys.Count == 0)
                return false;

            entitys.ForEach(entity =>
            {
                _DbContext.Attach(entity);
                var entry = _DbContext.Entry<T>(entity);

                updatePropertyList.ForEach(p =>
                {
                    entry.Property(p).IsModified = true;
                });
            });

            if (isSave)
                return await SaveChangesAsync() > 0;
            return false;
        }

        /// <summary>
        /// 获取entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<T> GetAsync(object id)
        {
            if (id != null)
                return await _DbSet.FindAsync(id);

            return default(T);
        }

        /// <summary>
        /// 获取entity
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<T> GetAsync(Expression<Func<T, bool>> where = null, bool isNoTracking = true)
        {
            var data = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);
            return await data.FirstOrDefaultAsync();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetListAsync(bool isNoTracking = true)
        {
            return await GetListAsync(null);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var data = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            return await data.ToListAsync();
        }

        /// <summary>
        /// 可指定排序查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="where"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, string order, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            if (!string.IsNullOrEmpty(order))
            {
                query = query.OrderByBatch(order);
            }

            return await query.ToListAsync();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, int pageIndex, int pageSize, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            return await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetListAsync(Expression<Func<T, bool>> where, int pageIndex, int pageSize, string order, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            if (!string.IsNullOrEmpty(order))
            {
                query = query.OrderByBatch(order);
            }

            return await query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        /// <summary>
        /// 可指定返回结果、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            return await query.Select(select).ToListAsync();
        }

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, int pageIndex, int pageSize, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            return await query.Select(select).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, string order, bool isNoTracking = false)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            if (!string.IsNullOrEmpty(order))
                query = query.OrderByBatch(order);

            return await query.Select(select).ToListAsync();
        }

        /// <summary>
        /// 可指定返回结果、排序、查询条件的通用查询方法，返回动态类对象集合
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="isNoTracking"></param>
        /// <returns></returns>
        public virtual async Task<List<dynamic>> GetListAsync(Expression<Func<T, dynamic>> select, Expression<Func<T, bool>> where, int pageIndex, int pageSize, string order, bool isNoTracking = true)
        {
            if (where == null)
                where = c => true;

            var query = isNoTracking
                ? _DbSet.Where(where).AsNoTracking()
                : _DbSet.Where(where);

            if (!string.IsNullOrEmpty(order))
                query = query.OrderByBatch(order);

            return await query.Select(select).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        /// <summary>
        /// 获取记录条数
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<int> GetCountAsync(Expression<Func<T, bool>> where)
        {
            if (where == null)
                where = c => true;

            return await _DbSet.Where(where).CountAsync();
        }

        /// <summary>
        /// 验证是否存在
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<bool> IsExistAsync(Expression<Func<T, bool>> where)
        {
            if (where == null)
                return false;

            var entry = _DbSet.Where(where);
            return await entry.AnyAsync();
        }
    }
}
