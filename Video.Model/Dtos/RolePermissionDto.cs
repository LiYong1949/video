/*
 * @Author: liyong
 * @Date: 2020-10-07 21:26:43
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-21 20:44:12
 * @Description: 角色权限Dto
 */
using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;
using System.Collections.Generic;

namespace Video.Model
{
    ///<summary>
    ///角色权限Dto
    ///</summary>
    [AutoInjectAttribute(typeof(RolePermission))]
    public class RolePermissionDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        public string PermissionCode { get; set; }

        ///<summary>
        ///按钮组权限
        ///</summary>
        public string AuthButtons { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }
    }

    ///<summary>
    ///权限添加Dto
    ///</summary>
    [AutoInjectAttribute(typeof(RolePermission))]
    public class RolePermissionAddDto
    {
        ///<summary>
        ///角色编号
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        [Required(ErrorMessage = "权限码不能为空")]
        public string PermissionCode { get; set; }

        ///<summary>
        ///按钮组权限
        ///</summary>
        public string AuthButtons { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }

    ///<summary>
    ///角色授权Dto
    ///</summary>
    public class RoleAuthDto
    {
        ///<summary>
        ///授权详情
        ///</summary>
        public List<RoleAuthItemDto> Auths { get; set; }

        ///<summary>
        ///角色组
        ///</summary>
        public int[] RoleIds { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }
    }

    ///<summary>
    ///角色授权详情Dto
    ///</summary>
    public class RoleAuthItemDto
    {
        public string PermissionCode { get; set; }
        public string AuthButtons { get; set; }
    }
}