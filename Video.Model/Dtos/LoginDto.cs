using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;
using System.Collections.Generic;

namespace Video.Model
{
    /// <summary>
    /// 登录
    /// </summary>
    [AutoInjectAttribute(typeof(Admin))]
    public class LoginDto
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        [Required(ErrorMessage = "登录账号不能为空")]
        public string Account { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        [Required(ErrorMessage = "登录密码不能为空")]
        public string Password { get; set; }

        /// <summary>
        /// 最后登录IP
        /// </summary>
        public string LastLoginIP { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime? LastLoginTime { get; set; }
    }

    public class MenuPageTreeDto
    {
        public int Id { get; set; }

        public string Path { get; set; }

        public object Meta { get; set; }

        public List<MenuPageTreeDto> Children { get; set; }
    }

    public class RouterPageTreeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Component { get; set; }

        public string Path { get; set; }

        public string Redirect { get; set; }

        public object Meta { get; set; }

        public List<RouterPageTreeDto> Children { get; set; }
    }
}