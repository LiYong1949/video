/*
 * @Author: liyong
 * @Date: 2020-10-22 17:31:54
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-16 14:43:05
 * @Description: 菜单Dto
 */
using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;
using System.Collections.Generic;

namespace Video.Model
{
    ///<summary>
    ///菜单树Dto
    ///</summary>
    [AutoInjectAttribute(typeof(Menu))]
    public class MenuTreeDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///菜单名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///链接地址
        ///</summary>
        public string Link { get; set; }

        ///<summary>
        ///父级Id
        ///</summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 路由路径
        /// </summary>
        public string RouterPath { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        public string PermissionCode { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public int Sort { get; set; }

        ///<summary>
        ///菜单图标
        ///</summary>
        public string Icon { get; set; }

        ///<summary>
        ///权限按钮组
        ///</summary>
        public string AuthButtons { get; set; }

        ///<summary>
        ///跳转目标
        ///</summary>
        public MenuTargetEnum Target { get; set; }

        /// <summary>
        /// 是否根
        /// </summary>		
        public bool IsGroup { get; set; }

        ///<summary>
        ///是否显示
        ///</summary>
        public bool Visible { get; set; }

        /// <summary>
        /// 附加参数
        /// </summary>
        public object Meta { get; set; }

        ///<summary>
        ///子集
        ///</summary>
        public List<MenuTreeDto> Children;
    }

    ///<summary>
    ///菜单添加Dto
    ///</summary>
    [AutoInjectAttribute(typeof(Menu))]
    public class MenuAddDto
    {
        ///<summary>
        ///菜单名称
        ///</summary>
        [Required(ErrorMessage = "菜单名称不能为空")]
        public string Name { get; set; }

        ///<summary>
        ///父级Id
        ///</summary>
        public int ParentId { get; set; } = 0;

        ///<summary>
        ///排序
        ///</summary>
        [PositiveInteger(ErrorMessage = "排序格式非法")]
        public int Sort { get; set; } = 0;

        ///<summary>
        ///菜单图标
        ///</summary>
        public string Icon { get; set; }

        ///<summary>
        ///权限按钮组
        ///</summary>
        public string AuthButtons { get; set; }

        ///<summary>
        ///链接地址
        ///</summary>
        public string Link { get; set; }

        ///<summary>
        ///路由地址
        ///</summary>
        public int RouterPath { get; set; }

        ///<summary>
        ///跳转目标
        ///</summary>
        public MenuTargetEnum Target { get; set; }

        /// <summary>
        /// 是否根
        /// </summary>		
        public bool IsGroup { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }

    ///<summary>
    ///菜单修改Dto
    ///</summary>
    [AutoInjectAttribute(typeof(Menu))]
    public class MenuUpdateDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///菜单名称
        ///</summary>
        [Required(ErrorMessage = "菜单名称不能为空")]
        public string Name { get; set; }

        ///<summary>
        ///父级Id
        ///</summary>
        public int ParentId { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [PositiveInteger(ErrorMessage = "排序格式非法")]
        public int Sort { get; set; }

        ///<summary>
        ///菜单图标
        ///</summary>
        [MaxLength(50)]
        public string Icon { get; set; }

        ///<summary>
        ///权限按钮组
        ///</summary>
        public string AuthButtons { get; set; }

        ///<summary>
        ///链接地址
        ///</summary>
        [MaxLength(50)]
        public string Link { get; set; }

        ///<summary>
        ///路由地址
        ///</summary>
        public int RouterPath { get; set; }

        ///<summary>
        ///跳转目标
        ///</summary>
        public MenuTargetEnum Target { get; set; }

        /// <summary>
        /// 是否根
        /// </summary>		
        public bool IsGroup { get; set; }

        ///<summary>
        ///最后修改人Id
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime LastModifyTime { get; set; } = DateTime.Now;

        ///<summary>
        ///是否显示
        ///</summary>
        public bool Visible { get; set; } = true;
    }
}
