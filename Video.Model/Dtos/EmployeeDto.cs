using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;

namespace Video.Model
{
  ///<summary>
  ///员工Dto
  ///</summary>
  [AutoInjectAttribute(typeof(Employee))]
  public class EmployeeDto
  {
    ///<summary>
    ///编号
    ///</summary>
    public int Id { get; set; }

    ///<summary>
    ///工号
    ///</summary>
    public string JobNo { get; set; }

    ///<summary>
    ///姓名
    ///</summary>
    public string RealName { get; set; }

    ///<summary>
    ///手机号
    ///</summary>
    public string Mobile { get; set; }

    ///<summary>
    ///头像
    ///</summary>
    public string Avatar { get; set; }

    ///<summary>
    ///性别
    ///</summary>
    public GenderTypeEnum Gender { get; set; }

    ///<summary>
    ///邮箱
    ///</summary>
    public string Email { get; set; }

    ///<summary>
    ///部门编号
    ///</summary>
    public int DepartmentId { get; set; }

    ///<summary>
    ///部门名称
    ///</summary>
    public string DepartmentName { get; set; }

    ///<summary>
    ///入职时间
    ///</summary>
    public DateTime? EntryTime { get; set; }

    ///<summary>
    ///离职时间
    ///</summary>
    public DateTime? ResignTime { get; set; }

    ///<summary>
    ///生日
    ///</summary>
    public DateTime? Birthday { get; set; }

    ///<summary>
    ///创建人编号
    ///</summary>
    public int Creator { get; set; }

    ///<summary>
    ///创建人姓名
    ///</summary>
    public string CreatorName { get; set; }

    ///<summary>
    ///创建时间
    ///</summary>
    public DateTime CreateTime { get; set; }

    ///<summary>
    ///状态值
    ///</summary>
    public EmployeeStatusEnum Status { get; set; }

    ///<summary>
    ///状态说明
    ///</summary>
    public string StatusText { get; set; }

    ///<summary>
    ///最后修改人编号
    ///</summary>
    public int LastModifier { get; set; }

    ///<summary>
    ///最后修改人姓名
    ///</summary>
    public string LastModifierName { get; set; }

    ///<summary>
    ///最后修改时间
    ///</summary>
    public DateTime? LastModifyTime { get; set; }

    ///<summary>
    ///备注
    ///</summary>
    public string Remark { get; set; }
  }

  ///<summary>
  ///员工添加Dto
  ///</summary>
  [AutoInjectAttribute(typeof(Employee))]
  public class EmployeeAddDto
  {
    ///<summary>
    ///姓名
    ///</summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string RealName { get; set; }

    ///<summary>
    ///头像
    ///</summary>
    public string Avatar { get; set; }

    ///<summary>
    ///性别
    ///</summary>
    public GenderTypeEnum Gender { get; set; }

    ///<summary>
    ///手机号
    ///</summary>
    [Required(ErrorMessage = "手机号码不能为空")]
    [RegularExpression(RegexKey.MOBILE, ErrorMessage = "请填写正确的手机号码")]
    public string Mobile { get; set; }

    ///<summary>
    ///邮箱
    ///</summary>
    [EmailAddress(ErrorMessage = "邮箱地址错误")]
    public string Email { get; set; }

    ///<summary>
    ///入职时间
    ///</summary>
    public DateTime? EntryTime { get; set; }

    ///<summary>
    ///生日
    ///</summary>
    public DateTime? Birthday { get; set; }

    ///<summary>
    ///部门编号
    ///</summary>
    public int DepartmentId { get; set; }

    ///<summary>
    ///职位编号
    ///</summary>
    public int PositionId { get; set; }

    ///<summary>
    ///备注
    ///</summary>
    public string Remark { get; set; }

    ///<summary>
    ///创建人编号
    ///</summary>
    public int Creator { get; set; }

    ///<summary>
    ///创建时间
    ///</summary>
    public DateTime CreateTime { get; set; } = DateTime.Now;
  }


  ///<summary>
  ///员工修改Dto
  ///</summary>
  [AutoInjectAttribute(typeof(Employee))]
  public class EmployeeUpdateDto
  {
    /// <summary>
    /// 编号
    /// </summary>
    public int Id { get; set; }

    ///<summary>
    ///姓名
    ///</summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string RealName { get; set; }

    ///<summary>
    ///头像
    ///</summary>
    public string Avatar { get; set; }

    ///<summary>
    ///性别
    ///</summary>
    public GenderTypeEnum Gender { get; set; }

    ///<summary>
    ///手机号
    ///</summary>
    [Required(ErrorMessage = "手机号码不能为空")]
    [RegularExpression(RegexKey.MOBILE, ErrorMessage = "请填写正确的手机号码")]
    public string Mobile { get; set; }

    ///<summary>
    ///邮箱
    ///</summary>
    [EmailAddress(ErrorMessage = "邮箱地址错误")]
    public string Email { get; set; }

    ///<summary>
    ///部门编号
    ///</summary>
    public int DepartmentId { get; set; }

    ///<summary>
    ///生日
    ///</summary>
    public DateTime? Birthday { get; set; }

    ///<summary>
    ///入职时间
    ///</summary>
    public DateTime? EntryTime { get; set; }
    ///<summary>
    ///离职时间
    ///</summary>
    public DateTime? ResignTime { get; set; }

    ///<summary>
    ///最后修改人编号
    ///</summary>
    public int LastModifier { get; set; }

    ///<summary>
    ///最后修改时间
    ///</summary>
    public DateTime? LastModifyTime { get; set; } = DateTime.Now;

    ///<summary>
    ///职位编号
    ///</summary>
    public int PositionId { get; set; }

    ///<summary>
    ///备注
    ///</summary>
    public string Remark { get; set; }
  }

  ///<summary>
  ///登录员工个人信息修改Dto
  ///</summary>
  [AutoInjectAttribute(typeof(Employee))]
  public class EmployeePersonalUpdateDto
  {

    ///<summary>
    ///姓名
    ///</summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string RealName { get; set; }

    ///<summary>
    ///手机号
    ///</summary>
    [Required(ErrorMessage = "手机号码不能为空")]
    [RegularExpression(RegexKey.MOBILE, ErrorMessage = "请填写正确的手机号码")]
    public string Mobile { get; set; }

    ///<summary>
    ///邮箱
    ///</summary>
    [EmailAddress(ErrorMessage = "邮箱地址错误")]
    public string Email { get; set; }
  }

  ///<summary>
  ///员工分页Dto
  ///</summary>
  public class EmployeePageDto : PageDto
  {
    ///<summary>
    ///部门编号
    ///</summary>
    public int DepartmentId { get; set; }

    ///<summary>
    ///姓名
    ///</summary>
    public string RealName { get; set; }

    ///<summary>
    ///手机号
    ///</summary>
    public string Mobile { get; set; }
  }
}