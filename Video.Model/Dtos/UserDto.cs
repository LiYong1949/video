using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;

namespace Video.Model
{
    ///<summary>
    ///用户列表Dto
    ///</summary>
    [AutoInjectAttribute(typeof(User))]
    public class UserDto
    {
        ///<summary>
        ///自增列
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///唯一识别号
        ///</summary>
        public string Pid { get; set; }

        ///<summary>
        ///昵称
        ///</summary>
        public string NickName { get; set; }

        ///<summary>
        ///登录密码
        ///</summary>
        public string Password { get; set; }

        ///<summary>
        ///姓名
        ///</summary>
        public string RealName { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///用户类型
        ///</summary>
        public UserTypeEnum UserType { get; set; }

        ///<summary>
        ///个性签名
        ///</summary>
        public string Signature { get; set; }

        ///<summary>
        ///手机号码
        ///</summary>
        public string Mobile { get; set; }

        ///<summary>
        ///头像
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///邮箱地址
        ///</summary>
        public string Email { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        public DateTime? Birthday { get; set; }

        ///<summary>
        ///权限Id
        ///</summary>
        public int PermissionId { get; set; }

        ///<summary>
        ///注册方式
        ///</summary>
        public UserRegisterTypeEnum RegisterType { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人Id
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime LastModifyTime { get; set; }

        ///<summary>
        ///最后登录IP
        ///</summary>
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///微信Pid
        ///</summary>
        public string WeixinPid { get; set; }

        ///<summary>
        ///QQPid
        ///</summary>
        public string QQPid { get; set; }

        ///<summary>
        ///钉钉Pid
        ///</summary>
        public string DingTalkPid { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public UserStatusEnum Status { get; set; }
    }

    ///<summary>
    ///用户新增Dto
    ///</summary>
    [AutoInjectAttribute(typeof(User))]
    public class UserAddDto
    {
        ///<summary>
        ///昵称
        ///</summary>
        public string NickName { get; set; }

        ///<summary>
        ///登录密码
        ///</summary>
        public string Password { get; set; }

        ///<summary>
        ///姓名
        ///</summary>
        public string RealName { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///用户类型
        ///</summary>
        public UserTypeEnum UserType { get; set; }

        ///<summary>
        ///个性签名
        ///</summary>
        [StringLength(500, ErrorMessage = "个性签名长度不能超过500")]
        public string Signature { get; set; }

        ///<summary>
        ///手机号码
        ///</summary>
        [RegularExpression(RegexKey.MOBILE, ErrorMessage = "请填写正确的手机号码")]
        public string Mobile { get; set; }

        ///<summary>
        ///头像
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///邮箱地址
        ///</summary>
        [RegularExpression(RegexKey.EMAIL, ErrorMessage = "请填写正确的邮箱")]
        public string Email { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        public DateTime? Birthday { get; set; }

        ///<summary>
        ///注册方式
        ///</summary>
        public UserRegisterTypeEnum RegisterType { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///微信Pid
        ///</summary>
        [StringLength(100, ErrorMessage = "微信Pid长度不能超过100")]
        public string WeixinPid { get; set; }

        ///<summary>
        ///QQPid
        ///</summary>
        [StringLength(100, ErrorMessage = "QQPid长度不能超过100")]
        public string QQPid { get; set; }

        ///<summary>
        ///钉钉Pid
        ///</summary>
        [StringLength(100, ErrorMessage = "钉钉Pid长度不能超过100")]
        public string DingTalkPid { get; set; }
    }

    ///<summary>
    ///用户修改Dto
    ///</summary>
    [AutoInjectAttribute(typeof(User))]
    public class UserUpdateDto
    {
        ///<summary>
        ///自增列
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///昵称
        ///</summary>
        [StringLength(20, MinimumLength = 2, ErrorMessage = "昵称长度2~20")]
        public string NickName { get; set; }

        ///<summary>
        ///登录密码
        ///</summary>
        [StringLength(32, MinimumLength = 6, ErrorMessage = "密码长度6~32")]
        public string Password { get; set; }

        ///<summary>
        ///姓名
        ///</summary>
        [StringLength(20, MinimumLength = 2, ErrorMessage = "姓名长度2~20")]
        public string RealName { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///用户类型
        ///</summary>
        public UserTypeEnum UserType { get; set; }

        ///<summary>
        ///个性签名
        ///</summary>
        [StringLength(500, ErrorMessage = "个性签名长度须小于500")]
        public string Signature { get; set; }

        ///<summary>
        ///手机号码
        ///</summary>
        [RegularExpression(RegexKey.MOBILE, ErrorMessage = "请填写正确的手机号码")]
        public string Mobile { get; set; }

        ///<summary>
        ///头像
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///邮箱地址
        ///</summary>
        [RegularExpression(RegexKey.EMAIL, ErrorMessage = "请填写正确的邮箱地址")]
        public string Email { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        public DateTime? Birthday { get; set; }

        ///<summary>
        ///权限Id
        ///</summary>
        public int PermissionId { get; set; }

        ///<summary>
        ///注册方式
        ///</summary>
        public UserRegisterTypeEnum RegisterType { get; set; }

        ///<summary>
        ///最后修改人Id
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime LastModifyTime { get; set; } = DateTime.Now;

        ///<summary>
        ///最后登录IP
        ///</summary>
        [RegularExpression(RegexKey.IP, ErrorMessage = "请填写正确的IP")]
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///微信Pid
        ///</summary>
        [StringLength(100, ErrorMessage = "微信Pid长度不能超过100")]
        public string WeixinPid { get; set; }

        ///<summary>
        ///QQPid
        ///</summary>
        [StringLength(100, ErrorMessage = "QQPid长度不能超过100")]
        public string QQPid { get; set; }

        ///<summary>
        ///钉钉Pid
        ///</summary>
        [StringLength(100, ErrorMessage = "钉钉Pid长度不能超过100")]
        public string DingTalkPid { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public UserStatusEnum Status { get; set; }
    }

    ///<summary>
    ///用户分页查询Dto
    ///</summary>
    public class UserPageDto : PageDto
    {
        ///<summary>
        ///昵称
        ///</summary>
        public string NickName { get; set; }

        ///<summary>
        ///手机号
        ///</summary>
        public string Mobile { get; set; }

        ///<summary>
        ///注册起始时间
        ///</summary>
        public DateTime RegisterStartTime { get; set; }

        ///<summary>
        ///注册结束时间
        ///</summary>
        public DateTime RegisterEndTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; } = -1;
    }

    ///<summary>
    ///用户修改密码Dto
    ///</summary>
    public class UserUpdatePasswordDto
    {
        ///<summary>
        ///旧密码
        ///</summary>
        [Required(ErrorMessage = "旧密码不能为空")]
        [StringLength(32, MinimumLength = 6, ErrorMessage = "密码长度6~32")]
        public string OldPassword { get; set; }

        ///<summary>
        ///新密码
        ///</summary>
        [Required(ErrorMessage = "新密码不能为空")]
        [StringLength(32, MinimumLength = 6, ErrorMessage = "密码长度6~32")]
        public string NewPassword { get; set; }
    }
}
