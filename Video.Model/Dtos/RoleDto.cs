﻿using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Video.Common;
using System;

namespace Video.Model
{
    /// <summary>
    /// 角色Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Role))]
    public class RoleDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///角色名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string Remark { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public RoleStatusEnum Status { get; set; }

    }

    /// <summary>
    /// 角色添加Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Role))]
    public class RoleAddDto
    {
        ///<summary>
        ///角色名称
        ///</summary>
        [Required(ErrorMessage = "路由名称不能为空")]
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string Remark { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public RoleStatusEnum Status { get; set; }
    }

    /// <summary>
    /// 角色修改Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Role))]
    public class RoleUpdateDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///角色名称
        ///</summary>
        [Required(ErrorMessage = "路由名称不能为空")]
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string Remark { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; } = DateTime.Now;

        ///<summary>
        ///状态
        ///</summary>
        public RoleStatusEnum Status { get; set; }
    }

    /// <summary>
    /// 角色分页Dto
    /// </summary>
    public class RolePageDto : PageDto
    {
        ///<summary>
        ///角色名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public int Status { get; set; } = -1;
    }
}
