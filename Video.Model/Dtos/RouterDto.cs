﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Video.Common;

namespace Video.Model
{
    /// <summary>
    /// 路由Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Router))]
    public class RouterDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 父级编号
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 权限码
        /// </summary>
        public string PermissionCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>		
        public int Sort { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }

        ///<summary>
        ///按钮组
        ///</summary>
        public string Icons { get; set; }

        /// <summary>
        /// 跳转
        /// </summary>
        public string Redirect { get; set; }

        /// <summary>
        /// 创建人编号
        /// </summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; }
    }

    /// <summary>
    /// 路由树
    /// </summary>
    [AutoInjectAttribute(typeof(Router))]
    public class RouterTreeDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 父级编号
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 权限码
        /// </summary>
        public string PermissionCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>		
        public int Sort { get; set; }

        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }

        ///<summary>
        ///按钮组
        ///</summary>
        public string Icons { get; set; }

        /// <summary>
        /// 跳转
        /// </summary>
        public string Redirect { get; set; }

        /// <summary>
        /// 附加参数
        /// </summary>
        public object Meta { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public List<RouterTreeDto> Children { get; set; }
    }

    /// <summary>
    /// 路由添加Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Router))]
    public class RouterAddDto
    {
        /// <summary>
        /// 路由名称
        /// </summary>		
        public string Name { get; set; }

        /// <summary>
        /// 父级编号
        /// </summary>		
        public int ParentId { get; set; }

        /// <summary>
        /// 权限码
        /// </summary>		
        public string PermissionCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>		
        public int Sort { get; set; }

        /// <summary>
        /// 路径
        /// </summary>		
        public string Path { get; set; }

        /// <summary>
        /// 组件路径
        /// </summary>		
        public string Component { get; set; }

        /// <summary>
        /// 跳转路径
        /// </summary>		
        public string Redirect { get; set; }

        ///<summary>
        ///按钮组
        ///</summary>
        public string Icons { get; set; }

        /// <summary>
        /// 创建人编号
        /// </summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }

    /// <summary>
    /// 路由修改Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Router))]
    public class RouterUpdateDto
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 路由名称
        /// </summary>		
        [Required(ErrorMessage = "路由名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 父级编号
        /// </summary>		
        public int ParentId { get; set; }

        /// <summary>
        /// 权限码
        /// </summary>		
        public string PermissionCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>		
        public int Sort { get; set; }

        /// <summary>
        /// 路径
        /// </summary>		
        [Required(ErrorMessage = "路径不能为空")]
        public string Path { get; set; }

        /// <summary>
        /// 组件路径
        /// </summary>		
        public string Component { get; set; }

        /// <summary>
        /// 跳转路径
        /// </summary>		
        public string Redirect { get; set; }

        ///<summary>
        ///按钮组
        ///</summary>
        public string Icons { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; }
    }
}
