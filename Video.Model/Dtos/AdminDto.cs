﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Video.Common;

namespace Video.Model
{
    /// <summary>
    /// 管理员Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Admin))]
    public class AdminDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///账户
        ///</summary>
        public string Account { get; set; }

        // ///<summary>
        // ///密码
        // ///</summary>
        // public string Password { get; set; }

        ///<summary>
        ///名字
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///员工编号
        ///</summary>
        public int EmployeeId { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///角色名称
        ///</summary>
        public string RoleName{get;set;}

        ///<summary>
        ///创建人编号
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///最后登录IP
        ///</summary>
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public AdminStatusEnum Status { get; set; }
    }

    [AutoInjectAttribute(typeof(Admin))]
    public class AdminAddDto
    {
        ///<summary>
        ///账户
        ///</summary>
        public string Account { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        public string Password { get; set; }

        ///<summary>
        ///名字
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///员工编号
        ///</summary>
        public int EmployeeId { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public AdminStatusEnum Status { get; set; } = AdminStatusEnum.Normal;
    }

    /// <summary>
    /// 管理员修改Dto
    /// </summary>
    [AutoInjectAttribute(typeof(Admin))]
    public class AdminUpdateDto
    {
        ///<summary>
        ///编号
        ///</summary>
        public int Id { get; set; }

        ///<summary>
        ///账户
        ///</summary>
        public string Account { get; set; }

        ///<summary>
        ///名字
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        public string Avatar { get; set; }

        ///<summary>
        ///员工编号
        ///</summary>
        public int EmployeeId { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///最后登录IP
        ///</summary>
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        public bool IsRoot { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public AdminStatusEnum Status { get; set; }
    }

    /// <summary>
    /// 管理员分页Dto
    /// </summary>
    public class AdminPageDto : PageDto
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; } = -1;
    }

    public class AdminUpdatePasswordDto
    {
        ///<summary>
        ///旧密码
        ///</summary>
        [Required(ErrorMessage = "旧密码不能为空")]
        [StringLength(32, MinimumLength = 6, ErrorMessage = "密码长度6~32")]
        public string OldPassword { get; set; }

        ///<summary>
        ///新密码
        ///</summary>
        [Required(ErrorMessage = "新密码不能为空")]
        [StringLength(32, MinimumLength = 6, ErrorMessage = "密码长度6~32")]
        public string NewPassword { get; set; }
    }
}
