using System.ComponentModel.DataAnnotations;
using Video.Common;

namespace Video.Model
{

    ///<summary>
    ///分页Dto
    ///</summary>
    public class PageDto
    {
        ///<summary>
        ///当前页
        ///</summary>
        public int PageIndex { get; set; } = 1;

        ///<summary>
        ///每页条数
        ///</summary>
        public int PageSize { get; set; } = 20;

        ///<summary>
        ///关键字
        ///</summary>
        public string Keyword { get; set; }

        ///<summary>
        ///排序表达式
        ///</summary>
        public string Sort { get; set; }
    }

    ///<summary>
    ///回调Dto
    ///</summary>
    public class PageResultDto
    {
        ///<summary>
        ///数据总数
        ///</summary>
        public int Total { get; set; } = 0;

        ///<summary>
        ///数据
        ///</summary>
        public object Data { get; set; }
    }
}