﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Video.Model
{
    /// <summary>
    /// 管理员状态
    /// </summary>
    public enum AdminStatusEnum
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 0,

        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Lock = 1
    }
}
