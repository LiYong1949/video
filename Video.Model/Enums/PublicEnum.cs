/*
 * @Author: liyong
 * @Date: 2020-12-07 20:19:32
 * @LastEditors: liyong
 * @LastEditTime: 2020-12-07 20:25:00
 * @Description: 公共枚举
 */
using System.ComponentModel;

namespace Video.Model
{
    ///<summary>
    ///性别
    ///</summary>
    public enum GenderTypeEnum
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        UnKnown = 0,

        /// <summary>
        /// 男性
        /// </summary>
        [Description("男性")]
        Male = 1,

        /// <summary>
        /// 女性
        /// </summary>
        [Description("女性")]
        Female = 2,
    }
}
