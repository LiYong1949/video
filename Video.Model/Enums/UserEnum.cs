using System.ComponentModel;

namespace Video.Model
{
    ///<summary>
    ///用户注册方式
    ///</summary>
    public enum UserRegisterTypeEnum
    {
        /// <summary>
        /// 站内添加
        /// </summary>
        [Description("站内添加")]
        OnSite = 1,

        /// <summary>
        /// 微信注册
        /// </summary>
        [Description("微信注册")]
        Weixin = 2,

        /// <summary>
        /// QQ注册
        /// </summary>
        [Description("QQ注册")]
        QQ = 3,

        /// <summary>
        /// 钉钉注册
        /// </summary>
        [Description("钉钉注册")]
        DingTalk = 4,

        /// <summary>
        /// 手机号注册
        /// </summary>
        [Description("手机号注册")]
        Mobile = 5,
    }

    ///<summary>
    ///用户类型
    ///</summary>
    public enum UserTypeEnum
    {
        /// <summary>
        /// 内部员工
        /// </summary>
        [Description("内部员工")]
        Workers = 1,

        /// <summary>
        /// 普通用户
        /// </summary>
        [Description("普通用户")]
        GeneralUser = 2,

        /// <summary>
        /// 商家
        /// </summary>
        [Description("商家")]
        Merchant = 3,
    }

    ///<summary>
    ///用户状态
    ///</summary>
    public enum UserStatusEnum
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 0,

        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Lock = 1,

        /// <summary>
        /// 注销
        /// </summary>
        [Description("注销")]
        Logout = 2,
    }
}
