/*
 * @Author: liyong
 * @Date: 2020-10-16 09:21:15
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-11 16:59:11
 * @Description: 员工Enum
 */
using System.ComponentModel;

namespace Video.Model
{
  ///<summary>
  ///员工状态
  ///</summary>
  public enum EmployeeStatusEnum
  {
    /// <summary>
    /// 正常
    /// </summary>
    [Description("正常")]
    On = 0,

    /// <summary>
    /// 离职
    /// </summary>
    [Description("离职")]
    Lock = 1,
  }
}