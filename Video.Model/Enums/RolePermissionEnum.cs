using System.ComponentModel;

namespace Video.Model
{
    /// <summary>
    /// 权限状态
    /// </summary>
    public enum PermissionStatusEnum
    {
        /// <summary>
        /// 启用
        /// </summary>
        [Description("启用")]
        On = 1,

        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Lock = 2,
    }

    /// <summary>
    /// 权限按钮枚举
    /// </summary>
    public enum AuthIconEnum
    {
        /// <summary>
        /// 搜索
        /// </summary>
        [Description("搜索")]
        Search = 1,

        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        Add = 2,

        /// <summary>
        /// 编辑
        /// </summary>
        [Description("编辑")]
        Edit = 3,

        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delted = 4,

        /// <summary>
        /// 导出
        /// </summary>
        [Description("导出")]
        Export = 5,

        /// <summary>
        /// 导入
        /// </summary>
        [Description("导入")]
        Import = 6,

        /// <summary>
        /// 授权
        /// </summary>
        [Description("授权")]
        Auth = 7
    }
}