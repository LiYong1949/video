using System.ComponentModel;

namespace Video.Model
{
    ///<summary>
    ///菜单跳转目标
    ///</summary>
    public enum MenuTargetEnum
    {
        /// <summary>
        /// 路由
        /// </summary>
        [Description("路由")]
        Router = 0,

        /// <summary>
        /// 外部链接
        /// </summary>
        [Description("外部链接")]
        Link = 1
    }
}
