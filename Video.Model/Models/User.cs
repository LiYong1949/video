/*
 * @Author: liyong
 * @Date: 2020-12-07 19:50:02
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-08 21:56:54
 * @Description: 用户
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    ///<summary>
    ///用户
    ///</summary>
    [Table("user")]
    public class User : IEntity
    {
        ///<summary>
        ///自增列
        ///</summary>
        [Key]
        [Description("自增列")]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///唯一识别号
        ///</summary>
        [Description("唯一识别号")]
        [Column("pid")]
        [Required]
        public string Pid { get; set; }

        ///<summary>
        ///登录账号
        ///</summary>
        [Description("登录账号")]
        [Column("account")]
        [MaxLength(100)]
        [Required]
        public string Account { get; set; }

        ///<summary>
        ///昵称
        ///</summary>
        [Description("昵称")]
        [Column("nick_name")]
        [MaxLength(50)]
        public string NickName { get; set; }

        ///<summary>
        ///登录密码
        ///</summary>
        [Description("登录密码")]
        [Column("password")]
        [MaxLength(100)]
        public string Password { get; set; }

        ///<summary>
        ///姓名
        ///</summary>
        [Description("姓名")]
        [Column("real_name")]
        [MaxLength(50)]
        public string RealName { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        [Description("性别")]
        [Column("gender")]
        [DefaultValue(0)]
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///用户类型
        ///</summary>
        [Description("用户类型")]
        [Column("user_type")]
        [DefaultValue(0)]
        public UserTypeEnum UserType { get; set; }

        ///<summary>
        ///个性签名
        ///</summary>
        [Description("个性签名")]
        [Column("signature")]
        [MaxLength(500)]
        public string Signature { get; set; }

        ///<summary>
        ///手机号码
        ///</summary>
        [Description("手机号码")]
        [Column("mobile")]
        [MaxLength(20)]
        public string Mobile { get; set; }

        ///<summary>
        ///头像
        ///</summary>
        [Description("头像")]
        [Column("avatar")]
        [MaxLength(200)]
        public string Avatar { get; set; }

        ///<summary>
        ///邮箱地址
        ///</summary>
        [Description("邮箱地址")]
        [Column("email")]
        [MaxLength(50)]
        public string Email { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        [Description("生日")]
        [Column("birthday")]
        public DateTime? Birthday { get; set; }

        ///<summary>
        ///权限Id
        ///</summary>
        [Description("权限Id")]
        [Column("permission_id")]
        [DefaultValue(0)]
        public int PermissionId { get; set; }

        ///<summary>
        ///注册方式
        ///</summary>
        [Description("注册方式")]
        [Column("register_type")]
        [DefaultValue(0)]
        public UserRegisterTypeEnum RegisterType { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        [Description("创建人Id")]
        [Column("creator")]
        [DefaultValue(0)]
        public int Creator { get; set; } = 0;

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人Id
        ///</summary>
        [Description("最后修改人Id")]
        [Column("last_modifier")]
        [DefaultValue(0)]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///最后登录IP
        ///</summary>
        [Description("最后登录IP")]
        [Column("last_login_ip")]
        [MaxLength(50)]
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        [Description("最后登录时间")]
        [Column("last_login_time")]
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        [Description("是否内置账号")]
        [Column("is_root", TypeName = "bit")]
        [DefaultValue(0)]
        public bool IsRoot { get; set; }

        ///<summary>
        ///微信Pid
        ///</summary>
        [Description("微信Pid")]
        [Column("wx_pid")]
        [MaxLength(100)]
        public string WeixinPid { get; set; }

        ///<summary>
        ///QQPid
        ///</summary>
        [Description("QQPid")]
        [Column("qq_pid")]
        [MaxLength(100)]
        public string QQPid { get; set; }

        ///<summary>
        ///钉钉Pid
        ///</summary>
        [Description("钉钉Pid")]
        [Column("dingtalk_pid")]
        [MaxLength(100)]
        public string DingTalkPid { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        [Description("状态")]
        [Column("status")]
        [DefaultValue(0)]
        public UserStatusEnum Status { get; set; }

        ///<summary>
        ///删除标志
        ///</summary>
        [Description("删除标志")]
        [Column("is_deleted", TypeName = "bit")]
        [DefaultValue(0)]
        public bool IsDeleted { get; set; }
    }
}
