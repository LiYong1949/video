/*
 * @Author: liyong
 * @Date: 2020-06-05 12:14:08
 * @LastEditors: liyong
 * @LastEditTime: 2020-11-03 22:07:32
 * @Description: 部门Model
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    ///<summary>
    ///部门
    ///</summary>
    [Table("department")]
    public class Department : IEntity
    {
        ///<summary>
        ///编号
        ///</summary>
        [Column("id")]
        [Description("编号")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///部门名称
        ///</summary>
        [Description("部门名称")]
        [Column("name")]
        [MaxLength(20)]
        [Required]
        public string Name { get; set; }

        ///<summary>
        ///上级部门编号
        ///</summary>
        [Description("上级部门编号")]
        [Column("parent_id")]
        public int ParentId { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("creator")]
        [Required]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        [Description("最后修改人编号")]
        [Column("last_modifier")]
        [Required]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        [Required]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///删除标志
        ///</summary>
        [Description("删除标志")]
        [Column("is_deleted", TypeName = "bit")]
        public bool IsDeleted { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Description("备注")]
        [Column("remark")]
        [MaxLength(500)]
        public string Remark { get; set; }
    }
}