/*
 * @Author: liyong
 * @Date: 2020-08-11 20:52:17
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-16 14:42:23
 * @Description: 菜单Model
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    ///<summary>
    ///菜单
    ///</summary>
    [Table("menu")]
    public class Menu : IEntity
    {
        ///<summary>
        ///主键
        ///</summary>
        [Key]
        [Description("自增列")]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///菜单名称
        ///</summary>
        [Description("菜单名称")]
        [Column("name")]
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        ///<summary>
        ///父级Id
        ///</summary>
        [Description("父级Id")]
        [Column("parent_id")]
        public int ParentId { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [Description("排序")]
        [Column("sort")]
        public int Sort { get; set; }

        ///<summary>
        ///菜单图标
        ///</summary>
        [Description("菜单图标")]
        [Column("icon")]
        [MaxLength(50)]
        public string Icon { get; set; }

        ///<summary>
        ///链接地址
        ///</summary>
        [Description("链接地址")]
        [Column("link")]
        [MaxLength(200)]
        public string Link { get; set; }

        ///<summary>
        ///路由地址
        ///</summary>
        [Description("路由地址")]
        [Column("router_path")]
        [MaxLength(50)]
        public string RouterPath { get; set; }

        ///<summary>
        ///跳转目标
        ///</summary>
        [Description("跳转目标")]
        [Column("target")]
        [MaxLength(100)]
        public MenuTargetEnum Target { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        [Description("权限码")]
        [Column("permission_code")]
        [MaxLength(50)]
        public string PermissionCode { get; set; }

        ///<summary>
        ///权限按钮组
        ///</summary>
        [Description("权限按钮组")]
        [Column("auth_buttons")]
        [MaxLength(50)]
        public string AuthButtons { get; set; }


        /// <summary>
        /// 是否根菜单
        /// </summary>	
        [Description("是否根菜单")]
        [Column("is_group")]
        [DefaultValue(50)]
        public bool IsGroup { get; set; }

        ///<summary>
        ///创建人Id
        ///</summary>
        [Description("创建人Id")]
        [Column("creator")]
        [Required]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人Id
        ///</summary>
        [Description("最后修改人Id")]
        [Column("last_modifier")]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///是否显示
        ///</summary>
        [Description("是否显示")]
        [Column("visible", TypeName = "bit")]
        public bool Visible { get; set; }

        ///<summary>
        ///删除标志
        ///</summary>
        [Description("删除标志")]
        [Column("is_deleted", TypeName = "bit")]
        public bool IsDeleted { get; set; }
    }
}