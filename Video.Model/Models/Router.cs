/*
* @Author: liyong
* @Date: 2021-03-02 20:59:07
 * @LastEditors: liyong
 * @LastEditTime: 2021-04-26 21:40:15
* @Description: 路由
*/
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Video.Model
{
    ///<summary>
    ///路由
    ///</summary>
    [Table("router")]
    public class Router : IEntity
    {
        ///<summary>
        ///主键
        ///</summary>
        [Key]
        [Description("自增列")]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///路由名称
        ///</summary>
        [Description("路由名称")]
        [Column("name")]
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        ///<summary>
        ///父级编号
        ///</summary>
        [Description("父级编号")]
        [Column("parent_id")]
        public int ParentId { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [Description("排序")]
        [Column("sort")]
        public int Sort { get; set; }

        ///<summary>
        ///页面路径
        ///</summary>
        [Description("页面路径")]
        [Column("path")]
        [MaxLength(100)]
        [Required]
        public string Path { get; set; }

        ///<summary>
        ///组件名称
        ///</summary>
        [Description("组件名称")]
        [Column("component")]
        [MaxLength(100)]
        public string Component { get; set; }

        ///<summary>
        ///跳转路径
        ///</summary>
        [Description("跳转路径")]
        [Column("redirect")]
        [MaxLength(100)]
        public string Redirect { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        [Description("权限码")]
        [Column("permission_code")]
        [MaxLength(50)]
        public string PermissionCode { get; set; }

        ///<summary>
        ///按钮组
        ///</summary>
        [Description("按钮组")]
        [Column("icons")]
        [MaxLength(500)]
        public string Icons { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("creator")]
        [Required]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        [Description("最后修改人编号")]
        [Column("last_modifier")]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///删除标志
        ///</summary>
        [Description("删除标志")]
        [Column("is_deleted", TypeName = "bit")]
        public bool IsDeleted { get; set; }
    }
}
