/*
 * @Author: liyong
 * @Date: 2020-06-04 12:38:43
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-21 20:42:07
 * @Description: 用户权限Model
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    /// <summary>
    /// 权限
    /// </summary>
    [Table("role_permission")]
    public class RolePermission : IEntity
    {
        ///<summary>
        ///主键
        ///</summary>
        [Column(name: "编号")]
        [Description("编号")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        [Description("角色编号")]
        [Column("role_id")]
        [Required]
        public int RoleId { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        [Description("权限码")]
        [Column("permission_code")]
        [Required]
        [MaxLength(50)]
        public string PermissionCode { get; set; }

        ///<summary>
        ///按钮组权限
        ///</summary>
        [Description("按钮组权限")]
        [Column("auth_buttons")]
        [MaxLength(50)]
        public string AuthButtons { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("creator")]
        [Required]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

    }
}