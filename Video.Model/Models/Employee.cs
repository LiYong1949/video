/*
 * @Author: liyong
 * @Date: 2020-06-04 12:38:43
 * @LastEditors: liyong
 * @LastEditTime: 2021-06-11 16:15:07
 * @Description: 员工Model
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
  ///<summary>
  ///用户
  ///</summary>
  [Table("employee")]
  public class Employee : IEntity
  {
    ///<summary>
    ///编号
    ///</summary>
    [Key]
    [Description("编号")]
    [Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    ///<summary>
    ///工号
    ///</summary>
    [Description("工号")]
    [Column("job_no")]
    [MaxLength(20)]
    public string JobNo { get; set; }

    ///<summary>
    ///姓名
    ///</summary>
    [Description("姓名")]
    [Column("real_name")]
    [MaxLength(20)]
    [Required]
    public string RealName { get; set; }

    ///<summary>
    ///手机号码
    ///</summary>
    [Description("手机号码")]
    [Column("mobile")]
    [MaxLength(20)]
    [Required]
    public string Mobile { get; set; }

    ///<summary>
    ///头像
    ///</summary>
    [Description("头像")]
    [Column("avatar")]
    [MaxLength(200)]
    public string Avatar { get; set; }

    ///<summary>
    ///性别
    ///</summary>
    [Description("性别")]
    [Column("gender")]
    [DefaultValue(0)]
    public GenderTypeEnum Gender { get; set; }

    ///<summary>
    ///邮箱地址
    ///</summary>
    [Description("邮箱地址")]
    [Column("email")]
    [MaxLength(50)]
    public string Email { get; set; }

    ///<summary>
    ///入职时间
    ///</summary>
    [Description("入职时间")]
    [Column("entry_time")]
    public DateTime? EntryTime { get; set; }

    ///<summary>
    ///离职时间
    ///</summary>
    [Description("离职时间")]
    [Column("resign_time")]
    public DateTime? ResignTime { get; set; }

    ///<summary>
    ///生日
    ///</summary>
    [Description("生日")]
    [Column("birthday")]
    public DateTime? Birthday { get; set; }

    ///<summary>
    ///部门编号
    ///</summary>
    [Description("部门编号")]
    [Column("department_id")]
    [DefaultValue(0)]
    public int DepartmentId { get; set; }

    ///<summary>
    ///职位编号
    ///</summary>
    [Description("职位编号")]
    [Column("position_id")]
    [DefaultValue(0)]
    public int PositionId { get; set; }

    ///<summary>
    ///备注
    ///</summary>
    [Description("备注")]
    [Column("remark")]
    [MaxLength(500)]
    public string Remark { get; set; }

    ///<summary>
    ///创建人编号
    ///</summary>
    [Description("创建人编号")]
    [Column("creator")]
    [Required]
    [DefaultValue(0)]
    public int Creator { get; set; }

    ///<summary>
    ///创建时间
    ///</summary>
    [Description("创建时间")]
    [Column("create_time")]
    [Required]
    public DateTime CreateTime { get; set; }

    ///<summary>
    ///最后修改人编号
    ///</summary>
    [Description("最后修改人编号")]
    [Column("last_modifier")]
    [DefaultValue(0)]
    public int LastModifier { get; set; }

    ///<summary>
    ///最后修改时间
    ///</summary>
    [Description("最后修改时间")]
    [Column("last_modify_time")]
    public DateTime? LastModifyTime { get; set; }

    ///<summary>
    ///状态
    ///</summary>
    [Description("状态")]
    [Column("status")]
    [DefaultValue(0)]
    public EmployeeStatusEnum Status { get; set; }

    ///<summary>
    ///删除标志
    ///</summary>
    [Description("删除标志")]
    [Column("is_deleted", TypeName = "bit")]
    [DefaultValue(0)]
    public bool IsDeleted { get; set; }
  }
}