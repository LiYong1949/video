using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    ///<summary>
    ///角色
    ///</summary>
    [Table("role")]
    public class Role : IEntity
    {
        ///<summary>
        ///主键
        ///</summary>
        [Key]
        [Description("主键")]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///角色名称
        ///</summary>
        [Description("角色名称")]
        [Column("name")]
        [MaxLength(20)]
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Description("备注")]
        [Column("remark")]
        [MaxLength(200)]
        public string Remark { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("creator")]
        [DefaultValue(0)]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改编号
        ///</summary>
        [Description("最后修改编号")]
        [Column("last_modifier")]
        [DefaultValue(0)]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        [Description("状态")]
        [Column("status")]
        [DefaultValue(0)]
        public RoleStatusEnum Status { get; set; }

        ///<summary>
        ///是否逻辑删除
        ///</summary>
        [Description("是否逻辑删除")]
        [Column("is_deleted", TypeName = "bit")]
        [DefaultValue(0)]
        public bool IsDeleted { get; set; } = false;
    }
}
