/*
 * @Author: liyong
 * @Date: 2021-03-02 21:08:37
 * @LastEditors: liyong
 * @LastEditTime: 2021-05-11 21:38:31
 * @Description: 后台管理员Model
 */
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Video.Model
{
    ///<summary>
    ///后台管理员
    ///</summary>
    [Table("admin")]
    public class Admin : IEntity
    {
        ///<summary>
        ///编号
        ///</summary>
        [Key]
        [Description("编号")]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        ///<summary>
        ///账户
        ///</summary>
        [Description("账户")]
        [Column("account")]
        [MaxLength(20)]
        public string Account { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        [Description("密码")]
        [Column("password")]
        [MaxLength(50)]
        public string Password { get; set; }

        ///<summary>
        ///名字
        ///</summary>
        [Description("名字")]
        [Column("name")]
        [MaxLength(20)]
        public string Name { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        [Description("性别")]
        [Column("gender")]
        [DefaultValue(0)]
        public GenderTypeEnum Gender { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("avatar")]
        [MaxLength(20)]
        public string Avatar { get; set; }

        ///<summary>
        ///员工编号
        ///</summary>
        [Description("员工编号")]
        [Column("employee_id")]
        [DefaultValue(0)]
        public int EmployeeId { get; set; }

        ///<summary>
        ///角色编号
        ///</summary>
        [Description("角色编号")]
        [Column("role_id")]
        [DefaultValue(0)]
        public int RoleId { get; set; }

        ///<summary>
        ///创建人编号
        ///</summary>
        [Description("创建人编号")]
        [Column("creator")]
        [DefaultValue(0)]
        public int Creator { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Description("创建时间")]
        [Column("create_time")]
        [Required]
        public DateTime CreateTime { get; set; }

        ///<summary>
        ///最后修改人编号
        ///</summary>
        [Description("最后修改人编号")]
        [Column("last_modifier")]
        [DefaultValue(0)]
        public int LastModifier { get; set; }

        ///<summary>
        ///最后修改时间
        ///</summary>
        [Description("最后修改时间")]
        [Column("last_modify_time")]
        public DateTime? LastModifyTime { get; set; }

        ///<summary>
        ///最后登录IP
        ///</summary>
        [Description("最后登录IP")]
        [Column("last_login_ip")]
        [MaxLength(50)]
        public string LastLoginIP { get; set; }

        ///<summary>
        ///最后登录时间
        ///</summary>
        [Description("最后登录时间")]
        [Column("last_login_time")]
        public DateTime? LastLoginTime { get; set; }

        ///<summary>
        ///是否内置账号
        ///</summary>
        [Description("是否内置账号")]
        [Column("is_root", TypeName = "bit")]
        [DefaultValue(0)]
        public bool IsRoot { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        [Description("状态")]
        [Column("status")]
        [DefaultValue(0)]
        public AdminStatusEnum Status { get; set; }

        ///<summary>
        ///删除标志
        ///</summary>
        [Description("删除标志")]
        [Column("is_deleted", TypeName = "bit")]
        [DefaultValue(0)]
        public bool IsDeleted { get; set; }
    }
}
