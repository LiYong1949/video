using System;
using System.Collections.Generic;
using System.Text;

namespace Video.Model
{
    /// <summary>
    /// API 返回JSON字符串
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// 状态码 
        /// </summary>
        public int Code { get; set; } = 200;

        /// <summary>
        /// success
        /// </summary>
        public bool Success { get; set; } = true;

        /// <summary>
        /// 信息
        /// </summary>
        public string Message { get; set; } = "success";

        /// <summary>
        /// 数据集
        /// </summary>
        public object Data { get; set; }
    }
}
