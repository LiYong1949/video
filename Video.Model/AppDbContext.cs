using System.Reflection;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Video.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    public static readonly LoggerFactory MyLoggerFactory = new LoggerFactory(new[] {
            new DebugLoggerProvider()
        });


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        optionsBuilder.EnableSensitiveDataLogging();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
        var entityTypes = Assembly.Load(new AssemblyName(assemblyName)).GetTypes()
            .Where(type => !string.IsNullOrWhiteSpace(type.Namespace))
            .Where(type => type.GetTypeInfo().IsClass)
            .Where(type => type.GetTypeInfo().BaseType != null)
            .Where(type => typeof(IEntity).IsAssignableFrom(type)).ToList();

        foreach (var entityType in entityTypes)
        {
            if (entityType == null)
                continue;

            //  防止重复附加模型，否则会在生成指令中报错
            if (builder.Model.FindEntityType(entityType) != null)
                continue;

            builder.Model.AddEntityType(entityType);
        }

        base.OnModelCreating(builder);
    }
}