using System;
using System.Collections.Generic;

namespace Video.Common
{
    public static class DataTypeExtensions
    {
        public static int ToInt32(this object p, int defaultValue = 0)
        {
            try
            {
                return Convert.ToInt32(p);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static bool ToBoolean(this object p, bool defaultValue = false)
        {
            try
            {
                var value = ToStringEx(p);
                return string.Equals("true", value, StringComparison.OrdinalIgnoreCase) || string.Equals("1", value) || Convert.ToBoolean(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static DateTime ToDateTime(this object p)
        {
            return p.ToDateTime(DateTime.MinValue);
        }
        public static DateTime ToDateTime(this object p, DateTime defaultValue)
        {
            try
            {
                return Convert.ToDateTime(p);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static decimal ToDecimal(this object p, decimal defaultValue = 0)
        {
            try
            {
                return Convert.ToDecimal(p);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static double ToDouble(this object p, double defaultValue = 0)
        {
            try
            {
                return Convert.ToDouble(p);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static float ToSingle(this object p, float defaultValue = 0)
        {
            try
            {
                return Convert.ToSingle(p);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string ToStringEx(this object p, string defaultValue = "")
        {
            try
            {
                return p.ToString();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string[] ToArray(this object p, char separator = ',')
        {
            try
            {
                var pStr = p.ToStringEx();
                if (!string.IsNullOrEmpty(pStr))
                    return p.ToStringEx().Split(separator);
            }
            catch { }

            return new string[] { };
        }

        public static int[] ToIntArray(this object p, char separator = ',')
        {
            var list = new List<int>();

            foreach (var it in p.ToArray(separator))
            {
                list.Add(it.ToInt32());
            }

            return list.ToArray();
        }

        public static List<int> ToIntList(this object p, char separator = ',')
        {
            var list = new List<int>();

            foreach (var it in p.ToArray(separator))
            {
                list.Add(it.ToInt32());
            }

            return list;
        }
    }
}