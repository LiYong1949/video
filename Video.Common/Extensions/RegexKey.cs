namespace Video.Common
{
    public class RegexKey
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        public const string MOBILE = @"^1[3456789]\d{9}$";

        /// <summary>
        /// 邮箱地址
        /// </summary>
        public const string EMAIL = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,5})+$";

        /// <summary>
        /// 货币
        /// </summary>
        public const string MONEY = @"^([\d]+)\.([\d]{2})$";

        /// <summary>
        /// 座机号码
        /// </summary>
        public const string PHONENUMBER = @"\d{3}-\d{8}(-\d+)|\d{4}-\d{7}(-\d+)$";

        /// <summary>
        /// 数字
        /// </summary>
        public const string NUMBER = @"\d?";

        /// <summary>
        /// IP
        /// </summary>
        public const string IP = @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$";

        /// <summary>
        /// 身份证号码
        /// </summary>
        public const string IDCARD = @"(^\d{15}$)|(^\d{18}$)|(^\d{17}(X|x)$)";

        /// <summary>
        /// URL
        /// </summary>
        public const string URL = @"^((http|https)://)?(www.)?[^\.]+(\.(com|net|cn|com\.cn|com\.net|net\.cn))(/[^\s\n]*)?$";
    }
}