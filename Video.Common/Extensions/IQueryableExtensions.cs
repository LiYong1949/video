using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Video.Common
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> OrderByBatch<T>(this IQueryable<T> query, string orders)
        {
            var typeObj = typeof(T);
            //创建表达式变量参数
            var parameter = Expression.Parameter(typeObj);

            orders = orders.Substring(0, 1).ToUpper() + orders.Substring(1);
            var orderArr = orders.Trim().Split(',');

            if (orderArr != null && orderArr.Length > 0)
            {
                for (int i = 0; i < orderArr.Length; i++)
                {
                    var items = orderArr[i].Trim().Split(' ');

                    //根据属性名获取属性
                    var property = typeObj.GetProperty(items[0]);
                    //创建一个访问属性的表达式
                    var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                    var orderByExp = Expression.Lambda(propertyAccess, parameter);

                    var orderStr = i > 0
                        ? IsDesc(items) ? "ThenByDescending" : "ThenBy"
                        : IsDesc(items) ? "OrderByDescending" : "OrderBy";

                    MethodCallExpression resultExp = Expression.Call(typeof(Queryable), orderStr, new Type[] { typeObj, property.PropertyType }, query.Expression, Expression.Quote(orderByExp));

                    query = query.Provider.CreateQuery<T>(resultExp);
                }
            }

            return query;
        }

        private static bool IsDesc(string[] items)
        {
            return items.Length == 2 && items[1].Trim().ToLower().StartsWith("desc");
        }
    }
}