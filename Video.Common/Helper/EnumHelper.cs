using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Video.Common
{
    /// <summary>
    /// 枚举操作类
    /// </summary>
    public class EnumHelper
    {

        /// <summary>
        /// 获取枚举的描述信息
        /// </summary>
        /// <param name="enumType">枚举对象</param>
        /// <param name="val">枚举值</param>
        public static string GetEnumDescription(Type enumType, object val)
        {
            string enumvalue = Enum.GetName(enumType, val);
            if (enumvalue == string.Empty || enumvalue == null)
                return "";
            FieldInfo finfo = enumType.GetField(enumvalue);
            object[] cAttr = finfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (cAttr.Length > 0)
            {
                DescriptionAttribute desc = cAttr[0] as DescriptionAttribute;
                if (desc != null)
                {
                    return desc.Description;
                }
            }
            return enumvalue;
        }

        /// <summary>
        /// 返回枚举项目
        /// 获取枚举的名称[Name],值[Value],备注[Description]的集合。
        /// 调用如：EnumHelper.EnumToDataTable(typeof(IsOpen))
        /// </summary>
        /// <param name="enumType"></param>
        public static DataTable EnumToDataTable(Type enumType)
        {
            DataTable dataEnum = new DataTable();
            dataEnum.Columns.Add("Name");
            dataEnum.Columns.Add("Value");
            dataEnum.Columns.Add("Description");

            string[] names = Enum.GetNames(enumType);
            Array values = Enum.GetValues(enumType);

            for (int i = 0; i < values.Length; i++)
            {
                var desc = GetEnumDescription(enumType, values.GetValue(i));

                DataRow row = dataEnum.NewRow();
                row[0] = names[i];
                row[1] = values.GetValue(i).GetHashCode().ToString();
                row[2] = desc;
                dataEnum.Rows.Add(row);
            }
            return dataEnum;
        }

        /// <summary>
        /// 返回枚举项目
        /// 调用如：EnumHelper.EnumToList(typeof(Enum))
        /// </summary>
        public static List<EnumItem> EnumToList(Type enumType)
        {
            var names = Enum.GetNames(enumType);
            var values = Enum.GetValues(enumType);
            var meInfo = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            var items = new List<EnumItem>();

            for (int i = 0; i < values.Length; i++)
            {
                var attrs = meInfo[i].GetCustomAttributes(typeof(DescriptionAttribute), false);
                var item = new EnumItem()
                {
                    Name = names[i],
                    Value = values.GetValue(i).GetHashCode().ToString(),
                    Description = "",
                };
                if (attrs != null && attrs.Length > 0)
                    item.Description = ((DescriptionAttribute)attrs[0]).Description;

                items.Add(item);
            }
            return items;
        }
    }

    public class EnumItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
