using System;
using System.IO;
using log4net;
using log4net.Config;
using log4net.Repository;

namespace Video.Common
{
    public class LogHelper
    {
        private static ILoggerRepository repository { get; set; }
        private static ILog _Log;
        private static ILog Log
        {
            get
            {
                if (_Log == null)
                    Configure();
                return _Log;
            }
        }

        public static void Configure(string repositoryName = "NETCoreRepository", string configFile = "log4net.config")
        {
            repository = LogManager.CreateRepository(repositoryName);
            var file = new FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFile));
            XmlConfigurator.Configure(repository, file);
            _Log = LogManager.GetLogger(repositoryName, "loginfo");
        }

        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Info(string message, Exception exception = null)
        {
            if (exception == null)
                Log.Info(message);
            else
                Log.Info(message, exception);
        }
 
        /// <summary>
        /// 告警日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Warn(string message, Exception exception = null)
        {
            if (exception == null)
                Log.Warn(message);
            else
                Log.Warn(message, exception);
        }
 
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Error(string message, Exception exception = null)
        {
            if (exception == null)
                Log.Error(message);
            else
                Log.Error(message, exception);
        }
    }
}