using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Video.Common
{
    /// <summary>
    /// 加解密
    /// </summary>
    public class CryptHelper
    {
        /// <summary>
        /// 使用给定密钥加密
        /// </summary>
        /// <param name="original">明文</param>
        /// <param name="key">密钥</param>
        /// <returns>密文</returns>
        public static byte[] Encrypt(byte[] original, byte[] key)
        {
            var tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = ComputeMD5Hash(key);
            tripleDES.Mode = CipherMode.ECB;

            return tripleDES.CreateEncryptor().TransformFinalBlock(original, 0, original.Length);
        }

        /// <summary>
        /// 使用给定密钥解密数据
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <returns>明文</returns>
        public static byte[] Decrypt(byte[] encrypted, byte[] key)
        {
            var tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = ComputeMD5Hash(key);
            tripleDES.Mode = CipherMode.ECB;

            return tripleDES.CreateDecryptor().TransformFinalBlock(encrypted, 0, encrypted.Length);
        }

        public static byte[] ComputeMD5Hash(byte[] bytes)
        {
            return MD5.Create().ComputeHash(bytes);
        }

        /// <summary>
        /// 使用给定密钥字符串加密string
        /// </summary>
        /// <param name="original">原始文字</param>
        /// <param name="key">密钥</param>
        /// <param name="encoding">字符编码方案</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original, string key)
        {
            byte[] buff = System.Text.Encoding.Default.GetBytes(original);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return Convert.ToBase64String(Encrypt(buff, kb));
        }

        /// <summary>
        /// 使用给定密钥字符串解密string,返回指定编码方式明文
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <param name="encoding">字符编码方案</param>
        /// <returns>明文</returns>
        public static string Decrypt(string encrypted, string key)
        {
            byte[] buff = Convert.FromBase64String(encrypted);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return Encoding.Default.GetString(Decrypt(buff, kb));
        }

        /// <summary>
        /// 与ASP兼容的MD5加密算法
        /// </summary>
        public static string GetMD5(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";

            var md5 = new MD5CryptoServiceProvider();
            var bytes = md5.ComputeHash(Encoding.GetEncoding("utf-8").GetBytes(str));
            var stringBuilder = new StringBuilder(32);
            for (int i = 0; i < bytes.Length; i++)
            {
                stringBuilder.Append(bytes[i].ToString("x").PadLeft(2, '0'));
            }
            return stringBuilder.ToString();
        }
    }
}