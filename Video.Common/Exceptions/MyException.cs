using System;

namespace Video.Common
{
    public class MyException : Exception
    {
        public MyException(string message)
            : base(message)
        {
        }
    }
}