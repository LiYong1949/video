using System.ComponentModel.DataAnnotations;

namespace Video.Common
{
    public class PositiveIntegerAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value.ToInt32() > 0;
        }
    }
}